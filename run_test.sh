export BACKEND_URL=https://dev-backend.bizibaza.com
export ENVIRONMENT_KIND=develop
export FRONTEND_URL=http://localhost:4200
export SELENIUM_DRIVER_KIND=chrome

python3 -m pytest test_objects -v --html=test-reports-windows/report_windows_chrome.html