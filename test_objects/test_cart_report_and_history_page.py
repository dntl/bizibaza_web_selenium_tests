import datetime

from page_objects.cart_report_page import CartReport
from page_objects.history_page import HistoryElementDetails, HistoryElement
from test_objects.base_test_case import BaseTestCase
from factory import get_environment
from decimal import Decimal
from utility.formated_date import get_current_datetime

CHEDDAR_SALE_ID: str = "6aa6ed05eb7e41bc9a7fab40e3ce663a"
VISA_TEST_CARD: str = "4242424242424242"
EXP_DATE: float = 10 / 28
CVC: int = 123
NAME_ON_CARD: str = "Auto Test"

EXPECTED_TOTAL: Decimal = Decimal("8.90")
EXPECTED_PRICE_PER_UNIT: str = "$0.89 /Box"
EXPECTED_BUYER_NAME: str = "buyer_002"
EXPECTED_PURCHASE_TOTAL: Decimal = EXPECTED_TOTAL
EXPECTED_TOTAL_AMOUNT: Decimal = EXPECTED_TOTAL
EXPECTED_PRODUCT_NAME: str = "Cheddar Sale"
EXPECTED_COMPANY_NAME: str = "sellertest_0001"
EXPECTED_SELLER_PHONE_NUMBER: str = "(497) 412-2163"
EXPECTED_TOTAL_QUANTITY: str = "10 Box"
EXPECTED_TOTAL_QUANTITY_VALUE: str = EXPECTED_TOTAL_QUANTITY.split()[0]
EXPECTED_PICK_UP_ADDRESS: str = "2350 Noriega St, 730 Taraval St, San Francisco, CA, 94111"
EXPECTED_SELLER_ADDRESS: str = EXPECTED_PICK_UP_ADDRESS


class TestCartReportHistoryPages(BaseTestCase):
    cart_report_model: CartReport | None = None

    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""

        super().before_all_tests()
        cls.product_details_page_cheddar_sale = (
            BaseTestCase.page_factory().product_details_page(CHEDDAR_SALE_ID)
        )
        cls.cart_page = BaseTestCase.page_factory().cart_page()
        cls.payment_page = BaseTestCase.page_factory().payment_page()
        cls.cart_report_page = BaseTestCase.page_factory().cart_report_page()
        cls.orders_page = BaseTestCase.page_factory().orders_page()
        cls.history_page = BaseTestCase.page_factory().history_page()

        BaseTestCase.sign_in_via_api(
            get_environment().get_buyer_user_with_history().account
        )
        # Go to the product page directly.
        cls.product_details_page_cheddar_sale.open()

    @classmethod
    def after_all_tests(cls) -> None:
        BaseTestCase.logout_via_api()
        super().after_all_tests()

    def test_1_go_to_the_cart_report_page_after_successful_payment(self) -> None:
        """""
        https://dntlab.testrail.io/index.php?/cases/view/2814
        [TC-CE] Go to the Cart report page after successful payment
        """ ""
        # Enter nuber of products to the QTY field
        self.product_details_page_cheddar_sale.add_quantity(10)

        # Click on the button "Add to Cart"
        self.product_details_page_cheddar_sale.click_add_to_cart_button()

        # Click OK on the modal window about successful adding product
        self.product_details_page_cheddar_sale.modal_window.click_ok_button()

        # Click on the Cart icon on the header
        self.product_details_page_cheddar_sale.header.click_on_the_cart()

        # Wait the Cart with product is loaded
        self.cart_page.wait_full_cart_page_loaded()

        # Choose a delivery method
        self.cart_page.select_pick_up_option()

        # Check the total in the cart and compare with the total on the product page.
        actual_total = self.cart_page.get_cart_total_sum()
        self.assertEqual(EXPECTED_TOTAL, actual_total)

        # Click on the Pay button
        self.cart_page.click_on_pay_button()

        # Check that the link is correct, and we are on the Payment page
        self.assertTrue(self.payment_page.is_current_url())

        actual_total = self.payment_page.get_payment_total_sum()
        self.assertEqual(EXPECTED_TOTAL, actual_total)

        self.payment_page.input_card_number(VISA_TEST_CARD)
        self.payment_page.input_exp_date(EXP_DATE)
        self.payment_page.input_cvc(CVC)
        self.payment_page.input_name_on_card(NAME_ON_CARD)

        # Implement assert for validation errors here?
        pay_button_actual: str = self.payment_page.get_pay_button_status()
        pay_button_expected: str = "true"
        self.assertIsNot(pay_button_expected, pay_button_actual)

        self.payment_page.click_on_pay_button()

        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Checking text on Modal window
        expected_header: str = "Success:"
        expected_text: str = (
            "Payment was successful. We just sent your receipt to"
            " your email address, and your items will be on their way shortly."
        )

        actual_header: str = self.payment_page.modal_window.get_modal_title_text()
        actual_text: str = self.payment_page.modal_window.get_modal_message_text()
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_text, actual_text)

        self.payment_page.click_close_button_on_modal()

        # Check that the link is correct, and we are on the Cart Report Page
        self.assertTrue(self.cart_report_page.is_current_url())

        # Extract the CartReportModel object
        cart_report_model = self.cart_report_page.get_cart_report_model()

        # Assign the cart_report_model to the class attribute
        TestCartReportHistoryPages.cart_report_model = cart_report_model

        expected_buyer_name = EXPECTED_BUYER_NAME
        expected_purchase_total = EXPECTED_PURCHASE_TOTAL

        # Assert the attributes of the CartReportModel
        # Define the expected values
        # Convert the datetime objects to timestamps
        expected_sale_date = get_current_datetime()
        actual_sale_date = cart_report_model.sale_date

        # Verify general information.
        # Compare the sale dates with a tolerance of 140 seconds
        self.assertAlmostEqual(
            expected_sale_date, actual_sale_date, delta=datetime.timedelta(seconds=140)
        )
        self.assertEqual(cart_report_model.buyer_name, expected_buyer_name)
        self.assertEqual(cart_report_model.purchase_total, expected_purchase_total)

        # Verify product details.
        self.assertTrue(1, len(cart_report_model.products))
        self.assertEqual(
            EXPECTED_PRODUCT_NAME, cart_report_model.products[0].product_name
        )
        self.assertEqual(
            EXPECTED_COMPANY_NAME, cart_report_model.products[0].company_name
        )
        self.assertEqual(
            EXPECTED_TOTAL_QUANTITY, cart_report_model.products[0].total_quantity
        )
        self.assertEqual(
            EXPECTED_TOTAL_AMOUNT, cart_report_model.products[0].total_amount
        )
        self.assertEqual(
            EXPECTED_SELLER_PHONE_NUMBER,
            cart_report_model.products[0].seller_phone_number,
        )
        self.assertEqual(
            EXPECTED_PICK_UP_ADDRESS,
            cart_report_model.products[0].pick_up,
        )
        self.assertTrue(len(cart_report_model.products[0].order_code) > 0)

    def test_2_verify_products_counter_and_total(self) -> None:
        """""
        https://dntlab.testrail.io/index.php?/cases/view/1076
        [TC-BH] "Items counter" is displayed the total number of purchases 
        https://dntlab.testrail.io/index.php?/cases/view/1077
        [TC-BH] "Purchases total price" is displayed total price of all purchases
        """ ""

        # Open the Orders page
        self.cart_report_page.header.click_on_orders_button()
        # Open the Buyer history page
        self.orders_page.click_history_button()
        # Check that the link is correct, and we are on the History Page
        self.assertTrue(self.history_page.is_current_url())

        # Get the HistoryList model from the page
        history_model = self.history_page.get_history_model()

        # We verify that the total number of products in the list corresponds to the TOTAL figure
        # at the bottom of the page, and also compare the total cost of all products
        # on the page with the total cost at the bottom of the page.
        expected_product_count = history_model.total_qty_on_page
        actual_product_count = len(history_model.products_list)

        self.assertEqual(actual_product_count, expected_product_count)

        self.assertEqual(actual_product_count, expected_product_count)

        # Calculate the total sum from the history elements
        actual_total_sum_products_on_list = sum(
            element.total for element in history_model.products_list
        )

        # Get the parsed total from the page
        displayed_total_sum_products = self.history_page.get_parsed_total_of_the_page()

        # Assert the calculated total sum matches the parsed total
        self.assertEqual(
            actual_total_sum_products_on_list, displayed_total_sum_products
        )

    def test_3_verify_history_record(self) -> None:
        """""
        https://dntlab.testrail.io/index.php?/cases/view/11639
        [TC-BH] Verify History record
        """ ""

        # Access the stored cart report model from the setUp method
        cart_report_model = self.cart_report_model

        assert cart_report_model is not None
        # Create the expected HistoryElement
        expected_history_element = HistoryElement(
            sale_datetime=cart_report_model.sale_date,
            product_name=EXPECTED_PRODUCT_NAME,
            company_name=EXPECTED_COMPANY_NAME,
            total=EXPECTED_TOTAL_AMOUNT,
        )

        # Get the HistoryList model from the page
        history_model = self.history_page.get_history_model()

        # Assert the attributes of the HistoryElement
        actual_history_element = history_model.products_list[0]
        self.assertEqual(
            expected_history_element.sale_datetime,
            actual_history_element.sale_datetime,
        )
        self.assertEqual(
            expected_history_element.product_name,
            actual_history_element.product_name,
        )
        self.assertEqual(
            expected_history_element.company_name,
            actual_history_element.company_name,
        )
        self.assertEqual(
            expected_history_element.total, actual_history_element.total
        )


    def test_4_verify_history_record_details(self) -> None:
        """""
        # https://dntlab.testrail.io/index.php?/cases/view/1079
        # [TC-BH] Open the purchase record
        """""

        # Access the stored cart report model from the setUp method
        cart_report_model = self.cart_report_model

        assert cart_report_model is not None
        # Get the expected Order Code from the cart report model
        expected_order_code = cart_report_model.products[0].order_code

        # Create the expected HistoryElementDetails
        expected_history_element_details = HistoryElementDetails(
            product_name_details=EXPECTED_PRODUCT_NAME,
            cost_product_details=EXPECTED_PRICE_PER_UNIT,
            company_name_details=EXPECTED_COMPANY_NAME,
            order_code_details=expected_order_code,
            seller_address_details=EXPECTED_SELLER_ADDRESS,
            total_quantity_details=EXPECTED_TOTAL_QUANTITY_VALUE,
            total_cost_details=EXPECTED_TOTAL_AMOUNT,
            pick_up_details=EXPECTED_PICK_UP_ADDRESS,
            seller_phone_number_details=EXPECTED_SELLER_PHONE_NUMBER,
        )

        # Expand the newest (first) history element and view the Details:
        self.history_page.click_on_arrow_to_expand_history_element_by_index(0)

        # Extract the HistoryElementDetails from the page
        history_element_details = (
            self.history_page.extract_history_element_details()
        )

        # Assert the attributes of the HistoryElementDetails
        self.assertEqual(
            expected_history_element_details.product_name_details,
            history_element_details.product_name_details,
        )
        self.assertEqual(
            expected_history_element_details.cost_product_details,
            history_element_details.cost_product_details,
        )
        self.assertEqual(
            expected_history_element_details.company_name_details,
            history_element_details.company_name_details,
        )
        self.assertEqual(
            expected_history_element_details.order_code_details,
            history_element_details.order_code_details,
        )
        self.assertEqual(
            expected_history_element_details.seller_address_details,
            history_element_details.seller_address_details,
        )
        self.assertEqual(
            expected_history_element_details.total_quantity_details,
            history_element_details.total_quantity_details,
        )
        self.assertEqual(
            expected_history_element_details.total_cost_details,
            history_element_details.total_cost_details,
        )
