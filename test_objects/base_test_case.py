import unittest

from retry import retry
from selenium.common.exceptions import WebDriverException, InvalidCookieDomainException
from selenium.webdriver.remote.webdriver import WebDriver

from api_client.model.login_request import Account, LoginRequest
from api_client.operation.login import login_guest, auth_login
from api_client.operation.logout import auth_logout
from page_objects.bizipix_viewer_page import BiziPixViewerPage
from page_objects.cart_page import CartPage
from page_objects.cart_report_page import CartReportPage
from page_objects.change_password_page import ChangePasswordPage
from page_objects.chat_page import ChatPage
from page_objects.favorites_list_page import FavoritesListPage
from page_objects.history_page import HistoryPage
from page_objects.inventory_list_page import InventoryListPage
from page_objects.login_page import LoginPage
from page_objects.map_page import MapPage
from page_objects.orders_page import OrdersPage
from page_objects.page_component.navigation_menu import NavigationMenu
from page_objects.page_factory import PageFactory
from factory.webdriver_factory import WebDriverFactory
from factory import get_environment
from page_objects.payment_page import PaymentPage
from page_objects.product_details_page import ProductDetailsPage
from page_objects.product_reviews_page import ProductReviewsPage
from page_objects.profile_page import ProfilePage
from page_objects.search_by_subcategory_page import SearchbySubcategoryPage
from page_objects.search_page import SearchPage
from page_objects.seller_details_page import SellerDetailsPage
from page_objects.settings_page import SettingsPage
from page_objects.shopping_list_page import ShoppingListPage
from page_objects.sub_categories_page import SubCategoriesPage
from page_objects.watch_list_page import WatchListPage


class BaseTestCase(unittest.TestCase):
    _driver: WebDriver | None = None
    token: str | None = None
    bizipix_viewer_page: BiziPixViewerPage
    cart_page: CartPage
    cart_report_page: CartReportPage
    change_password_page: ChangePasswordPage
    chat_page: ChatPage
    favorites_list_page: FavoritesListPage
    history_page: HistoryPage
    inventory_list_page: InventoryListPage
    login_page: LoginPage
    map_page: MapPage
    navigation_menu: NavigationMenu
    orders_page: OrdersPage
    payment_page: PaymentPage
    product_details_page_cheddar: ProductDetailsPage
    product_details_page_cheddar_sale: ProductDetailsPage
    product_details_page_lemon_premium: ProductDetailsPage
    product_details_page_mustard_powder: ProductDetailsPage
    product_details_page_mustard_sauce: ProductDetailsPage
    product_details_page_sesame_seed: ProductDetailsPage
    product_details_page_square_apple: ProductDetailsPage
    product_reviews_page: ProductReviewsPage
    profile_page: ProfilePage
    search_by_subcategory_page: SearchbySubcategoryPage
    search_page: SearchPage
    seller_details_page: SellerDetailsPage
    settings_page: SettingsPage
    shopping_list_page: ShoppingListPage
    sub_categories_page: SubCategoriesPage
    watch_list_page: WatchListPage

    @classmethod
    def before_all_tests(cls) -> None:
        """Must be implemented in children classes.
        Put here Selenium action which must be executed BEFORE ALL TESTS (e.g. signing).

        Default implementation does nothing.
        """
        ...

    @classmethod
    def after_all_tests(cls) -> None:
        """Must be implemented in children classes.
        Put here Selenium action which must be executed AFTER ALL TESTS (e.g. logout).

        Default implementation does nothing.
        """
        ...

    @classmethod
    def setUpClass(cls) -> None:
        try:
            BaseTestCase._driver = WebDriverFactory.get_driver()
            cls.before_all_tests()
        except WebDriverException as e:
            BaseTestCase.get_driver().quit()
            raise e

    def setUp(self) -> None:
        self.env = get_environment()
        self.driver = BaseTestCase.get_driver()

    @classmethod
    def tearDownClass(cls) -> None:
        try:
            cls.after_all_tests()
        except WebDriverException as e:
            BaseTestCase.get_driver().quit()
            raise e

        BaseTestCase.get_driver().quit()

    def tearDown(self) -> None:
        self.driver.save_screenshot("test_reports/" + self.id() + ".png")

    @classmethod
    @retry(exceptions=InvalidCookieDomainException, tries=3, delay=0.5)
    def set_local_storage(cls, key: str, value: str) -> None:
        """Sets a value in Local Storage"""
        js_code = "window.localStorage.setItem(arguments[0], arguments[1]);"
        BaseTestCase.get_driver().execute_script(js_code, key, value)

    @classmethod
    @retry(exceptions=InvalidCookieDomainException, tries=3, delay=0.5)
    def set_cookie(cls, key: str, value: str) -> None:
        BaseTestCase.get_driver().add_cookie({"name": key, "value": value})

    @classmethod
    def page_factory(cls) -> PageFactory:
        assert BaseTestCase.get_driver()
        return PageFactory(BaseTestCase.get_driver())

    @classmethod
    def get_driver(cls) -> WebDriver:
        assert BaseTestCase._driver
        return BaseTestCase._driver

    @classmethod
    def sign_in_via_api(cls, account: Account) -> None:
        login_request = LoginRequest(account=account)
        guest_token = login_guest()
        response = auth_login(
            request={
                "login": login_request.account.login,
                "password": login_request.account.password,
            },
            headers={"token": guest_token},
        )
        BaseTestCase.get_driver().get(get_environment().get_frontend_url())
        BaseTestCase.token = response.token
        # Save to local storage guest token and type that guest is changed to user
        BaseTestCase.set_cookie("user", BaseTestCase.token)
        BaseTestCase.set_cookie("type", "")
        # Set the value that the Privacy Policy window is already closed.
        BaseTestCase.set_local_storage("isShowPrivacyPolicyNotification", "true")

    @classmethod
    def logout_via_api(cls) -> None:
        if BaseTestCase.token is None:
            raise ValueError("Token is not set")

        auth_logout(BaseTestCase.token)
