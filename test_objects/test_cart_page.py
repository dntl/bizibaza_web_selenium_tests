import pytest

from test_objects.base_test_case import BaseTestCase
from factory import get_environment

LEMON_PREMIUM_EACH = "d5153f934d8d425da3d6067f2aa89ced"
SESAME_SEED = "bc334720c2d148aabe00f5fa06e6795e"


class TestCartPage(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.search_page = BaseTestCase.page_factory().search_page()
        cls.product_details_page_lemon_premium = (
            BaseTestCase.page_factory().product_details_page(LEMON_PREMIUM_EACH)
        )
        cls.product_details_page_sesame_seed = (
            BaseTestCase.page_factory().product_details_page(SESAME_SEED)
        )
        cls.cart_page = BaseTestCase.page_factory().cart_page()
        cls.payment_page = BaseTestCase.page_factory().payment_page()
        cls.chat_page = BaseTestCase.page_factory().chat_page()
        BaseTestCase.sign_in_via_api(get_environment().get_seller_user().account)
        cls.shopping_list_page.open()

    def setUp(self) -> None:
        # Executed in every test
        super().setUp()

        self.search_keyword: str = "lemon"
        self.search_keyword2: str = "Sesame seed"
        self.product: str = "LEMON PREMIUM EACH"
        self.product2: str = "Sesame seed"

    def tearDown(self) -> None:
        # Return to Shopping list
        self.cart_page.header.click_bizibaza_logo()
        # Go to the Cart
        self.shopping_list_page.header.click_on_the_cart()
        # Removing products from the cart if they are present
        self.cart_page.remove_all_products_from_cart()
        # Checking that the Cart is empty
        self.cart_page.wait_empty_page_is_present()
        # Return to Shopping list
        self.cart_page.header.click_bizibaza_logo()
        # Executed after each test!
        super().tearDown()

    @classmethod
    def after_all_tests(cls) -> None:
        BaseTestCase.logout_via_api()
        super().after_all_tests()

    # @unittest.skip
    def test_add_to_cart_and_go_to_payment_C2399(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2790
        # https://dntlab.testrail.io/index.php?/cases/view/2399
        # Go to the product page
        self.product_details_page_lemon_premium.go_to_the_product_page_by_search(
            self.search_keyword, self.product
        )

        # Find the QTY input field, clear it and enter a new amount
        self.product_details_page_lemon_premium.add_quantity(10)

        # Click on the button "Add to Cart"
        self.product_details_page_lemon_premium.click_add_to_cart_button()
        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Get the message test from the modal window and compare with the expected result and click OK
        expected_header_added: str = "Success"
        expected_text_added: str = "This item was added to your Shopping Cart"
        actual_header_added: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_title_text()
        )
        actual_text_added: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_added, actual_header_added)
        self.assertEqual(expected_text_added, actual_text_added)

        self.cart_page.modal_window.click_ok_button()

        # We are waiting for the cart icon to display a counter with the number 1 of the added items.
        self.cart_page.wait_cart_counter()

        # Go to the Cart
        self.search_page.header.click_on_the_cart()

        # Check that the link is correct, and we are on the Cart page
        expected_url_cart: str = self.cart_page.expected_url()
        actual_url_cart: str = self.cart_page.get_current_url()

        self.assertEqual(expected_url_cart, actual_url_cart)
        # Check that the cart page has loaded
        self.cart_page.wait_full_cart_page_loaded()
        # Choose a delivery method
        self.cart_page.select_pick_up_option()
        # Click on the Pay button
        self.cart_page.click_on_pay_button()
        # Waiting for the page to load
        self.payment_page.wait_page_is_present()

        # Check that the link is correct, and we are on the Payment page
        expected_url_payment: str = self.payment_page.expected_url()
        actual_url_payment: str = self.payment_page.get_current_url()
        self.assertEqual(expected_url_payment, actual_url_payment)

    # @unittest.skip
    def test_removing_item_from_cart(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2422

        # Go to the product page
        self.product_details_page_lemon_premium.go_to_the_product_page_by_search(
            self.search_keyword, self.product
        )

        # Find the QTY input field, clear it and enter a new amount
        self.product_details_page_lemon_premium.add_quantity(10)

        # Click on the button "Add to Cart"
        self.product_details_page_lemon_premium.click_add_to_cart_button()

        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Get the message test from the modal window and compare with the expected result and click OK
        expected_header_added: str = "Success"
        expected_text_added: str = "This item was added to your Shopping Cart"
        actual_header_added: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_title_text()
        )
        actual_text_added: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_added, actual_header_added)
        self.assertEqual(expected_text_added, actual_text_added)

        self.cart_page.modal_window.click_ok_button()

        # Waiting for the displaying "1" on the Cart icon
        self.cart_page.wait_cart_counter()

        self.search_page.header.click_on_the_cart()

        # Check that the link is correct, and we are on the Cart page
        expected_url_cart: str = self.cart_page.expected_url()
        actual_url_cart: str = self.cart_page.get_current_url()

        self.assertEqual(expected_url_cart, actual_url_cart)
        # Check that the cart page has loaded
        self.cart_page.wait_full_cart_page_loaded()
        # Click on the Remove button
        self.cart_page.remove_first_item_from_cart()
        # Checking if the cart is empty
        self.assertTrue(self.cart_page.is_cart_page_empty(), msg="Cart is NOT empty!")

        actual_counter: str = self.cart_page.get_badge_counter_on_the_cart()
        expected_counter: str = "0"
        self.assertEqual(actual_counter, expected_counter)

    def test_purchase_less_2_usd(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2398

        # Go to the product page (Product must be less than 2$ cost)
        self.product_details_page_sesame_seed.go_to_the_product_page_by_search(
            self.search_keyword2, self.product2
        )

        # Click on the button "Add to Cart"
        self.product_details_page_sesame_seed.click_add_to_cart_button()

        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Get the message test from the modal window and compare with the expected result and click OK
        expected_header_pay: str = "Success"
        expected_text_pay: str = "This item was added to your Shopping Cart"
        actual_header_pay: str = (
            self.product_details_page_sesame_seed.modal_window.get_modal_title_text()
        )
        actual_text_pay: str = (
            self.product_details_page_sesame_seed.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_pay, actual_header_pay)
        self.assertEqual(expected_text_pay, actual_text_pay)

        self.cart_page.modal_window.click_ok_button()

        self.cart_page.wait_cart_counter()

        self.search_page.header.click_on_the_cart()

        # Check that the link is correct, and we are on the Cart page
        expected_url_cart: str = self.cart_page.expected_url()
        actual_url_cart: str = self.cart_page.get_current_url()

        self.assertEqual(expected_url_cart, actual_url_cart)
        # Check that the cart page has loaded
        self.cart_page.wait_full_cart_page_loaded()

        # Choose a delivery method
        self.cart_page.select_pick_up_option()

        # Click on the Pay button
        self.cart_page.click_on_pay_button()

        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Get the message test from the modal window and compare with the expected result and click OK
        expected_header_unable_pay: str = "Unable to pay:"
        expected_text_unable_pay: str = (
            "Minimum purchase required is $2.00 per seller. Please adjust your cart's "
            "products/quantities in order to complete your transaction."
        )
        actual_header_unable_pay: str = (
            self.product_details_page_sesame_seed.modal_window.get_modal_title_text()
        )
        actual_text_unable_pay: str = (
            self.product_details_page_sesame_seed.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_unable_pay, actual_header_unable_pay)
        self.assertEqual(expected_text_unable_pay, actual_text_unable_pay)

        # Click on Close
        self.cart_page.modal_window.click_ok_button()

    # @unittest.skip
    def test_delivery_method(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2432

        # Go to the product page
        self.product_details_page_lemon_premium.go_to_the_product_page_by_search(
            self.search_keyword, self.product
        )

        # Click on the button "Add to Cart"
        self.product_details_page_lemon_premium.click_add_to_cart_button()

        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Get the message test from the modal window and compare with the expected result and click OK
        expected_header_delivery: str = "Success"
        expected_text_delivery: str = "This item was added to your Shopping Cart"
        actual_header_delivery: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_title_text()
        )
        actual_text_delivery: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_delivery, actual_header_delivery)
        self.assertEqual(expected_text_delivery, actual_text_delivery)

        self.cart_page.modal_window.click_ok_button()

        # Go to the Cart
        self.search_page.header.click_on_the_cart()

        # Check that the link is correct, and we are on the Cart page
        expected_url_cart: str = self.cart_page.expected_url()
        actual_url_cart: str = self.cart_page.get_current_url()

        self.assertEqual(expected_url_cart, actual_url_cart)
        # Check that the cart page has loaded
        self.cart_page.wait_full_cart_page_loaded()

        self.cart_page.select_delivery_option()

        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Get the message test from the modal window and compare with the expected result and click OK
        expected_header_terms: str = "Delivery Terms"
        expected_text_terms: str = "Contact Seller for details?"
        actual_header_terms: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_title_text()
        )
        actual_text_terms: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_terms, actual_header_terms)
        self.assertEqual(expected_text_terms, actual_text_terms)

        self.cart_page.click_ok_button_to_contact_seller()

        # Check that the Chat Page has loaded and the link is correct

        expected_url_chat: str = self.chat_page.expected_url()
        actual_url_chat: str = self.chat_page.get_current_url()

        self.assertIn(expected_url_chat, actual_url_chat)

    @pytest.mark.skip(reason="https://dntlab.atlassian.net/browse/BIZ-4744")
    def test_click_the_pay_button_without_choosing_the_delivery_method(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2392
        # Go to the product page
        self.product_details_page_lemon_premium.go_to_the_product_page_by_search(
            self.search_keyword, self.product
        )

        # Click on the button "Add to Cart"
        self.product_details_page_lemon_premium.click_add_to_cart_button()

        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Get the message test from the modal window and compare with the expected result and click OK
        expected_header_delivery: str = "Success"
        expected_text_delivery: str = "This item was added to your Shopping Cart"
        actual_header_delivery: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_title_text()
        )
        actual_text_delivery: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_delivery, actual_header_delivery)
        self.assertEqual(expected_text_delivery, actual_text_delivery)

        self.cart_page.modal_window.click_ok_button()

        # Go to the Cart
        self.search_page.header.click_on_the_cart()

        # Check that the link is correct, and we are on the Cart page
        expected_url_cart: str = self.cart_page.expected_url()
        actual_url_cart: str = self.cart_page.get_current_url()

        self.assertEqual(expected_url_cart, actual_url_cart)
        # Check that the cart page has loaded
        self.cart_page.wait_full_cart_page_loaded()
        # Click on the Pay button
        self.cart_page.click_on_pay_button()
        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Get the message test from the modal window and compare with the expected result and click OK
        expected_header_warning: str = "Warning:"
        expected_text_warning: str = (
            "Delivery method must be selected for all products."
        )
        actual_header_warning: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_title_text()
        )
        actual_text_warning: str = (
            self.product_details_page_lemon_premium.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_warning, actual_header_warning)
        self.assertEqual(expected_text_warning, actual_text_warning)

        self.cart_page.modal_window.click_ok_button()

        # Click on the Remove button
        self.cart_page.remove_first_item_from_cart()
        # Checking if the cart is empty
        self.assertTrue(self.cart_page.is_cart_page_empty(), msg="Cart is NOT empty!")
