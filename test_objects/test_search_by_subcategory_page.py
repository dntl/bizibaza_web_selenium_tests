from decimal import Decimal

import pytest

from factory import get_environment
from page_objects.page_component.navigation_menu import NavigationMenuLocators
from test_objects.base_test_case import BaseTestCase
from model.my_search_results import SearchResult

MUSTARD_POWDER = "15f08c6be09544adb6fdcb41ac28ad8f"
MUSTARD_SAUCE = "6c7059396f814780a61c6969c72d3dcf"


class TestSearchBySubcategory(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.navigation_menu = BaseTestCase.page_factory().navigation_menu()
        cls.search_by_subcategory_page = (
            BaseTestCase.page_factory().search_by_subcategory_page()
        )
        cls.product_details_page_mustard_powder = (
            BaseTestCase.page_factory().product_details_page(MUSTARD_POWDER)
        )
        cls.product_details_page_mustard_sauce = (
            BaseTestCase.page_factory().product_details_page(MUSTARD_SAUCE)
        )
        cls.map_page = BaseTestCase.page_factory().map_page()
        cls.search_page = BaseTestCase.page_factory().search_page()
        cls.orders_page = BaseTestCase.page_factory().orders_page()
        cls.cart_page = BaseTestCase.page_factory().cart_page()

        # Log in to the Bizibaza via API
        BaseTestCase.sign_in_via_api(get_environment().get_buyer_user_with_sh().account)

        # Open the Shopping list
        cls.shopping_list_page.open()

    def setUp(self) -> None:
        # Runs before every test!
        super().setUp()

        # Return to Shopping list
        self.search_by_subcategory_page.header.click_bizibaza_logo()

        self.product: str = "Mustard"
        self.market: str = " Kharkov Market 2"

        self.shopping_list_page.click_on_the_item_on_shopping_list(self.product)

    def tearDown(self) -> None:
        # Executed after each test!
        super().tearDown()

    @classmethod
    def after_all_tests(cls) -> None:
        BaseTestCase.logout_via_api()
        super().after_all_tests()

    def test_open_the_cart_page_from_search_by_subcategory(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/454

        self.search_by_subcategory_page.header.click_on_the_cart()
        expected_url_orders: str = (
            self.env.get_frontend_url() + self.cart_page.get_url()
        )
        actual_url_orders: str = self.driver.current_url
        self.assertEqual(expected_url_orders, actual_url_orders)

    def test_open_the_orders_page_search_by_subcategory(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/455

        self.search_by_subcategory_page.header.click_on_orders_button()
        expected_url_orders: str = (
            self.env.get_frontend_url() + self.orders_page.get_url()
        )
        actual_url_orders: str = self.driver.current_url
        self.assertEqual(expected_url_orders, actual_url_orders)

    def test_open_categories_menu_from_search_by_subcategory(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/8265

        # Open the Categories menu
        self.search_by_subcategory_page.footer.click_on_categories_btn()
        # Check that all categories have loaded
        self.search_by_subcategory_page.categories_menu.wait_till_all_categories_is_displayed()
        # Close the Categories menu
        self.search_by_subcategory_page.footer.click_on_categories_btn()
        # TODO: Add assert

    def test_open_navigation_menu_from_search_by_subcategory(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/458

        self.search_by_subcategory_page.footer.open_the_navigation_menu()

        self.assertTrue(
            self.navigation_menu.is_visible(NavigationMenuLocators.CART),
            "The Navigation menu is NOT displayed on the page.",
        )

        # Click the menu button again to close
        self.search_by_subcategory_page.footer.close_the_navigation_menu()

        self.assertFalse(
            self.navigation_menu.is_visible(NavigationMenuLocators.CART),
            "The Navigation menu is displayed on the page.",
        )

    def test_open_map_from_search_by_subcategory(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/459

        # Open the Map
        self.search_by_subcategory_page.open_map_by_subcategory()
        # Check that the map with all elements has loaded
        self.search_by_subcategory_page.check_maps_elements_search_by_subcategory(
            self.product
        )
        # TODO: Add assert
        # Check URL
        map_page_expected_url: str = (
            self.env.get_frontend_url() + self.map_page.get_url()
        )
        map_page_actual_url: str = self.driver.current_url
        self.assertIn(map_page_expected_url, map_page_actual_url)
        # Go back from Map
        self.search_by_subcategory_page.back_from_the_subcategory_map()

    def test_go_to_the_product_page(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/462
        # https://dntlab.testrail.io/index.php?/cases/view/463

        # Sort prices from high to low (Sort by price ⇩)
        self.search_page.sort_items("descending")

        # Check that the product is not sold out (does not have the "Sold Out" label)
        self.search_by_subcategory_page.get_product_is_not_sold_out()

        # We find the input field for the zero product in the list and enter the desired number of products
        self.search_by_subcategory_page.enter_qty_to_first_item(2)

        # Go to the product page
        self.search_by_subcategory_page.click_on_the_first_item_in_list()

        # We check that the value entered earlier is in the field for the number of goods, i.e.
        # the quantity is multiplied by the cost and compared with the expected result
        actual_cost = self.product_details_page_mustard_powder.get_cost_of_product()
        expected_cost: Decimal = Decimal("19.98")
        self.assertEqual(expected_cost, actual_cost)

    def test_sorting_items_by_price(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/467

        # Sort prices from low to high
        self.search_page.sort_items("ascending")

        # We collect all the values of the price tags from the page into a list, parse the values
        # by separating the $ sign
        current_page_search_results: list[
            SearchResult
        ] = self.search_page.get_current_page_search_results_price()

        # Check that the list of search results is not empty, i.e. is not equal to zero.
        assert len(current_page_search_results) != 0, "Search results are empty"

        # We want to check that the price tags are sorted in ascending order.
        # The first step is to get a list of all price tags.
        prices: list[Decimal] = []
        for result in current_page_search_results:
            prices.append(result.price)

        # Comparing that the list of price tags is sorted in non-descending order
        self.assertEqual(sorted(prices), prices)

    @pytest.mark.xfail(reason="https://dntlab.atlassian.net/browse/BIZ-4847")
    def test_go_to_the_market_product_page(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/470

        # Sort prices from low to high (Sort by price ⇧)
        self.search_page.sort_items("ascending")

        # Expand the list of markets of the first product in the list
        self.search_by_subcategory_page.open_market_list_of_the_first_item()

        # Find the first layout in the list and click on it
        self.search_by_subcategory_page.click_on_the_market_on_search_by_subcategory(
            self.market, MUSTARD_SAUCE
        )

        # Check that we are on the product page of the desired market
        self.product_details_page_mustard_sauce.find_market_name_on_product_page(
            self.market
        )
        # TODO: Add assert
