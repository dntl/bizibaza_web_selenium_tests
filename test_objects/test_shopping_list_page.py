from typing import List

from factory import get_environment
from page_objects.page_component.navigation_menu import NavigationMenuLocators
from test_objects.base_test_case import BaseTestCase


class TestShoppingList(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.navigation_menu = BaseTestCase.page_factory().navigation_menu()
        cls.search_page = BaseTestCase.page_factory().search_page()
        cls.settings_page = BaseTestCase.page_factory().settings_page()
        cls.orders_page = BaseTestCase.page_factory().orders_page()
        cls.cart_page = BaseTestCase.page_factory().cart_page()
        cls.inventory_list_page = BaseTestCase.page_factory().inventory_list_page()
        cls.sub_categories_page = BaseTestCase.page_factory().sub_categories_page()
        BaseTestCase.sign_in_via_api(get_environment().get_seller_user().account)

        # Open the Shopping list page
        cls.shopping_list_page.open()

        # Clear the shopping list
        cls.shopping_list_page.delete_all_items_from_list()

    def setUp(self) -> None:
        super().setUp()

        # Return to Shopping list
        self.shopping_list_page.header.click_bizibaza_logo()
        # Existing items
        self.item: str = "Red Apple"
        self.item2: str = "Mustard"
        self.item3: str = "Pinkerton"
        self.item4: str = "Gherkin"
        self.item5: str = "Wild Plantain"
        # Custom items
        self.custom: str = "Lemon Custom"
        self.custom2: str = "Wild_PlantainCustom"
        self.custom3: str = "Avocado Custom"
        self.custom4: str = "Red Apple Custom"
        self.custom5: str = "Gherkin Custom"

    def tearDown(self) -> None:
        super().tearDown()
        # Clear the shopping list
        self.shopping_list_page.delete_all_items_from_list()

    @classmethod
    def after_all_tests(cls) -> None:
        super().after_all_tests()

    # @unittest.skip
    def test_add_existing_item_by_clicking_on_suggestion(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/189
        # Type in the field "New item title" the name 'Red Apple'
        self.shopping_list_page.text_to_the_new_item(self.item)
        # Select the first found suggestion for this query
        self.shopping_list_page.select_first_suggestion_by_click(self.item)
        # Check that new item is displayed on the Shopping list
        actual_item: str = self.shopping_list_page.is_item_displayed_on_the_list()
        expected_item: str = self.item
        self.assertEqual(actual_item, expected_item)
        # Check that new added item is not custom but existing

    # @unittest.skip
    def test_add_existing_item_from_categories_menu(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/7537
        # Add one item to the list using the Categories Menu
        # Open the Categories menu
        self.shopping_list_page.footer.click_on_categories_btn()
        # Check that all categories have loaded
        self.shopping_list_page.categories_menu.wait_till_all_categories_is_displayed()
        # Select Category Fruits
        self.shopping_list_page.categories_menu.select_category("Fruits")
        # Check that all subcategories are displayed on the page
        self.sub_categories_page.wait_page_is_present()
        # Select the second Subcategory
        self.sub_categories_page.select_subcategory()
        # Select the first value for this subcategory
        self.sub_categories_page.select_subcategory_entry()
        # Check if Subcategory entry is checked
        self.sub_categories_page.check_entry_title_ok(self.item3)
        # Go back to the Shopping list page
        self.shopping_list_page.back_to_shopping_list()
        # Check that new item is displayed on the Shopping list
        actual_item: str = self.shopping_list_page.is_item_displayed_on_the_list()
        expected_item: str = self.item3
        self.assertEqual(actual_item, expected_item)
        # Check that new added item is not custom but existing

    # @unittest.skip
    def test_add_custom_item_with_search_suggestions(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/190
        # Type in the field "New item title" the name 'Gherkin' and press Enter
        self.shopping_list_page.text_to_the_new_item_enter(self.item4)
        # Check that new item is displayed on the Shopping list
        actual_item: str = self.shopping_list_page.is_item_displayed_on_the_list()
        expected_item: str = self.item4
        self.assertEqual(actual_item, expected_item)

    # @unittest.skip
    def test_sorting_shopping_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/209
        # Add existing items to the Shopping list
        self.shopping_list_page.text_to_the_new_item(self.item)
        self.shopping_list_page.select_first_suggestion_by_click(self.item)
        self.shopping_list_page.text_to_the_new_item(self.item2)
        self.shopping_list_page.select_first_suggestion_by_click(self.item2)
        self.shopping_list_page.text_to_the_new_item(self.item3)
        self.shopping_list_page.select_first_suggestion_by_click(self.item3)
        self.shopping_list_page.text_to_the_new_item(self.item4)
        self.shopping_list_page.select_first_suggestion_by_click(self.item4)
        self.shopping_list_page.text_to_the_new_item(self.item5)
        self.shopping_list_page.select_first_suggestion_by_click(self.item5)

        # Add custom items to the Shopping list
        self.shopping_list_page.text_to_the_new_item_enter(self.custom)
        self.shopping_list_page.text_to_the_new_item_enter(self.custom2)
        self.shopping_list_page.text_to_the_new_item_enter(self.custom3)
        self.shopping_list_page.text_to_the_new_item_enter(self.custom4)
        self.shopping_list_page.text_to_the_new_item_enter(self.custom5)

        # Mark two existing items
        self.shopping_list_page.mark_item_on_shopping_list(0)
        self.shopping_list_page.mark_item_on_shopping_list(1)
        # Mark two custom items
        self.shopping_list_page.mark_item_on_shopping_list(3)
        self.shopping_list_page.mark_item_on_shopping_list(3)
        # Click on the Sort List button
        self.shopping_list_page.sort_shopping_list()

        actual_product_list_order: List[
            str
        ] = self.shopping_list_page.get_products_list_names()
        actual_product_list_marked_order: List[
            str
        ] = self.shopping_list_page.get_products_list_marked_names()

        # We check that the list of marked and the list of unmarked items are sorted alphabetically from A to Z,
        # and both lists sorted separately from each other.
        # Each of these lists is checked separately.
        self.assertEqual(actual_product_list_order, sorted(actual_product_list_order))
        self.assertEqual(
            actual_product_list_marked_order, sorted(actual_product_list_marked_order)
        )

    # @unittest.skip
    def test_open_search_link_from_shopping_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/169
        self.search_page.click_search_button()
        expected_url_search: str = (
            self.env.get_frontend_url() + self.search_page.get_url()
        )
        actual_url_search: str = self.driver.current_url
        self.assertEqual(expected_url_search, actual_url_search)

    # @unittest.skip
    def test_open_settings_from_shopping_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/168
        self.shopping_list_page.click_settings_button()
        expected_url_settings: str = (
            self.env.get_frontend_url() + self.settings_page.get_url()
        )
        actual_url_settings: str = self.driver.current_url
        self.assertEqual(expected_url_settings, actual_url_settings)

    # @unittest.skip
    def test_open_the_orders_page_from_shopping_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/167
        self.shopping_list_page.header.click_on_orders_button()
        expected_url_orders: str = (
            self.env.get_frontend_url() + self.orders_page.get_url()
        )
        actual_url_orders: str = self.driver.current_url
        self.assertEqual(expected_url_orders, actual_url_orders)

    # @unittest.skip
    def test_open_the_cart_page_from_shopping_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/166
        self.shopping_list_page.header.click_on_the_cart()
        expected_url_orders: str = (
            self.env.get_frontend_url() + self.cart_page.get_url()
        )
        actual_url_orders: str = self.driver.current_url
        self.assertEqual(expected_url_orders, actual_url_orders)

    # @unittest.skip
    def test_go_to_seller_mode_from_shopping_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/170
        self.shopping_list_page.footer.click_seller_mode()
        expected_url_seller: str = (
            self.env.get_frontend_url() + self.inventory_list_page.get_url()
        )
        actual_url_seller: str = self.driver.current_url
        self.assertEqual(expected_url_seller, actual_url_seller)
        # return to buyer mode
        self.shopping_list_page.footer.click_buyer_mode()

    # @unittest.skip
    def test_open_navigation_menu_from_shopping_list_page(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/172
        self.shopping_list_page.footer.open_the_navigation_menu()
        # Check that all menu items are displayed
        self.shopping_list_page.footer.navigation_menu.wait_for_all_navigation_menu()

        self.assertTrue(
            self.navigation_menu.is_visible(NavigationMenuLocators.CART),
            "The Navigation menu is NOT displayed on the page.",
        )
        # Click the menu button again to close
        self.shopping_list_page.footer.close_the_navigation_menu()

        self.assertFalse(
            self.navigation_menu.is_visible(NavigationMenuLocators.CART),
            "The Navigation menu is displayed on the page.",
        )

    # @unittest.skip
    def test_delete_an_unmarked_item_from_categories(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/221
        # Type in the field "New item title" the name 'Avocado'
        self.shopping_list_page.text_to_the_new_item(self.item3)
        # Select the first found suggestion for this query
        self.shopping_list_page.select_first_suggestion_by_click(self.item3)
        # Check that new item is displayed on the Shopping list
        actual_item: str = self.shopping_list_page.is_item_displayed_on_the_list()
        expected_item: str = self.item3
        self.assertEqual(actual_item, expected_item)

        # Delete item from the Shopping list
        self.shopping_list_page.click_on_trash_icon_on_list()
        # Check that item is deleted and Shopping list is empty
        self.assertIs(self.shopping_list_page.is_shopping_list_empty(), True)
