import time
from decimal import Decimal

from test_objects.base_test_case import BaseTestCase
from factory import get_environment


SQUARE_APPLE_ID: str = "4dbb9e09f29644eb9e672ffdb0571f4e"
CHEDDAR_SALE_ID: str = "6aa6ed05eb7e41bc9a7fab40e3ce663a"


class TestProductDetailsPage(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.orders_page = BaseTestCase.page_factory().orders_page()
        cls.cart_page = BaseTestCase.page_factory().cart_page()

        cls.product_details_page_square_apple = (
            BaseTestCase.page_factory().product_details_page(SQUARE_APPLE_ID)
        )

        cls.payment_page = BaseTestCase.page_factory().payment_page()
        cls.favorites_list_page = BaseTestCase.page_factory().favorites_list_page()
        cls.watch_list_page = BaseTestCase.page_factory().watch_list_page()
        cls.bizipix_viewer_page = BaseTestCase.page_factory().bizipix_viewer_page()
        cls.product_reviews_page = BaseTestCase.page_factory().product_reviews_page()
        cls.seller_details_page = BaseTestCase.page_factory().seller_details_page()

        BaseTestCase.sign_in_via_api(get_environment().get_seller_user().account)

    def setUp(self) -> None:
        # Runs before every test!
        super().setUp()

        # Go to the product page
        self.product_details_page_square_apple.open()

        self.product: str = "Unusual square red apple"

    def tearDown(self) -> None:
        # Executed after each test!
        super().tearDown()

    @classmethod
    def after_all_tests(cls) -> None:
        BaseTestCase.logout_via_api()
        super().after_all_tests()


    def test_02_add_quantity_success(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2578
        # The test verifies the successful entry of the available quantity of items and its correct pricing.

        # Find the QTY input field, clear it and enter a new amount
        self.product_details_page_square_apple.add_quantity(10)

        # Get the value from the QTY field
        actual_qty: str = (
            self.product_details_page_square_apple.get_value_from_quantity_field()
        )

        # Compare the received value with the expected value
        self.assertEqual("10", actual_qty)

        # Check that the amount was calculated correctly and is displayed next to the input field
        actual_cost = self.product_details_page_square_apple.get_cost_of_product()

        # Compare the received value with the expected value
        expected_cost: Decimal = Decimal("27.00")
        self.assertEqual(expected_cost, actual_cost)


    def test_03_add_quantity_exceeds(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2580
        # The test revealed that errors are detected when entering a quantity of goods that is excessive.

        # Find the QTY input field, clear it and enter a new quantity that exceeds the available number of products
        self.product_details_page_square_apple.add_quantity(100000)

        # Check that the error about exceeding the number is displayed
        actual_error: str = (
            self.product_details_page_square_apple.get_exceeds_error_text()
        )
        expected_error: str = "Sorry, your demand exceeds available quantity"
        self.assertIn(expected_error, actual_error)


    def test_04_add_quantity_invalid_data(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2581
        # The test checks that invalid characters cannot be entered into the QTY field

        # Find the QTY input field, clear it and enter the character set as an invalid value '@#$%asdfg'
        self.product_details_page_square_apple.add_quantity("@#$%asdfg")

        # Get the value that is set in the QTY input field
        actual_qty: str = (
            self.product_details_page_square_apple.get_value_from_quantity_field()
        )

        # Compare the received value with the expected value.
        # Check that the input field is still empty,
        # (set stub as "0" (placeholder), but actually empty)
        self.assertEqual("", actual_qty)

        # Check that the cost is still "$0.00"
        actual_cost = self.product_details_page_square_apple.get_cost_of_product()

        # Compare the received value with the expected value
        expected_cost: Decimal = Decimal("0.00")
        self.assertEqual(expected_cost, actual_cost)


    def test_05_add_quantity_zero(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/8516

        # The test checks that you can enter "0" in the QTY field,
        # this will be a valid value and the price will not be calculated

        # Find the QTY input field, clear it and enter '0'
        self.product_details_page_square_apple.add_quantity(0)

        # Get the value that is set in the QTY input field
        actual_qty: str = (
            self.product_details_page_square_apple.get_value_from_quantity_field()
        )

        # Get the value that is set in the QTY input field
        self.assertEqual("0", actual_qty)

        # Check that the cost is still $0.00"
        actual_cost: Decimal = (
            self.product_details_page_square_apple.get_cost_of_product()
        )

        # Check that the cost is still $0.00"
        expected_cost: Decimal = Decimal("0.00")
        self.assertEqual(expected_cost, actual_cost)


    def test_06_add_item_to_the_cart(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2610
        # The test checks the successful addition of a product to the Cart using the Add to Cart button

        # Find the QTY input field, clear it and enter the quantity of goods that is definitely available for purchase
        self.product_details_page_square_apple.add_quantity(10)

        # Get the value from the QTY field and compare the expected value with the received one
        actual_qty: str = (
            self.product_details_page_square_apple.get_value_from_quantity_field()
        )
        self.assertEqual("10", actual_qty)

        # Check that the amount was calculated correctly and is displayed next to the input field
        actual_cost: Decimal = (
            self.product_details_page_square_apple.get_cost_of_product()
        )

        # Compare the received value with the expected value
        expected_cost: Decimal = Decimal("27.00")
        self.assertEqual(expected_cost, actual_cost)

        # Click on the button "Add to Cart"
        self.product_details_page_square_apple.click_add_to_cart_button()

        # Get the message test from the modal window and compare with the expected result and click OK
        self.assertTrue(
            self.product_details_page_square_apple.modal_window.is_present()
        )
        expected_header_added: str = "Success"
        expected_text_added: str = "This item was added to your Shopping Cart"
        actual_header_added: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_text_added: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_added, actual_header_added)
        self.assertEqual(expected_text_added, actual_text_added)
        self.product_details_page_square_apple.modal_window.click_ok_button()

        # Check that the "Add to Cart" button has changed to "Remove from Cart"
        actual_button = (
            self.product_details_page_square_apple.get_remove_from_cart_button_text()
        )
        self.assertEqual("Remove from Cart", actual_button)

        # Check that the Cart counter "1" is displayed and Go to the Cart

        # Wait till message is appeared on the cart counter about added product
        self.cart_page.wait_badge_counter_text()

        self.assertTrue(self.cart_page.visible_cart_counter_badge())

        # Click on the Cart icon on the header
        self.product_details_page_square_apple.header.click_on_the_cart()
        # Check that the link is correct, and we are on the cart page
        expected_url_cart: str = self.cart_page.expected_url()
        actual_url_cart: str = self.cart_page.get_current_url()

        self.assertEqual(expected_url_cart, actual_url_cart)

        # Wait the Cart with product is loaded
        self.cart_page.wait_full_cart_page_loaded()

        # Check that our product in the Cart
        actual_name = self.cart_page.get_cart_product_name()
        expected_name = self.product
        self.assertIn(expected_name, actual_name)

        # Empty the Cart and check that it is empty (Checking for an empty Cart is necessary in order to
        # so that the delete button has time to click before switching to SH)
        self.cart_page.remove_first_item_from_cart()
        self.cart_page.wait_page_is_present()


    def test_07_remove_item_from_the_cart(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2612

        # The test checks if the product was successfully removed from the Cart using the Remove from Cart button

        # Find the QTY input field, clear it and enter the quantity of goods that is definitely available for purchase
        self.product_details_page_square_apple.add_quantity(10)
        # Click on the button "Add to Cart""
        self.product_details_page_square_apple.click_add_to_cart_button()
        # Click OK on the message "This item was added to your Shopping Cart"
        self.product_details_page_square_apple.modal_window.click_ok_button()
        # Added a sleep because the application reacts incorrectly when clicking quickly and the product counter
        # is not disappear when an item is removed from the cart.
        time.sleep(1)
        # Click on the button "Remove from Cart"
        self.product_details_page_square_apple.click_remove_from_cart_button()
        # Get the message test from the modal window and compare with the expected result and click OK
        self.assertTrue(
            self.product_details_page_square_apple.modal_window.is_present()
        )
        expected_header_removed: str = "Success"
        expected_text_removed: str = "This item was removed from your Shopping Cart"
        actual_header_removed: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_text_removed: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_text_removed, actual_text_removed)
        self.assertEqual(expected_header_removed, actual_header_removed)

        self.product_details_page_square_apple.modal_window.click_ok_button()

        # Check if the cart counter is zero (invisible of element "There are 1 product in your shopping cart")
        self.product_details_page_square_apple.get_invisibility_cart_counter()

        # Go to the cart and check that it is empty.
        self.product_details_page_square_apple.header.click_on_the_cart()
        self.cart_page.wait_empty_page_is_present()


    def test_08_add_to_cart_without_enter_qty(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2608
        # The test checks that when you click on the Add to Cart button without entering the number of products,
        # 1 item is added to the cart.

        # Without entering the quantity of goods, click on the "Add to Cart" button
        self.product_details_page_square_apple.click_add_to_cart_button()

        # Get the message test from the modal window and compare with the expected result, click OK
        self.assertTrue(
            self.product_details_page_square_apple.modal_window.is_present()
        )
        expected_header_added: str = "Success"
        expected_text_added: str = "This item was added to your Shopping Cart"
        actual_header_added: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_text_added: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_added, actual_header_added)
        self.assertEqual(expected_text_added, actual_text_added)

        self.product_details_page_square_apple.modal_window.click_ok_button()

        # Check that the Cart counter "1" is displayed and Go to the cart
        self.cart_page.wait_cart_counter()
        self.product_details_page_square_apple.header.click_on_the_cart()

        # Check that the link is correct, and we are on the cart page
        expected_url_cart: str = self.cart_page.expected_url()
        actual_url_cart: str = self.cart_page.get_current_url()

        self.assertEqual(expected_url_cart, actual_url_cart)

        # Check that the number of items in Cart is 1 (Check Total and compare with expected)
        actual_total: str = self.cart_page.get_total_of_items()
        expected_total: str = "1 items"
        self.assertEqual(expected_total, actual_total)

        # Remove item from cart
        self.cart_page.remove_first_item_from_cart()
        self.cart_page.wait_page_is_present()


    def test_09_buy_now_item_without_enter_qty(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2613

        # The test checks that when clicking on the Buy now button without entering a quantity, an error is displayed

        # Click on the Buy now button
        self.product_details_page_square_apple.click_buy_now_button()

        # Get test message from Alert and compare with expected result, click "OK"
        expected_header: str = "Alert:"
        expected_message: str = "Please enter quantity desired."
        actual_header: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_message: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_message, actual_message)

        self.product_details_page_square_apple.click_ok_alert_message()


    def test_10_buy_now_item_entered_qty(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2614

        # The test checks the successful transition to payment using the "Buy now" button

        # Find the QTY input field, clear it and enter the quantity of goods that is definitely available for purchase
        self.product_details_page_square_apple.add_quantity(10)

        # Get the value from the QTY field and compare the expected value with the received one
        actual_qty: str = (
            self.product_details_page_square_apple.get_value_from_quantity_field()
        )
        self.assertEqual("10", actual_qty)

        # Click on the Buy now button
        self.product_details_page_square_apple.click_buy_now_button()

        # Get test message from modal window and compare with expected result, click Pay
        expected_header: str = "Buy Now"
        expected_message: str = "Seller offers only pick up for this product."
        actual_header: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_message: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_message, actual_message)

        self.product_details_page_square_apple.modal_window.click_ok_button()

        # Check the link that we are on the payment page, check if
        # that the page has loaded (there is a Pay button)
        self.payment_page.wait_page_is_present()
        expected_url_payment: str = self.payment_page.expected_url()
        actual_url_payment: str = self.payment_page.get_current_url()

        self.assertEqual(expected_url_payment, actual_url_payment)

        # Go back to the product page
        self.payment_page.click_on_back_from_payment()

        # Click on the Remove from Cart button
        self.product_details_page_square_apple.click_remove_from_cart_button()

        # Get the message test from the modal window and compare with the expected result and click OK
        self.assertTrue(
            self.product_details_page_square_apple.modal_window.is_present()
        )
        expected_header_removed: str = "Success"
        expected_text_removed: str = "This item was removed from your Shopping Cart"
        actual_header_removed: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_text_removed: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_text_removed, actual_text_removed)
        self.assertEqual(expected_header_removed, actual_header_removed)

        self.product_details_page_square_apple.modal_window.click_ok_button()


    def test_13_add_to_watch_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2567

        # The test checks if the product was successfully added to the Watch list

        # Add item to Watch list
        self.product_details_page_square_apple.click_watch_list_button()

        self.product_details_page_square_apple.wait_enabled_watch_list_button()

        # Check that the Watch list button is active
        actual_status = (
            self.product_details_page_square_apple.get_watchlist_button_status()
        )
        self.assertEqual("true", actual_status)

        # Go back to the Shopping List to open the Navigation Menu
        self.product_details_page_square_apple.header.click_bizibaza_logo()

        # Open the Navigation Menu and go to the Watch list page
        self.product_details_page_square_apple.footer.open_the_navigation_menu()
        self.product_details_page_square_apple.navigation_menu.click_watch_list()

        # Check that the page has the product we added
        self.watch_list_page.wait_item_is_present_on_watchlist(self.product)

        # Click on the product and go to its page
        self.watch_list_page.click_on_product_on_watch_list(
            self.product, SQUARE_APPLE_ID
        )

        self.product_details_page_square_apple.wait_enabled_watch_list_button()

        # Check that the Watch list button is active
        actual_status2 = (
            self.product_details_page_square_apple.get_watchlist_button_status()
        )
        self.assertEqual("true", actual_status2)

        # Click on the icon to remove the product from the Watch list
        self.product_details_page_square_apple.click_watch_list_button()

        self.product_details_page_square_apple.wait_disabled_watch_list_button()

        # Check that the Add to Watch list button is disabled
        actual_status3 = (
            self.product_details_page_square_apple.get_watchlist_button_status()
        )
        self.assertEqual("false", actual_status3)


    def test_14_remove_from_watch_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2568

        # The test checks if the product was successfully removed from the Watch list

        # Adding a product to the Watch list
        self.product_details_page_square_apple.click_watch_list_button()

        # Waiting for the icon to change
        self.product_details_page_square_apple.wait_enabled_watch_list_button()

        # Click on the icon to remove a product from the Watch list
        self.product_details_page_square_apple.click_watch_list_button()

        # Waiting for the icon to change
        self.product_details_page_square_apple.wait_disabled_watch_list_button()

        # Check that the icon has changed to Add to Watch list
        actual_status = (
            self.product_details_page_square_apple.get_watchlist_button_status()
        )
        self.assertEqual("false", actual_status)

        # Going back to the Shopping List to open the Navigation Menu
        self.product_details_page_square_apple.header.click_bizibaza_logo()

        # Open the Navigation Menu and go to the Watch list page
        self.product_details_page_square_apple.footer.open_the_navigation_menu()
        self.product_details_page_square_apple.navigation_menu.click_watch_list()

        # # Check if Watch list is empty
        self.watch_list_page.get_watch_list_is_empty()


    def test_15_check_price_alert_switcher_on_off(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2585

        # The test checks that the Price Alert toggle switches from OFF to ON

        # Check that the toggle switch is in the "OFF" position
        actual_switcher: str = (
            self.product_details_page_square_apple.get_disabled_attribute_from_price_alert_field()
        )
        self.assertEqual("true", actual_switcher)
        # Enable Price alert switcher
        self.product_details_page_square_apple.switch_on_off_price_alert()
        # Wait until the Alert QTY field becomes active
        self.product_details_page_square_apple.wait_till_alert_qty_on_or_off("true")
        # Check that the toggle switch is in the "ON" position
        actual_switcher2: str = (
            self.product_details_page_square_apple.get_on_off_switch_status()
        )
        self.assertEqual("true", actual_switcher2)
        # Turn off Price alert switcher by returning to its original position
        self.product_details_page_square_apple.switch_on_off_price_alert()
        # Check that the Alert QTY field becomes inactive
        actual_switcher3: str = (
            self.product_details_page_square_apple.get_disabled_attribute_from_price_alert_field()
        )
        self.assertEqual("true", actual_switcher3)


    def test_16_set_price_alert_valid(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2588

        # The test checks that when you enter a value in the price alert, and the value is lower
        # than the cost of the product, this value is saved and the product is added to the Watch list

        # Enable Price alert switcher
        self.product_details_page_square_apple.switch_on_off_price_alert()
        # Wait until the Alert QTY field becomes active
        self.product_details_page_square_apple.wait_till_alert_qty_on_or_off("true")
        # Check that the toggle switch is in the "ON" position
        actual_switcher: str = (
            self.product_details_page_square_apple.get_on_off_switch_status()
        )
        self.assertEqual("true", actual_switcher)

        # Find the QTY input field, clear it and enter a valid value (less than the cost of the product),
        # press Enter
        self.product_details_page_square_apple.add_price_alert(2.1)

        # Check that the value is saved in the input field
        actual_alert: str = (
            self.product_details_page_square_apple.get_value_from_price_alert_field()
        )
        self.assertEqual("2.1", actual_alert)

        # Wait that the product has been added to WL (the icon has changed)
        self.product_details_page_square_apple.wait_enabled_watch_list_button()

        # Check that the page has a "remove from Watch list" element i.e. product is in WL
        actual_status2 = (
            self.product_details_page_square_apple.get_watchlist_button_status()
        )
        self.assertEqual("true", actual_status2)

        # Turn off Price alert switcher (disabled = true)
        self.product_details_page_square_apple.switch_on_off_price_alert()


    def test_17_set_price_alert_zero(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/7626

        # The test checks that when entering the value "0" in Price Alert, the corresponding error is displayed

        # Enable Price alert switcher
        self.product_details_page_square_apple.switch_on_off_price_alert()
        # Wait until the Alert QTY field becomes active
        self.product_details_page_square_apple.wait_till_alert_qty_on_or_off("true")
        # Check that the toggle switch is in the "ON" position
        actual_switcher: str = (
            self.product_details_page_square_apple.get_on_off_switch_status()
        )
        self.assertEqual("true", actual_switcher)

        #  We find the QTY input field, clear it and enter a zero value, press Enter
        self.product_details_page_square_apple.add_price_alert(0)

        # We get a test message from the modal window, compare it with the expected result, click "Close"
        expected_header: str = "Error:"
        expected_message: str = "Alert value cannot be zero or negative."
        actual_header: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_message: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_message, actual_message)

        self.product_details_page_square_apple.modal_window.click_ok_button()
        # Turn off Price alert switcher (disabled = true) disabled = true
        self.product_details_page_square_apple.switch_on_off_price_alert()


    def test_18_set_price_alert_equal(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2589

        # The test checks that an error is displayed when entering a value for the item's equivalent value.

        # Enable Price alert switcher (disabled = false)
        self.product_details_page_square_apple.switch_on_off_price_alert()

        # Check if "ON" is enabled
        actual_switcher = (
            self.product_details_page_square_apple.get_on_off_switch_status()
        )
        self.assertEqual("true", actual_switcher)

        # Find the QTY input field, clear it and enter a null value, press Enter
        self.product_details_page_square_apple.add_price_alert(2.7)

        # Get test message from modal window, compare with expected result, click "Close"
        expected_header: str = "Error:"
        expected_message: str = "Alert value cannot be equal or higher than the price."
        actual_header: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_message: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_message, actual_message)

        self.product_details_page_square_apple.modal_window.click_ok_button()

        # Turn off Price alert switcher (disabled = true)
        self.product_details_page_square_apple.switch_on_off_price_alert()


    def test_19_set_price_alert_higher(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2590

        # The test checks that when entering a value higher than the price of the product in the Price Alert,
        # the corresponding error is displayed

        # Enable Price alert switcher (disabled = false)
        self.product_details_page_square_apple.switch_on_off_price_alert()

        # Check if "ON" is enabled
        actual_switcher = (
            self.product_details_page_square_apple.get_on_off_switch_status()
        )
        self.assertEqual("true", actual_switcher)

        # Find the QTY input field, clear it and enter a value that exceeds the cost of the product, press Enter
        self.product_details_page_square_apple.add_price_alert(3.7)

        # Get test message from modal window, compare with expected result, click "Close"
        expected_header: str = "Error:"
        expected_message: str = "Alert value cannot be equal or higher than the price."
        actual_header: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_message: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_message, actual_message)

        self.product_details_page_square_apple.modal_window.click_ok_button()

        # Turn off Price alert switcher (disabled = true) disabled = true
        self.product_details_page_square_apple.switch_on_off_price_alert()


    def test_20_set_price_alert_invalid(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2591

        # The test checks that invalid characters cannot be entered in the Price alert field.

        # Enable Price alert switcher (disabled = false)
        self.product_details_page_square_apple.switch_on_off_price_alert()

        # Check if "ON" is enabled
        actual_switcher = (
            self.product_details_page_square_apple.get_on_off_switch_status()
        )
        self.assertEqual("true", actual_switcher)

        # Find the QTY input field, clear it and enter an invalid value - special characters, letters, press Enter
        self.product_details_page_square_apple.add_price_alert("@#$%asdfg")

        # Get test message from modal window, compare with expected result, click "Close"
        expected_header: str = "Error:"
        expected_message: str = "Alert value cannot be zero or negative."
        actual_header: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_message: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_message, actual_message)

        self.product_details_page_square_apple.modal_window.click_ok_button()

        # Turn off Price alert switcher (disabled = true) disabled = true
        self.product_details_page_square_apple.switch_on_off_price_alert()


    def test_21_disabling_price_alert_removes_item_from_wl(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2593

        # The test checks that when the Price Alert is turned off,
        # the product is automatically removed from the Watch list.
        # Go to the product page

        # Enable Price alert switcher (disabled = false)
        self.product_details_page_square_apple.switch_on_off_price_alert()

        # Check if "ON" is enabled
        actual_switcher = (
            self.product_details_page_square_apple.get_on_off_switch_status()
        )
        self.assertEqual("true", actual_switcher)

        # Find the QTY input field, clear it and enter a valid value (less than the cost of the product),
        # press Enter
        self.product_details_page_square_apple.add_price_alert(1.5)

        # Check that the value is saved in the input field
        actual_alert = (
            self.product_details_page_square_apple.get_value_from_price_alert_field()
        )
        self.assertEqual("1.5", actual_alert)

        # Wait that the product has been added to WL (the icon has changed)
        self.product_details_page_square_apple.wait_enabled_watch_list_button()

        # Check that the page has a "remove from Watch list" element i.e. product is in WL
        actual_status2 = (
            self.product_details_page_square_apple.get_watchlist_button_status()
        )
        self.assertEqual("true", actual_status2)

        # Turn off Price alert switcher (disabled = true) disabled = true
        self.product_details_page_square_apple.switch_on_off_price_alert()

        # Waiting for the icon to change
        self.product_details_page_square_apple.wait_disabled_watch_list_button()

        # Check that the icon has changed to Add to Watch list
        actual_status = (
            self.product_details_page_square_apple.get_watchlist_button_status()
        )
        self.assertEqual("false", actual_status)

        # Go back to the Shopping List to open the Navigation Menu
        self.product_details_page_square_apple.header.click_bizibaza_logo()

        # Open the Navigation Menu and go to the Watch list page
        self.product_details_page_square_apple.footer.open_the_navigation_menu()
        self.product_details_page_square_apple.navigation_menu.click_watch_list()

        # Check if Watch list is empty
        self.watch_list_page.get_watch_list_is_empty()


    def test_22_removing_item_from_wl_disables_price_alert(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2594

        # The test checks that when removed from the Watch list, the Price Alert toggle switch turns off

        # Enable Price alert switcher (disabled = false)
        self.product_details_page_square_apple.switch_on_off_price_alert()

        # Check if "ON" is enabled
        actual_switcher = (
            self.product_details_page_square_apple.get_on_off_switch_status()
        )
        self.assertEqual("true", actual_switcher)

        # Find the QTY input field, clear it and enter a valid value (less than the cost of the product),
        # press Enter
        self.product_details_page_square_apple.add_price_alert(1.5)

        # Check that the value is saved in the input field
        actual_alert = (
            self.product_details_page_square_apple.get_value_from_price_alert_field()
        )
        self.assertEqual("1.5", actual_alert)

        # Wait that the product has been added to WL (the icon has changed)
        self.product_details_page_square_apple.wait_enabled_watch_list_button()

        # Check that the page has a "remove from Watch list" element i.e. product is in WL
        actual_status2 = (
            self.product_details_page_square_apple.get_watchlist_button_status()
        )
        self.assertEqual("true", actual_status2)

        # Click on the icon to remove the product from the Watch list
        self.product_details_page_square_apple.click_watch_list_button()

        # Get test message from modal window, compare with expected result, press "No"
        expected_header: str = "Alert:"
        expected_message: str = (
            "Removing this item from watch list will turn off its price alert. "
            "Do you wish to proceed?"
        )
        actual_header: str = (
            self.product_details_page_square_apple.modal_window.get_modal_title_text()
        )
        actual_message: str = (
            self.product_details_page_square_apple.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_message, actual_message)

        self.product_details_page_square_apple.modal_window.click_no_button()

        # Wait that the product has been removed from WL
        self.product_details_page_square_apple.wait_enabled_watch_list_button()

        # Check that the page has a "remove from Watch list" element i.e. product is in WL
        actual_status3 = (
            self.product_details_page_square_apple.get_watchlist_button_status()
        )
        self.assertEqual("true", actual_status3)

        # Again, click on the icon to remove the product from the Watch list
        self.product_details_page_square_apple.click_watch_list_button()

        # Get test message from modal window, compare with expected result, click "Yes"
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_message, actual_message)

        self.product_details_page_square_apple.modal_window.click_ok_button()

        # Check that the switch is in the "OFF" position
        actual_switcher = (
            self.product_details_page_square_apple.get_on_off_switch_status()
        )
        self.assertEqual("false", actual_switcher)

        # Check that the icon has changed to Add to Watch list
        self.product_details_page_square_apple.get_watch_list_button()


    def test_23_add_item_to_favorites_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2573

        # The test checks if the product was successfully added to the Favorites list

        # Add product to Favorites list
        self.product_details_page_square_apple.click_favorites_list_button()

        self.product_details_page_square_apple.wait_enabled_fav_list_button()

        # Check that the page has a button remove from Favorites list
        actual_status = (
            self.product_details_page_square_apple.get_favlist_button_status()
        )
        self.assertEqual("true", actual_status)

        # Go back to the Shopping List to open the Navigation Menu
        self.product_details_page_square_apple.header.click_bizibaza_logo()

        # Open the Navigation Menu and go to the Favorites list page
        self.product_details_page_square_apple.footer.open_the_navigation_menu()
        self.product_details_page_square_apple.navigation_menu.click_favorites_list()

        # Check that the page has the product we added
        self.favorites_list_page.wait_item_is_present_on_favorites_list(self.product)

        # Click on the product and go to its page
        self.favorites_list_page.click_on_product_on_the_favorites_list(
            self.product, SQUARE_APPLE_ID
        )

        # Check if there is an element on the page to remove from the Favorites list i.e. product is in FL)
        actual_status2 = (
            self.product_details_page_square_apple.get_favlist_button_status()
        )
        self.assertEqual("true", actual_status2)

        # Click on the icon to remove the product from the Favorites list
        self.product_details_page_square_apple.click_favorites_list_button()

        self.product_details_page_square_apple.wait_disabled_fav_list_button()

        # Check that the icon has changed to Add to Favorites list
        actual_status3 = (
            self.product_details_page_square_apple.get_favlist_button_status()
        )
        self.assertEqual("false", actual_status3)


    def test_24_remove_item_from_favorites_list(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2574

        # The test checks if the product was successfully removed from the Favorites list

        # Add product to Favorites list
        self.product_details_page_square_apple.click_favorites_list_button()

        # Wait till button is changed
        self.product_details_page_square_apple.wait_enabled_fav_list_button()

        # Click again on the Fav list button to remove product from Favorites
        self.product_details_page_square_apple.click_favorites_list_button()

        # Wait till button is changed to disabled
        self.product_details_page_square_apple.wait_disabled_fav_list_button()

        # Check that the icon has changed to Add to Favorites list
        actual_status = (
            self.product_details_page_square_apple.get_favlist_button_status()
        )
        self.assertEqual("false", actual_status)

        # Go back to the Shopping List to open the Navigation Menu
        self.product_details_page_square_apple.header.click_bizibaza_logo()

        # Open the Navigation Menu and Go to the Favorites list page
        self.product_details_page_square_apple.footer.open_the_navigation_menu()
        self.product_details_page_square_apple.navigation_menu.click_favorites_list()

        # Check if Favorites list is empty
        self.favorites_list_page.wait_that_favorites_list_is_empty(self.product)


    def test_25_open_bizipix_viewer(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2571

        # Open BiziPix Viewer
        self.product_details_page_square_apple.click_on_bizipix_viewer()
        # Check that the BiziPix Viewer page has loaded and the link is correct
        expected_url_bizipix_viewer: str = self.bizipix_viewer_page.expected_url()
        actual_url_bizipix_viewer: str = self.bizipix_viewer_page.get_current_url()
        self.assertEqual(expected_url_bizipix_viewer, actual_url_bizipix_viewer)
        # Close BiziPix Viewer
        self.bizipix_viewer_page.close_bizipix_viewer()


    def test_26_open_product_reviews(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2583

        # Open Product Reviews
        self.product_details_page_square_apple.click_on_product_reviews()

        # Check that the Product Reviews page has loaded and the link is correct
        self.product_reviews_page.wait_product_reviews_page_loaded(self.product)
        expected_url_product_reviews: str = self.product_reviews_page.expected_url()
        actual_url_product_reviews: str = self.product_reviews_page.get_current_url()
        self.assertIn(expected_url_product_reviews, actual_url_product_reviews)
        # Go back
        self.product_reviews_page.click_back_button()

    def test_27_open_seller_details(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2584
        # Open Seller Details
        self.product_details_page_square_apple.click_on_seller_details()
        # Check that the Seller Details page has loaded and the link is valid
        self.seller_details_page.wait_page_is_present()

        seller_details_expected_url: str = self.seller_details_page.expected_url()
        seller_details_actual_url: str = self.seller_details_page.get_current_url()
        self.assertIn(seller_details_expected_url, seller_details_actual_url)
        # Go back
        self.seller_details_page.click_back_button()

    #@unittest.skip("bug report")
    def test_28_open_the_orders_page(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2555

        # Open the Orders page
        self.product_details_page_square_apple.header.click_on_orders_button()
        # Check if the page has loaded
        self.orders_page.wait_page_is_present()
        # Check that the link is correct, and we are on the Orders page
        expected_url_orders: str = self.orders_page.expected_url()
        actual_url_orders: str = self.orders_page.get_current_url()
        self.assertEqual(expected_url_orders, actual_url_orders)


    def test_29_open_the_cart_page(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2556

        # Open the Cart page
        self.product_details_page_square_apple.header.click_on_the_cart()
        # Check that the Cart has loaded
        self.cart_page.wait_page_is_present()
        # Check that the link is correct, and we are on the cart page
        expected_url_cart: str = self.cart_page.expected_url()
        actual_url_cart: str = self.cart_page.get_current_url()

        self.assertEqual(expected_url_cart, actual_url_cart)


class TestProductDetailsPageSelf(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()

        cls.product_details_page_cheddar = (
            BaseTestCase.page_factory().product_details_page(CHEDDAR_SALE_ID)
        )

        BaseTestCase.sign_in_via_api(get_environment().get_seller_user().account)
        cls.shopping_list_page.open()

    def setUp(self) -> None:
        # Runs before two test test_11 and test_12 where using ourselves product
        super().setUp()

        # Go to the product page
        self.product_details_page_cheddar.open()

    def tearDown(self) -> None:
        # Executed after each test!
        super().tearDown()

    @classmethod
    def after_all_tests(cls) -> None:
        BaseTestCase.logout_via_api()
        super().after_all_tests()


    def test_11_add_to_cart_from_yourself(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2609
        # The test checks that clicking the "Add to Cart" button on your own product displays an error

        # Find the QTY input field, clear it and enter the quantity of goods that is definitely available for purchase
        self.product_details_page_cheddar.add_quantity(10)

        # Get the value from the QTY field and compare the expected value with the received one
        actual_qty: str = (
            self.product_details_page_cheddar.get_value_from_quantity_field()
        )
        self.assertEqual("10", actual_qty)

        # Click on the button "Add to Cart""
        self.product_details_page_cheddar.click_add_to_cart_button()

        # Get test message from modal window and compare with expected result and press "CLOSE"
        expected_header_added: str = "Error:"
        expected_text_added: str = "You cannot buy from yourself!"
        actual_header_added: str = (
            self.product_details_page_cheddar.modal_window.get_modal_title_text()
        )
        actual_text_added: str = (
            self.product_details_page_cheddar.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_added, actual_header_added)
        self.assertEqual(expected_text_added, actual_text_added)

        self.product_details_page_cheddar.modal_window.click_ok_button()


    def test_12_buy_now_from_yourself(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/8518
        # The test checks that clicking the "Buy now" button on your product page displays an error

        # Find the QTY input field, clear it and enter the quantity of goods that is definitely available for purchase
        self.product_details_page_cheddar.add_quantity(10)

        # Get the value from the QTY field and compare the expected value with the received one
        actual_qty: str = (
            self.product_details_page_cheddar.get_value_from_quantity_field()
        )
        self.assertEqual("10", actual_qty)

        # Click on the Buy now button
        self.product_details_page_cheddar.click_buy_now_button()

        # Get test message from modal window, compare with expected result, press "Pay"
        expected_header: str = "Buy Now"
        expected_message: str = "Seller offers only pick up for this product."
        actual_header: str = (
            self.product_details_page_cheddar.modal_window.get_modal_title_text()
        )
        actual_message: str = (
            self.product_details_page_cheddar.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_message, actual_message)

        self.product_details_page_cheddar.modal_window.click_ok_button()

        # Get the message test from the modal window, compare it with the expected result and click "Close"
        expected_header_added: str = "Error:"
        expected_text_added: str = "You cannot buy from yourself!"
        actual_header_added: str = (
            self.product_details_page_cheddar.modal_window.get_modal_title_text()
        )
        actual_text_added: str = (
            self.product_details_page_cheddar.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_added, actual_header_added)
        self.assertEqual(expected_text_added, actual_text_added)

        self.product_details_page_cheddar.modal_window.click_ok_button()
