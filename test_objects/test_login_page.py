from test_objects.base_test_case import BaseTestCase


class TestLogInPage(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.login_page = BaseTestCase.page_factory().login_page()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.settings_page = BaseTestCase.page_factory().settings_page()

    def setUp(self) -> None:
        super().setUp()
        self.login_page.open()

    def tearDown(self) -> None:
        super().tearDown()
        # Log out ->
        # Click on the Settings button
        self.shopping_list_page.click_settings_button()
        # Waiting for loading Settings page
        self.settings_page.wait_page_is_present()
        # Click on the Log Out button
        self.settings_page.click_log_out_button()
        # Click on the "Yes" button
        self.settings_page.click_yes_button_on_logout_confirmation()

    @classmethod
    def after_all_tests(cls) -> None:
        super().after_all_tests()

    def test_login_success(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/236
        # Login with valid data
        self.login_page.login_action("SellerTest_0001", "Ss123456")

        # Checking that a modal window did not appear reporting an error
        self.assertTrue(self.login_page.modal_window.is_not_present())
        # Waiting for loading Shopping list page
        self.shopping_list_page.wait_page_is_present()
        # Check  url /shopping-list
        expected_url_shopping_list: str = self.shopping_list_page.expected_url()
        actual_url_shopping_list: str = self.shopping_list_page.get_current_url()
        self.assertEqual(expected_url_shopping_list, actual_url_shopping_list)

        # Checking that Login and Sign Up links are NOT on the page
        self.assertFalse(
            self.shopping_list_page.is_visible_login_link(),
            msg="The Login link is displayed",
        )
        self.assertFalse(
            self.shopping_list_page.is_visible_get_sign_up_link(),
            msg="The Sign Up link is displayed",
        )
