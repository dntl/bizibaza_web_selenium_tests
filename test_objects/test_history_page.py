from page_objects.history_page import HistoryPageLocators

from test_objects.base_test_case import BaseTestCase
from factory import get_environment


class TestHistoryPages(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""

        super().before_all_tests()
        cls.cart_page = BaseTestCase.page_factory().cart_page()
        cls.chat_page = BaseTestCase.page_factory().chat_page()
        cls.orders_page = BaseTestCase.page_factory().orders_page()
        cls.history_page = BaseTestCase.page_factory().history_page()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()

        BaseTestCase.sign_in_via_api(
            get_environment().get_buyer_user_with_history().account
        )
        # Go to the product page directly.
        cls.shopping_list_page.open()

    @classmethod
    def after_all_tests(cls) -> None:
        BaseTestCase.logout_via_api()
        super().after_all_tests()

    def test_1_open_and_close_purchase_record(self) -> None:
        """""
        https://dntlab.testrail.io/index.php?/cases/view/1080
        [TC-BH] Open and close the purchase record
        """ ""

        self.shopping_list_page.footer.open_the_navigation_menu()
        self.shopping_list_page.navigation_menu.click_orders()

        # Open the Buyer history page
        self.orders_page.click_history_button()

        self.history_page.click_on_arrow_to_expand_history_element_by_index(0)

        self.assertTrue(
            self.history_page.is_visible(HistoryPageLocators.SHOW_RECORD_DETAILS),
            "The history record is not expanded on the page.",
        )

        self.history_page.click_on_arrow_to_collapse_history_element_by_index(0)

        self.assertFalse(
            self.history_page.is_visible(HistoryPageLocators.SHOW_RECORD_DETAILS),
            "The history record is expanded on the page.",
        )

        self.history_page.click_on_arrow_to_expand_history_element_by_index(0)

        self.assertTrue(
            self.history_page.is_visible(HistoryPageLocators.SHOW_RECORD_DETAILS),
            "The history record is not expanded on the page.",
        )

    def test_2_open_new_dialogue_with_seller(self) -> None:
        """""
        https://dntlab.testrail.io/index.php?/cases/view/1085
        [TC-BH] New dialogue is created if it is the first conversation with Seller
        """ ""

        # Click on the message icon in the first history element
        self.history_page.click_on_contact_button_by_index(0)

        # Check that the link is correct, and we are on the Chat Page
        self.assertTrue(self.chat_page.is_current_url())

        self.chat_page.click_the_back_button_from_chat()

    def test_3_open_the_cart_page(self) -> None:
        """""
        https://dntlab.testrail.io/index.php?/cases/view/1073
        [TC-BH] Open the "Cart" link.
        """ ""
        # Open the Cart page
        self.history_page.header.click_on_the_cart()
        # Check that the Cart has loaded
        self.cart_page.wait_page_is_present()
        # Check that the link is correct, and we are on the cart page
        self.assertTrue(self.cart_page.is_current_url())

        self.cart_page.header.click_bizibaza_logo()

    def test_4_open_the_bizibaza_link_logo(self) -> None:
        """""
        https://dntlab.testrail.io/index.php?/cases/view/1072
        [TC-BH] Open "BiziBAZA" link-logo.
        """ ""

        self.shopping_list_page.footer.open_the_navigation_menu()
        self.shopping_list_page.navigation_menu.click_orders()

        # Open the Buyer history page
        self.orders_page.click_history_button()

        self.history_page.header.click_bizibaza_logo()

        # Check that the link is correct, and we are on the Shopping list page
        self.assertTrue(self.shopping_list_page.is_current_url())

    def test_5_click_on_the_orders_icon(self) -> None:
        """""
        https://dntlab.testrail.io/index.php?/cases/view/1074
        [TC-BH] Open the "Orders" link.
        """ ""

        self.shopping_list_page.footer.open_the_navigation_menu()
        self.shopping_list_page.navigation_menu.click_orders()

        # Open the Buyer history page
        self.orders_page.click_history_button()

        # Click on the Orders icon on the Header
        self.history_page.header.click_on_orders_button()

        # Check that the link is correct, and we are on the Orders page
        self.assertTrue(self.orders_page.is_current_url())

    def test_6_return_to_the_orders_page(self) -> None:
        """""
        https://dntlab.testrail.io/index.php?/cases/view/1075
        [TC-BH] Open the "Orders" link.
        """ ""

        # Open the Buyer history page
        self.orders_page.click_history_button()

        # Get back
        self.history_page.click_back_button()

        # Check that the link is correct, and we are on the Orders page
        self.assertTrue(self.orders_page.is_current_url())
