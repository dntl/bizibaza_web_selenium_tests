import unittest

import pytest

from test_objects.base_test_case import BaseTestCase
from factory import get_environment

SQUARE_APPLE_ID = "4dbb9e09f29644eb9e672ffdb0571f4e"


@pytest.mark.skip(reason="https://dntlab.atlassian.net/browse/BIZ-4499")
class TestCartPageTimer(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.search_page = BaseTestCase.page_factory().search_page()
        cls.product_details_page_square_apple = (
            BaseTestCase.page_factory().product_details_page(SQUARE_APPLE_ID)
        )
        cls.cart_page = BaseTestCase.page_factory().cart_page()
        cls.payment_page = BaseTestCase.page_factory().payment_page()
        cls.chat_page = BaseTestCase.page_factory().chat_page()
        BaseTestCase.sign_in_via_api(get_environment().get_buyer_user_with_sh().account)
        cls.cart_page.open()

        # Temporarily added a method to explicitly wait for a full basket,
        # because the ANY_OF (wait_page_is_present) universal method is not suitable
        # for waiting for a full Cart due to the fact that the page is loaded first
        # with an empty Cart, and then with a full one.
        # Because of this, the wait_page_is_present is not work correctly.
        cls.cart_page.wait_full_cart_page_loaded()

    def setUp(self) -> None:
        # Executed in every test
        super().setUp()

    def tearDown(self) -> None:
        super().tearDown()

    @classmethod
    def after_all_tests(cls) -> None:
        BaseTestCase.logout_via_api()
        super().after_all_tests()

    @unittest.skip("https://dntlab.atlassian.net/browse/BIZ-4499")
    def test_01_try_to_pay_with_expired_timer(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2393

        # Check that "Refresh" button is displayed
        self.assertTrue(self.cart_page.visible_refresh_button())

        # Check that timer is expired
        actual_timer_value: str = self.cart_page.get_expire_time_text()
        expected_timer: str = "00:00"
        self.assertEqual(expected_timer, actual_timer_value)

        # Click on the "Pay" button

        self.cart_page.click_on_pay_button()

        # Checking text on Modal window expired booking
        expected_header_alert: str = "Alert:"
        expected_text_alert: str = (
            "Some of the items have their booking expired!"
            " Please, refresh their status and try again."
        )
        actual_header_alert: str = self.cart_page.modal_window.get_modal_title_text()
        actual_text_alert: str = self.cart_page.modal_window.get_modal_message_text()
        self.assertEqual(expected_header_alert, actual_header_alert)
        self.assertEqual(expected_text_alert, actual_text_alert)

        self.cart_page.modal_window.click_ok_button()

    @unittest.skip("https://dntlab.atlassian.net/browse/BIZ-4499")
    def test_02_refresh_timer_and_go_to_pay(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/2426

        # Click on the "Refresh button"
        self.cart_page.click_on_refresh_button()

        self.assertFalse(self.cart_page.visible_refresh_button())

        # Check that timer is NOT expired
        actual_timer_value: str = self.cart_page.get_expire_time_text()
        expected_timer: str = "00:00"

        self.assertNotEqual(expected_timer, actual_timer_value)

        # Click on the "Pay" button
        self.cart_page.click_on_pay_button()

        # Check that the link is correct, and we are on the Payment page
        expected_url_payment: str = self.payment_page.expected_url()
        actual_url_payment: str = self.payment_page.get_current_url()
        self.assertEqual(expected_url_payment, actual_url_payment)
