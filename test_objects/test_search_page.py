from decimal import Decimal

from test_objects.base_test_case import BaseTestCase
from model.my_search_results import SearchResult


class TestSearchPage(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.login_page = BaseTestCase.page_factory().login_page()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.search_page = BaseTestCase.page_factory().search_page()

    def setUp(self) -> None:
        super().setUp()

        self.data = self.env.get_sign_in_with_api_test_data()
        self.sign_in_via_api(self.data.account)
        self.shopping_list_page.open()

    def tearDown(self) -> None:
        super().tearDown()
        self.logout_via_api()

    @classmethod
    def after_all_tests(cls) -> None:
        super().after_all_tests()

    def test_search_by_keyword(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/413
        # Waiting for loading Shopping list page
        self.shopping_list_page.wait_page_is_present()

        # Click on the Search icon
        self.search_page.click_search_button()

        # Click on the Search field, type product name and submit
        self.search_page.search_by_keyword("mustard")

        self.search_page.sort_items("ascending")

        current_page_search_results: list[
            SearchResult
        ] = self.search_page.get_current_page_search_results_price()

        # Check that the list of search results is not empty, i.e. is not equal to zero.
        assert len(current_page_search_results) != 0, "Search results are empty"

        # We want to check that the prices are sorted in ascending order.
        # The first step is to get a list of all prices.
        prices: list[Decimal] = []
        for result in current_page_search_results:
            prices.append(result.price)

        # Comparing that the list of price tags is sorted in non-descending order
        self.assertEqual(sorted(prices), prices)

        # Task: find a result with a given price in the list of searched objects.
        found: bool = False
        for result in current_page_search_results:
            if result.price == Decimal("5.99"):
                found = True
                break

        self.assertTrue(found)
