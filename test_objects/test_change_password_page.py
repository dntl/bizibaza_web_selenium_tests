from factory import get_environment
from test_objects.base_test_case import BaseTestCase

NEW_PASSWORD: str = "Test1234"
OLD_PASSWORD: str = "Test123456"
USERNAME: str = "buyer_sh_list"


class TestChangePasswordPage(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.login_page = BaseTestCase.page_factory().login_page()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.settings_page = BaseTestCase.page_factory().settings_page()
        cls.change_password_page = BaseTestCase.page_factory().change_password_page()
        BaseTestCase.sign_in_via_api(get_environment().get_buyer_user_with_sh().account)
        cls.shopping_list_page.open()

    @classmethod
    def after_all_tests(cls) -> None:
        super().after_all_tests()
        cls.shopping_list_page.click_settings_button()
        cls.settings_page.click_change_password()
        # Enter the old password, the new one and repeat the new one
        cls.change_password_page.set_old_password(NEW_PASSWORD)
        cls.change_password_page.set_new_password(OLD_PASSWORD)
        cls.change_password_page.set_repeat_password(OLD_PASSWORD)
        cls.change_password_page.click_to_open_old_password_eye()
        cls.change_password_page.click_on_change_password_button()

    def test_01_change_password(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/396
        # [TC-CP] Successful change of password

        # Open Setting page
        self.shopping_list_page.click_settings_button()
        # Open Change Password page
        self.settings_page.click_change_password()
        # Enter the old password, the new one and repeat the new one
        self.change_password_page.set_old_password(OLD_PASSWORD)
        self.change_password_page.set_new_password(NEW_PASSWORD)
        self.change_password_page.set_repeat_password(NEW_PASSWORD)

        # Click on the button in the form of an eye to show the old password.
        # This is necessary so that the Change password button can be activated.
        # (The button is activated only after clicking anywhere on the page)
        self.change_password_page.click_to_open_old_password_eye()

        # We request the status of the Change password button to check that it is activated.
        actual_button_state = (
            self.change_password_page.get_status_of_change_password_button()
        )
        expected_button_state: bool = False
        # Check that disabled value is not True
        self.assertNotEqual(expected_button_state, actual_button_state)

        self.change_password_page.click_on_change_password_button()

        self.assertTrue(self.change_password_page.modal_window.is_present())

        # Get the message text from the modal window and compare with the expected result and click OK
        expected_header_added: str = "Password successfully changed"
        actual_header_added: str = (
            self.change_password_page.modal_window.get_modal_title_text()
        )
        self.assertEqual(expected_header_added, actual_header_added)

        self.change_password_page.click_ok_button_in_change_pass_successful()

    def test_02_login_with_old_password(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/399
        # [TC-CP] Login with old password after changing password

        self.settings_page.click_log_out_button()

        self.settings_page.click_yes_button_on_logout_confirmation()

        self.shopping_list_page.click_log_in_link_button()

        self.login_page.login_action(USERNAME, OLD_PASSWORD)

        self.assertTrue(self.login_page.modal_window.is_present())

        expected_header: str = "Unable to login:"
        expected_text: str = "Incorrect password"
        actual_header: str = self.login_page.modal_window.get_modal_title_text()
        actual_text: str = self.login_page.modal_window.get_modal_message_text()
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_text, actual_text)

        self.login_page.modal_window.click_ok_button()

        self.login_page.clear_username_field()

    def test_03_login_with_new_password(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/10659
        # [TC-CP] Login with new password after changing password

        self.login_page.login_action(USERNAME, NEW_PASSWORD)
        # Checking that a modal window did not appear reporting an error
        self.assertTrue(
            self.login_page.modal_window.is_not_present(),
            msg="The Error message is displayed!",
        )
        # Waiting for loading Shopping list page
        self.shopping_list_page.wait_page_is_present()

        # Checking that Login and Sign Up links are NOT on the page
        self.assertFalse(
            self.shopping_list_page.is_visible_login_link(),
            msg="The Login link is displayed",
        )
        self.assertFalse(
            self.shopping_list_page.is_visible_get_sign_up_link(),
            msg="The Sign Up link is displayed",
        )
