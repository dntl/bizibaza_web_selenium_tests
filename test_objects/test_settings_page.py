from factory import get_environment
from test_objects.base_test_case import BaseTestCase


class TestSettingsPage(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""
        super().before_all_tests()
        cls.login_page = BaseTestCase.page_factory().login_page()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.settings_page = BaseTestCase.page_factory().settings_page()
        cls.change_password_page = BaseTestCase.page_factory().change_password_page()
        cls.profile_page = BaseTestCase.page_factory().profile_page()
        BaseTestCase.sign_in_via_api(get_environment().get_buyer_user().account)

        # Open the Shopping list page
        cls.shopping_list_page.open()

    def setUp(self) -> None:
        super().setUp()
        # Executed in every test

    def tearDown(self) -> None:
        super().tearDown()
        # Return to Shopping list
        self.shopping_list_page.header.click_bizibaza_logo()

    @classmethod
    def after_all_tests(cls) -> None:
        super().after_all_tests()

    def test_1_open_change_password_page(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/367

        # Click on the Settings button
        self.shopping_list_page.click_settings_button()

        # Check url /settings
        expected_url_settings: str = self.settings_page.expected_url()
        actual_url_settings: str = self.settings_page.get_current_url()
        self.assertEqual(expected_url_settings, actual_url_settings)

        # Open the Change Password
        self.settings_page.click_change_password()

        # Check url /change-password
        expected_url_change_password: str = self.change_password_page.expected_url()
        actual_url_change_password: str = self.change_password_page.get_current_url()
        self.assertEqual(expected_url_change_password, actual_url_change_password)

    def test_2_open_profile_page(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/366

        # Click on the Settings button
        self.shopping_list_page.click_settings_button()

        # Check url /settings
        expected_url_settings: str = self.settings_page.expected_url()
        actual_url_settings: str = self.settings_page.get_current_url()
        self.assertEqual(expected_url_settings, actual_url_settings)

        # Open the Change Password
        self.settings_page.click_update_profile()

        # Waiting for loading Profile page
        self.profile_page.wait_page_is_present()

        # Check url /profile
        expected_url_profile: str = self.profile_page.expected_url()
        actual_url_profile: str = self.profile_page.get_current_url()
        self.assertEqual(expected_url_profile, actual_url_profile)

    def test_3_click_delete_account_and_cancel(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/8515
        # Click on the Settings button
        self.shopping_list_page.click_settings_button()

        # Waiting for loading Settings page
        self.settings_page.wait_page_is_present()

        # Check url /settings
        expected_url_settings: str = self.settings_page.expected_url()
        actual_url_settings: str = self.settings_page.get_current_url()
        self.assertEqual(expected_url_settings, actual_url_settings)

        # Click on the "Delete account" button
        self.settings_page.click_delete_account()

        # Check that modal window is displayed
        self.assertTrue(
            self.settings_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Checking text on Modal window about deleting account
        expected_header_logout: str = "Alert:"
        expected_text_logout: str = (
            "Are you sure you want to completely delete your BiziBAZA "
            "account along with all related data? This action cannot be undone."
        )
        actual_header_logout: str = (
            self.settings_page.modal_window.get_modal_title_text()
        )
        actual_text_logout: str = (
            self.settings_page.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_logout, actual_header_logout)
        self.assertEqual(expected_text_logout, actual_text_logout)

        self.settings_page.modal_window.click_no_button()

        # Check that the Logout button is displayed, which means we are not logged out,
        # and deleting the account has not happened.
        self.assertTrue(self.settings_page.visible_logout_button())

    def test_4_logout_success(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/370

        # Click on the Settings button
        self.shopping_list_page.click_settings_button()

        # Check url  /settings
        expected_url_settings: str = self.settings_page.expected_url()
        actual_url_settings: str = self.settings_page.get_current_url()
        self.assertEqual(expected_url_settings, actual_url_settings)

        # Click on the Log Out button
        self.settings_page.click_log_out_button()

        # Checking text on Modal window "Do you want to logout?"
        expected_header_logout: str = "Logout:"
        expected_text_logout: str = "Do you want to logout?"
        actual_header_logout: str = (
            self.settings_page.modal_window.get_modal_title_text()
        )
        actual_text_logout: str = (
            self.settings_page.modal_window.get_modal_message_text()
        )
        self.assertEqual(expected_header_logout, actual_header_logout)
        self.assertEqual(expected_text_logout, actual_text_logout)

        # Click on the Cancel button
        self.settings_page.modal_window.click_no_button()

        # Click on the Log Out button again
        self.settings_page.click_log_out_button()

        # Click on the "Yes" button
        self.settings_page.click_yes_button_on_logout_confirmation()

        # Check url /shopping-list
        expected_url_shopping_list: str = self.shopping_list_page.expected_url()
        actual_url_shopping_list: str = self.shopping_list_page.get_current_url()
        self.assertEqual(expected_url_shopping_list, actual_url_shopping_list)

        # Checking that Login and Sign Up links are on the page
        self.assertTrue(
            self.shopping_list_page.is_visible_login_link(),
            msg="The Log in link is not displayed",
        )
        self.assertTrue(
            self.shopping_list_page.is_visible_get_sign_up_link(),
            msg="The Sign Up link is not displayed",
        )

        # # We log in again so that the rest of the tests can pass successfully.
        # BaseTestCase.sign_in_via_api(get_environment().get_seller_user().account)
        # # Open the Shopping list page
        # self.shopping_list_page.open()
