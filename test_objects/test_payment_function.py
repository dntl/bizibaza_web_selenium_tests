from test_objects.base_test_case import BaseTestCase
from factory import get_environment
from decimal import Decimal
from parameterized import parameterized  # type: ignore

"""""
For details about testing credit cards see the link: https://stripe.com/docs/testing

""" ""

VISA_TEST_CARD: str = "4242424242424242"
MASTERCARD_2_SERIES: str = "2223003122003222"
AMERICAN_EXPRESS: str = "371449635398431"
DISCOVER_TEST_CARD: str = "6011111111111117"
DINERS_CLUB: str = "3056930009020004"
DINERS_CLUB_14: str = "36227206271667"
JCB_CARD: str = "3566002020360505"
UNION_PAY: str = "6200000000000005"
UNION_PAY_DEBIT: str = "6200000000000047"
UNION_PAY_19_DIGITS: str = "6205500000000000004"

EXP_DATE: float = 10 / 28
CVC: int = 123
CVC_4_DIGITS: int = 1234
NAME_ON_CARD: str = "Automated TEST Name"

CHEDDAR_SALE_ID: str = "6aa6ed05eb7e41bc9a7fab40e3ce663a"


class TestPaymentFunction(BaseTestCase):
    @classmethod
    def before_all_tests(cls) -> None:
        """It's done once before all tests."""

        super().before_all_tests()
        cls.shopping_list_page = BaseTestCase.page_factory().shopping_list_page()
        cls.search_page = BaseTestCase.page_factory().search_page()
        cls.product_details_page_cheddar_sale = (
            BaseTestCase.page_factory().product_details_page(CHEDDAR_SALE_ID)
        )
        cls.cart_page = BaseTestCase.page_factory().cart_page()
        cls.payment_page = BaseTestCase.page_factory().payment_page()
        cls.chat_page = BaseTestCase.page_factory().chat_page()
        cls.cart_report_page = BaseTestCase.page_factory().cart_report_page()
        BaseTestCase.sign_in_via_api(get_environment().get_buyer_user().account)

    def setUp(self) -> None:
        # Executed in every test
        super().setUp()
        self.product_details_page_cheddar_sale.open()

    def tearDown(self) -> None:
        super().tearDown()
        # Return to Shopping list
        self.product_details_page_cheddar_sale.header.click_bizibaza_logo()

    @classmethod
    def after_all_tests(cls) -> None:
        BaseTestCase.logout_via_api()
        super().after_all_tests()

    @parameterized.expand(
        [
            (
                "C2795",
                VISA_TEST_CARD,
            ),
            (
                "C2797",
                MASTERCARD_2_SERIES,
            ),
            (
                "C2798",
                AMERICAN_EXPRESS,
            ),
            (
                "C2799",
                DISCOVER_TEST_CARD,
            ),
            (
                "C2800",
                DINERS_CLUB,
            ),
            (
                "C2801",
                DINERS_CLUB_14,
            ),
            (
                "C2802",
                JCB_CARD,
            ),
            (
                "C2803",
                UNION_PAY,
            ),
            (
                "C10644",
                UNION_PAY_DEBIT,
            ),
            (
                "C10645",
                UNION_PAY_19_DIGITS,
            ),
        ]
    )
    def test_make_payment_using_various_test_cards(
        self, automation_id: str, card_number: str
    ) -> None:
        expected_total: Decimal = Decimal("8.90")
        # Enter nuber of items to the QTY field
        self.product_details_page_cheddar_sale.add_quantity(10)

        # Check that the amount was calculated correctly and is displayed next to the input field
        actual_cost = self.product_details_page_cheddar_sale.get_cost_of_product()

        # Compare the received value with the expected value
        self.assertEqual(expected_total, actual_cost)

        # Click on the button "Add to Cart"
        self.product_details_page_cheddar_sale.click_add_to_cart_button()

        # Click OK on the modal window about successful adding product
        self.product_details_page_cheddar_sale.modal_window.click_ok_button()

        # Click on the Cart icon on the header
        self.product_details_page_cheddar_sale.header.click_on_the_cart()

        # Wait the Cart with product is loaded
        self.cart_page.wait_full_cart_page_loaded()

        # Choose a delivery method
        self.cart_page.select_pick_up_option()

        # Check the total in the cart and compare with the total on the product page.
        actual_total = self.cart_page.get_cart_total_sum()
        self.assertEqual(expected_total, actual_total)

        # Click on the Pay button
        self.cart_page.click_on_pay_button()

        # Check that the link is correct, and we are on the Payment page
        expected_url_payment: str = self.payment_page.expected_url()
        actual_url_payment: str = self.payment_page.get_current_url()

        self.assertEqual(expected_url_payment, actual_url_payment)

        actual_total = self.payment_page.get_payment_total_sum()
        self.assertEqual(expected_total, actual_total)

        self.payment_page.input_card_number(card_number)

        self.payment_page.input_exp_date(EXP_DATE)

        if card_number == AMERICAN_EXPRESS:
            self.payment_page.input_cvc(CVC_4_DIGITS)
        else:
            self.payment_page.input_cvc(CVC)

        self.payment_page.input_name_on_card(NAME_ON_CARD)

        # Implement assert for validation errors here?

        pay_button_actual: str = self.payment_page.get_pay_button_status()

        pay_button_expected: str = "true"
        # Check that value of disabled is not True
        self.assertNotEqual(pay_button_expected, pay_button_actual)

        # Click on the Pay button
        self.payment_page.click_on_pay_button()

        # Check that modal window is displayed
        self.assertTrue(
            self.cart_page.modal_window.is_present(),
            msg="The Modal window is not displayed",
        )

        # Checking text on Modal window
        expected_header: str = "Success:"
        expected_text: str = (
            "Payment was successful. We just sent your receipt to"
            " your email address, and your items will be on their way shortly."
        )
        actual_header: str = self.payment_page.modal_window.get_modal_title_text()
        actual_text: str = self.payment_page.modal_window.get_modal_message_text()
        self.assertEqual(expected_header, actual_header)
        self.assertEqual(expected_text, actual_text)

        self.payment_page.click_close_button_on_modal()

        # Check that the link is correct, and we are on the Cart Report Page
        expected_url_cart_report: str = self.cart_report_page.expected_url()
        actual_url_cart_report: str = self.cart_report_page.get_current_url()

        self.assertEqual(expected_url_cart_report, actual_url_cart_report)
