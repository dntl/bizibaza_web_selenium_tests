import unittest

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from test_objects.base_test_case import BaseTestCase


class SigninViaApiTests(BaseTestCase):
    def setUp(self) -> None:
        super().setUp()
        assert BaseTestCase.get_driver()
        self.data = self.env.get_sign_in_with_api_test_data()
        self.sign_in_via_api(self.data.account)

    def test(self) -> None:
        # https://dntlab.testrail.io/index.php?/cases/view/232
        assert self.driver
        self.driver.get(f"{self.env.get_frontend_url()}/shopping-list")
        condition = EC.visibility_of_element_located(
            (
                By.XPATH,
                "//h4 [@aria-label='Hi!']",
            )
        )
        element = WebDriverWait(self.driver, 10).until(condition)
        self.assertTrue(element)

    def tearDown(self) -> None:
        super().tearDown()
        self.logout_via_api()
