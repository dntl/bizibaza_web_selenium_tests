from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_objects.base_page import BasePage
from page_objects.page_component.header import Header
from page_objects.page_component.modal_window import ModalWindow
from page_objects.page_component.spinner_preloader import SpinnerPreloader


# Class for all operations with Login


class LoginPage(BasePage):
    page_url = "/login"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.modal_window = ModalWindow(self.driver)
        self.header = Header(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def login_action(self, username: str, password: str) -> None:
        # TODO: Refactor this old method
        wait = WebDriverWait(self.driver, 10)

        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='username']")
            )
        ).send_keys(username)
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='password']")
            )
        ).send_keys(password)
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='loginBtn']")
            )
        ).click()
        # We are waiting for the disappearance of the pre-loader (spinner)
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def clear_username_field(self) -> None:
        self.driver.find_element(By.CSS_SELECTOR, "[data-test-id='username']").clear()

    def wait_page_is_present(self) -> None:
        wait = WebDriverWait(self.driver, 15)
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='username']")
            )
        )
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='password']")
            )
        )
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='loginBtn']")
            )
        )
