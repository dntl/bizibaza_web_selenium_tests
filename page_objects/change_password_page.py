from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

from page_objects.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.page_component.modal_window import ModalWindow

from page_objects.page_component.spinner_preloader import SpinnerPreloader
from page_objects.settings_page import SettingsPage


class ChangePasswordPage(BasePage):
    page_url = "/change-password"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.modal_window = ModalWindow(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def get_change_password_button(self) -> WebElement:
        return self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='changePassword']"
        )

    def get_status_of_change_password_button(self) -> bool:
        # Get status of the button - disabled or enabled
        return self.get_change_password_button().is_enabled()

    def get_old_password_input_field(self) -> WebElement:
        return self.driver.find_element(By.CSS_SELECTOR, "[data-test-id='oldPassword']")

    def get_new_password_input_field(self) -> WebElement:
        return self.driver.find_element(By.CSS_SELECTOR, "[data-test-id='newPassword']")

    def get_repeat_password_input_field(self) -> WebElement:
        return self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='repeatPassword']"
        )

    def get_old_password_icon_eye(self) -> WebElement:
        return self.driver.find_element(
            By.XPATH, "//*[@data-test-id='oldPassword_showPassword']"
        )

    def get_new_password_icon_eye(self) -> WebElement:
        return self.driver.find_element(
            By.XPATH, "//*[@data-test-id='newPassword_showPassword']"
        )

    def get_repeat_password_icon_eye(self) -> WebElement:
        return self.driver.find_element(
            By.XPATH, "//*[@data-test-id='repeatPassword_showPassword']"
        )

    def get_visibility_old_password_field(self) -> str:
        status = self.get_old_password_input_field().get_attribute("type")
        return status

    def get_visibility_new_password_field(self) -> str:
        status = self.get_new_password_input_field().get_attribute("type")
        return status

    def get_visibility_repeat_password_field(self) -> str:
        status = self.get_repeat_password_input_field().get_attribute("type")
        return status

    def get_old_password_eye_button_invisible(self) -> WebElement:
        return self.driver.find_element(
            By.XPATH,
            "//*[@data-test-id='oldPassword_showPassword']//*[@data-test-id='passInvisible']",
        )

    def get_new_password_eye_button_invisible(self) -> WebElement:
        return self.driver.find_element(
            By.XPATH,
            "//*[@data-test-id='newPassword_showPassword']//*[@data-test-id='passInvisible']",
        )

    def get_repeat_password_eye_button_invisible(self) -> WebElement:
        return self.driver.find_element(
            By.XPATH,
            "//*[@data-test-id='repeatPassword_showPassword']//*[@data-test-id='passInvisible']",
        )

    def click_to_open_old_password_eye(self) -> None:
        self.get_old_password_eye_button_invisible().click()

    def click_to_open_new_password_eye(self) -> None:
        self.get_new_password_eye_button_invisible().click()

    def click_to_open_repeat_password_eye(self) -> None:
        self.get_repeat_password_eye_button_invisible().click()

    def click_on_change_password_button(self) -> None:
        self.get_change_password_button().click()
        WebDriverWait(self.driver, 10).until(self.modal_window.is_present_condition())

    def click_ok_button_in_change_pass_successful(self) -> None:
        self.modal_window.click_ok_button()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        SettingsPage(self.driver).wait_page_is_present()

    def set_old_password(self, password: str) -> None:
        self.get_old_password_input_field().send_keys(password)

    def set_new_password(self, password: str) -> None:
        self.get_new_password_input_field().send_keys(password)

    def set_repeat_password(self, password: str) -> None:
        self.get_repeat_password_input_field().send_keys(password)

    def wait_page_is_present(self) -> None:
        WebDriverWait(self.driver, 10).until(
            EC.text_to_be_present_in_element(
                (By.CLASS_NAME, "input__info"),
                "Password must be 8 to 100 characters, including digits, upper- and lower-case letters; no spaces",
            )
        )
