from dataclasses import dataclass
from datetime import datetime
from typing import Optional

from price_parser import Price
from decimal import Decimal

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

from page_objects.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.chat_page import ChatPage
from page_objects.page_component.header import Header
from page_objects.page_component.spinner_preloader import SpinnerPreloader


class HistoryPageLocators:
    SHOW_RECORD_DETAILS = By.CSS_SELECTOR, '[data-test-id*="showProductButton"]'


@dataclass
class HistoryElement:
    sale_datetime: datetime
    product_name: str
    company_name: str
    total: Decimal


@dataclass
class HistoryElementDetails:
    product_name_details: str
    cost_product_details: str
    company_name_details: str
    order_code_details: str
    seller_address_details: str
    total_quantity_details: str
    total_cost_details: Decimal
    pick_up_details: str | None
    seller_phone_number_details: str


@dataclass
class HistoryList:
    products_list: list[HistoryElement]
    total_qty_on_page: int
    total_cost_on_page: Decimal


class HistoryPage(BasePage):
    page_url = "/history"

    def __init__(self, driver: WebDriver) -> None:
        super().__init__(driver)
        self.spinner_preloader: SpinnerPreloader = SpinnerPreloader(self.driver)
        self.header: Header = Header(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def get_history_model(self) -> HistoryList:
        # Retrieve data from the page
        products_list = self.get_history_elements()
        total_qty_on_page = self.get_products_of_the_page_number()
        total_cost_on_page = self.get_parsed_total_of_the_page()

        # Create a HistoryList instance with the retrieved data
        model = HistoryList(
            products_list=products_list,
            total_qty_on_page=total_qty_on_page,
            total_cost_on_page=total_cost_on_page,
        )

        return model

    def get_history_elements(self) -> list[HistoryElement]:
        history_elements: list[HistoryElement] = []

        history_product_elements: list[WebElement] = self.driver.find_elements(
            By.CSS_SELECTOR, '[data-test-id*="historyItem"]'
        )

        for element in history_product_elements:
            sale_date_time_element: WebElement = element.find_element(
                By.CSS_SELECTOR, '[data-test-id*="date"]'
            )
            sale_date_text: str = sale_date_time_element.text.strip("Date:")
            sale_date: datetime = datetime.strptime(
                sale_date_text.strip(), "%B %d, %Y %I:%M %p"
            )

            product_name_element: WebElement = element.find_element(
                By.CSS_SELECTOR, '[data-test-id*="idOrder_nameItem"]'
            )
            product_name: str = product_name_element.text.split("Order:")[-1].strip()

            company_name_element: WebElement = element.find_element(
                By.CSS_SELECTOR, '[data-test-id*="idOrder_companyNameItem"]'
            )
            company_name: str = company_name_element.text.strip(" Company Name:")

            total_element: WebElement = element.find_element(
                By.CSS_SELECTOR, '[data-test-id*="TotalItem"]'
            )
            total_text: str = total_element.text
            price = Price.fromstring(total_text)

            if not isinstance(price.amount, Decimal):
                raise TypeError("Invalid amount type")

            # Create a HistoryElement with the extracted data
            history_element: HistoryElement = HistoryElement(
                sale_datetime=sale_date,
                product_name=product_name,
                company_name=company_name,
                total=Decimal(price.amount),
            )
            history_elements.append(history_element)

        return history_elements

    def extract_history_element_details(self) -> HistoryElementDetails:
        product_name_element: WebElement = self.driver.find_element(
            By.CSS_SELECTOR, '[data-test-id="nameItem_0"]'
        )
        product_name_details: str = product_name_element.text.strip()

        cost_product_element: WebElement = self.driver.find_element(
            By.CSS_SELECTOR, '[data-test-id="costItem_0_0"]'
        )
        cost_product_details: str = cost_product_element.text

        company_name_element: WebElement = self.driver.find_element(
            By.CSS_SELECTOR, '[data-test-id="sellerNameItem_0_0"]'
        )
        company_name_details: str = company_name_element.text.strip(" Company Name:")

        order_code_element: WebElement = self.driver.find_element(
            By.CSS_SELECTOR, '[data-test-id="orderCodeItem_0_0"]'
        )
        order_code_details: str = order_code_element.text.strip(" Order code:")

        seller_address_element: WebElement = self.driver.find_element(
            By.CSS_SELECTOR, '[data-test-id="itemAddressFrom_0_0"]'
        )
        seller_address_details: str = seller_address_element.text.strip()

        total_quantity_element: WebElement = self.driver.find_element(
            By.XPATH, '//*[@class="qnt-item"]/*[@class="math-sign"]'
        )
        total_quantity_details: str = total_quantity_element.text.strip("× ")

        total_cost_element: WebElement = self.driver.find_element(
            By.CSS_SELECTOR, '[data-test-id="history-subtotal-cost_0_0"]'
        )
        total_cost_text: str = total_cost_element.text.strip()
        price = Price.fromstring(total_cost_text)

        if not isinstance(price.amount, Decimal):
            raise TypeError("Invalid amount type")

        total_cost_details: Decimal = Decimal(price.amount)

        seller_phone_number_element: WebElement = self.driver.find_element(
            By.CSS_SELECTOR, '[data-test-id="seller_primary_phone_0_0"]'
        )
        seller_phone_number_details: str = seller_phone_number_element.text

        pick_up_element: list[WebElement] = self.driver.find_elements(
            By.CSS_SELECTOR, '[data-test-id="buyer_address_0_0"]'
        )
        pick_up_details: Optional[str] = (
            pick_up_element[0].text.strip("Pick up:") if pick_up_element else None
        )

        history_element_details: HistoryElementDetails = HistoryElementDetails(
            product_name_details=product_name_details,
            cost_product_details=cost_product_details,
            company_name_details=company_name_details,
            order_code_details=order_code_details,
            seller_address_details=seller_address_details,
            total_quantity_details=total_quantity_details,
            total_cost_details=total_cost_details,
            pick_up_details=pick_up_details,
            seller_phone_number_details=seller_phone_number_details,
        )

        return history_element_details

    def get_qty_products_element_of_the_page(self) -> WebElement:
        qty_element = self.driver.find_element(
            By.XPATH, '//*[@data-test-id="qntItems"]'
        )
        return qty_element

    def get_products_of_the_page_number(self) -> int:
        products_text = self.get_qty_products_element_of_the_page().text
        products_text = products_text.replace(" items", "")
        return int(products_text)

    def get_total_products_of_the_page(self) -> str:
        total_element = self.driver.find_element(By.XPATH, '//*[@data-test-id="total"]')
        total_text = total_element.text.strip()
        return total_text

    def get_parsed_total_of_the_page(self) -> Decimal:
        total_text = self.get_total_products_of_the_page()
        price = Price.fromstring(total_text)
        if not isinstance(price.amount, Decimal):
            raise TypeError("Invalid amount type")
        return Decimal(price.amount)

    def get_expand_history_element(self, index: int) -> WebElement:
        expand = self.driver.find_element(
            By.CSS_SELECTOR, f"[data-test-id='rightDirItem_{index}']"
        )
        return expand

    def get_contact_button_element(self, index: int) -> WebElement:
        contact_element = self.driver.find_element(
            By.CSS_SELECTOR, f"[data-test-id='contactButton_{index}']"
        )
        return contact_element

    def get_back_button_element(self) -> WebElement:
        back_arrow_element = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='analyticLink']"
        )
        return back_arrow_element

    def click_back_button(self) -> None:
        self.get_back_button_element().click()
        wait = WebDriverWait(self.driver, 10)
        orders: WebElement = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='orders-menu']")
            )
        )
        history: WebElement = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='history']")
            )
        )

    def click_on_contact_button_by_index(self, index: int) -> None:
        self.get_contact_button_element(index).click()
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        ChatPage(self.driver).wait_page_is_present()

    def click_on_arrow_to_expand_history_element_by_index(self, index: int) -> None:
        self.get_expand_history_element(index).click()
        wait = WebDriverWait(self.driver, 2)
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id*='contactButton']")
            )
        )

    def click_on_arrow_to_collapse_history_element_by_index(self, index: int) -> None:
        self.get_expand_history_element(index).click()
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.invisibility_of_element_located(
                (By.CSS_SELECTOR, "HistoryPageLocators.SHOW_RECORD_DETAILS")
            )
        )

    def wait_page_is_present(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        wait.until(
            EC.visibility_of_all_elements_located(
                (By.XPATH, "//*[contains(@data-test-id, 'historyItem')]")
            )
        )
