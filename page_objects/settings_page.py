from typing import Final

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement

from page_objects.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.page_component.modal_window import ModalWindow
from page_objects.page_component.pop_up import PopUp
from page_objects.page_component.spinner_preloader import SpinnerPreloader
from page_objects.shopping_list_page import ShoppingListPage

LOGOUT_BUTTON: Final = By.CSS_SELECTOR, "[data-test-id=logout]"


class SettingsPage(BasePage):
    page_url = "/settings"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.modal_window = ModalWindow(self.driver)
        self.pop_up = PopUp(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def get_logout_button(self) -> list[WebElement]:
        # We are looking for elementS, not an element to avoid an error NoSuchElementException
        logout_button = self.driver.find_elements(LOGOUT_BUTTON)
        return logout_button

    def visible_logout_button(self) -> bool:
        return self.is_visible(LOGOUT_BUTTON)

    def click_log_out_button(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(EC.element_to_be_clickable(LOGOUT_BUTTON))
        element.click()
        WebDriverWait(self.driver, 10).until(self.modal_window.is_present_condition())

    def click_home_icon(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-test-id='backLink']"))
        )
        element.click()

    def click_change_password(self) -> None:
        change_pass = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id=goToChangePassword]"
        )
        change_pass.click()
        # We expect the Change Password page to load.
        # DO NOT refer to the page object of the Change Password page to avoid Circular dependency import errors.
        WebDriverWait(self.driver, 2).until(
            EC.text_to_be_present_in_element(
                (By.CLASS_NAME, "input__info"),
                "Password must be 8 to 100 characters, including digits, upper- and lower-case letters; no spaces",
            )
        )

    def click_update_profile(self) -> None:
        update_profile = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id=goToProfile]"
        )
        update_profile.click()
        # We expect the Profile page to load.
        # DO NOT refer to the page object of the Profile page to avoid Circular dependency import errors.
        WebDriverWait(self.driver, 10).until(
            EC.text_to_be_present_in_element(
                (By.CSS_SELECTOR, "[data-test-id=newAddress]"), "+ Add new address"
            )
        )

    def click_delete_account(self) -> None:
        delete = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id=deleteAccount]"
        )
        delete.click()
        WebDriverWait(self.driver, 10).until(self.modal_window.is_present_condition())

    def click_yes_button_on_logout_confirmation(self) -> None:
        self.modal_window.click_ok_button()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        ShoppingListPage(self.driver).wait_page_is_present()

    def wait_page_is_present(self) -> None:
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='settingsTitle']")
            )
        )
