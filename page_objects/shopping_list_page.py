from typing import List

from selenium.common.exceptions import TimeoutException
from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.expected_conditions import all_of
from selenium.webdriver.support.wait import WebDriverWait

from page_objects.base_page import BasePage
from selenium.webdriver.support import expected_conditions as EC

from page_objects.login_page import LoginPage
from page_objects.page_component.categories_menu import CategoriesMenu
from page_objects.page_component.footer import Footer
from page_objects.page_component.header import Header
from page_objects.page_component.input_field import InputField
from page_objects.page_component.modal_window import ModalWindow
from page_objects.page_component.navigation_menu import NavigationMenu
from page_objects.page_component.spinner_preloader import SpinnerPreloader


class ShoppingListPage(BasePage):
    page_url = "/shopping-list"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.categories_menu = CategoriesMenu(self.driver)
        self.navigation_menu = NavigationMenu(self.driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.modal_window = ModalWindow(self.driver)
        self.header = Header(self.driver)
        self.footer = Footer(self.driver)
        self.input_field = InputField(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def get_shopping_list_item_0(self) -> WebElement:
        return WebDriverWait(self.driver, 3).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='shoppingListProduct_0_0']")
            )
        )

    def get_product_list(self) -> list[WebElement]:
        products_list = self.driver.find_elements(
            By.XPATH, "//*[contains(@data-test-id, 'shoppingListProduct_0')]"
        )
        return products_list

    def get_product_list_marked(self) -> list[WebElement]:
        products_list_marked = self.driver.find_elements(
            By.XPATH, "//*[contains(@data-test-id, 'shoppingListProduct_1')]"
        )
        return products_list_marked

    def get_products_list_names(self) -> list[str]:
        """Get names of the all items on the Shopping list"""
        products_list = self.get_product_list()

        product_names: List[str] = []

        for product_name in products_list:
            product_names.append(product_name.text)

        return product_names

    def get_products_list_marked_names(self) -> list[str]:
        """Get names of the all items on the Shopping list"""
        products_list = self.get_product_list_marked()

        product_names_marked: List[str] = []

        for product_name in products_list:
            product_names_marked.append(product_name.text)

        return product_names_marked

    def is_item_displayed_on_the_list(self) -> str:
        text = self.get_shopping_list_item_0().text
        return text

    def wait_element_on_the_list(self, text: str):
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(
            EC.visibility_of_element_located(
                (By.XPATH, f"//*[contains(text(), '{text}')]")
            )
        )
        return element

    def back_to_shopping_list(self) -> None:
        back_button = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='shoppingLink']"
        )
        back_button.click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def click_on_the_item_on_shopping_list(self, text: str) -> None:
        item_on_shopping_list = EC.element_to_be_clickable(
            (By.XPATH, f"//*[contains(text(), '{text}')]")
        )
        WebDriverWait(self.driver, 5).until(item_on_shopping_list).click()
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def click_on_new_item_title_field(self) -> None:
        WebDriverWait(self.driver, 5).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='customItemInput']")
            )
        ).send_keys(Keys.ENTER)

    def count_all_trash_icons_on_the_list(self) -> int:
        try:
            visible = WebDriverWait(self.driver, 5).until(
                EC.visibility_of_all_elements_located(
                    (By.CLASS_NAME, "icon-trash-empty")
                )
            )
            remove_buttons = self.driver.find_elements(
                By.CLASS_NAME, "icon-trash-empty"
            )
            return len(remove_buttons)
        except TimeoutException:
            invisible = WebDriverWait(self.driver, 1).until(
                EC.invisibility_of_element(
                    (By.CSS_SELECTOR, "[data-test-id='removeBtn']")
                )
            )
            return 0

    def click_on_trash_icon_on_list(self) -> None:
        WebDriverWait(self.driver, 5).until(
            EC.visibility_of_element_located((By.CLASS_NAME, "icon-trash-empty"))
        ).click()

    def delete_all_items_from_list(self) -> None:
        count: int = self.count_all_trash_icons_on_the_list()
        while count > 0:
            self.click_on_trash_icon_on_list()
            count -= 1

    def is_visible_login_link(self) -> bool:
        try:
            login_link = WebDriverWait(self.driver, 2).until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, "[data-test-id='login-link']")
                )
            )
            return True
        except TimeoutException:
            return False

    def is_visible_get_sign_up_link(self) -> bool:
        try:
            signup_link = WebDriverWait(self.driver, 2).until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, "[data-test-id='signup-link']")
                )
            )
            return True
        except TimeoutException:
            return False

    def is_shopping_list_empty(self) -> bool:
        try:
            WebDriverWait(self.driver, 2).until(
                EC.invisibility_of_element(
                    (By.CSS_SELECTOR, "[data-test-id='removeBtn_0_0']")
                )
            )
            return True
        except TimeoutException:
            return False

    def mark_item_on_shopping_list(self, index) -> None:
        mark = self.driver.find_element(
            By.CSS_SELECTOR, f"[data-test-id='statusBtn_0_{index}']"
        )
        mark.click()

    def select_first_suggestion_by_click(self, item: str) -> None:
        # The method clicks on the zero value of suggestions from the search results,
        # then it waits until this item appears in the shopping list.
        suggestion = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='suggestion_0']"
        )
        suggestion.click()
        self.wait_element_on_the_list(item)

    def sort_shopping_list(self) -> None:
        sort = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='sortShoppingList']"
        )
        sort.click()

    def get_item_coming_soon(self) -> str:
        item_text = (
            WebDriverWait(self.driver, 10)
            .until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, "[data-test-id='item_comming_soon']")
                )
            )
            .text
        )
        return item_text

    def text_to_the_new_item(self, text: str) -> None:
        self.input_field.type_text("[data-test-id='customItemInput']", text)

    def text_to_the_new_item_enter(self, item: str) -> None:
        self.input_field.set_text_enter("[data-test-id='customItemInput']", item)
        self.wait_element_on_the_list(item)

    def wait_page_is_present(self) -> None:
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        data_test_ids: list[str] = [
            "customItemInput",
            "goToRootPage",
            "settingsLink",
        ]
        conditions: list = list(
            map(
                lambda data_test_id: EC.element_to_be_clickable(
                    (By.CSS_SELECTOR, f"[data-test-id='{data_test_id}'")
                ),
                data_test_ids,
            )
        )
        WebDriverWait(self.driver, 10).until(all_of(*conditions))

    def click_settings_button(self) -> None:
        settings_button = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id=settingsLink]"
        )
        settings_button.click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        # Wait for the settings page to load
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='settingsTitle']")
            )
        )

    def click_log_in_link_button(self) -> None:
        self.driver.find_element(By.CSS_SELECTOR, "[data-test-id=login-link]").click()
        LoginPage(self.driver).wait_page_is_present()
