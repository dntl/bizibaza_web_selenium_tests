from selenium.common import NoSuchElementException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait

from page_objects.base_page import BasePage
from selenium.webdriver.support import expected_conditions as EC
from page_objects.page_component.header import Header
from page_objects.page_component.modal_window import ModalWindow
from page_objects.page_component.spinner_preloader import SpinnerPreloader
from price_parser import Price
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.by import By
from typing import List
from decimal import Decimal
from datetime import datetime
from dataclasses import dataclass


@dataclass
class CartReportElement:
    product_name: str
    company_name: str
    order_code: str
    total_quantity: str
    total_amount: Decimal

    # Must be added later.
    # delivery_method: str | None

    pick_up: str | None
    seller_phone_number: str


@dataclass
class CartReport:
    sale_date: datetime
    buyer_name: str
    products: list[CartReportElement]
    purchase_total: Decimal


class CartReportPage(BasePage):
    page_url: str = "/cart-report"

    CART_REPORT_CLASS_NAMES: List[str] = [
        "cart-report__header",
        "cart-report__text",
        "cart-report__sale-info",
        "cart-report__items",
        "cart-report__total",
    ]

    def __init__(self, driver: WebDriver) -> None:
        super().__init__(driver)
        self.spinner_preloader: SpinnerPreloader = SpinnerPreloader(self.driver)
        self.modal_window: ModalWindow = ModalWindow(self.driver)
        self.header: Header = Header(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def get_cart_report_model(self) -> CartReport:
        # Retrieve data from the page
        sale_date = self.get_sale_date()
        buyer_name = self.get_buyer_name()
        purchase_total = self.parse_purchase_total()

        # Create a CartReport instance with the retrieved data
        model = CartReport(
            sale_date=sale_date,
            buyer_name=buyer_name,
            products=self.products(),
            purchase_total=purchase_total,
        )

        return model

    def products(self) -> list[CartReportElement]:
        """Get the models of products inside the cart."""
        products: list[CartReportElement] = []
        for el in self.driver.find_elements(
            By.CSS_SELECTOR, "div.cart-report__item__table__elements"
        ):
            # Total amount must be always a valid number.
            # If the Price library can't parse it — obviously it as error.
            amount: Decimal | None = Price.fromstring(
                el.find_element(
                    By.XPATH,
                    './/h4[contains(text(), "Total Amount:")]/following-sibling::h5',
                ).text
            ).amount
            assert amount is not None

            # Special handling of pickup info — it may be missing (if delivery is selected).
            pick_up: str | None = None
            try:
                pick_up = el.find_element(
                    By.XPATH,
                    "//*[contains(@data-test-id, 'pick_up_address')]",
                ).text
            except NoSuchElementException as e:
                # Sometimes it is missing.
                pass

            products.append(
                CartReportElement(
                    product_name=el.find_element(
                        By.XPATH,
                        ".//h4[contains(text(), 'Item Name:')]/following-sibling::h5",
                    ).text,
                    company_name=el.find_element(
                        By.XPATH,
                        ".//h4[contains(text(), 'Company Name:')]/following-sibling::h5",
                    ).text,
                    order_code=el.find_element(
                        By.XPATH,
                        ".//h4[contains(text(), 'Order code:')]/following-sibling::h5",
                    ).text,
                    total_quantity=el.find_element(
                        By.XPATH,
                        ".//h4[contains(text(), 'Total Quantity:')]/following-sibling::h5",
                    ).text,
                    total_amount=amount,
                    pick_up=pick_up,
                    seller_phone_number=el.find_element(
                        By.XPATH,
                        ".//h4[contains(text(), 'Seller phone number:')]/following-sibling::h5",
                    ).text,
                )
            )
        return products

    def confirmation_element(self) -> WebElement:
        # Find the element with class cart-report__text using its XPATH
        confirmation_element = self.driver.find_element(
            By.XPATH, '//*[@class="cart-report__text"]'
        )
        return confirmation_element

    def get_confirmation_text(self) -> str:
        """Confirmation text on the top."""
        return self.confirmation_element().text

    def find_sale_date_element(self) -> WebElement:
        sale_date_element = self.driver.find_element(
            By.XPATH, '//h5[@data-test-id="report_order_date"]'
        )
        return sale_date_element

    def get_sale_date(self) -> datetime:
        # Get the sale date element
        sale_date_element = self.find_sale_date_element()

        # Extract the text from the element and remove any leading or trailing spaces
        sale_date_str = sale_date_element.text

        # Parse the sale date string using the specified format and convert it to a datetime object
        sale_date = datetime.strptime(sale_date_str, "%B %d, %Y %I:%M %p")
        return sale_date

    def find_buyer_name_element(self) -> WebElement:
        # Find the element that contains the buyer name using its XPATH
        buyer_name_element = self.driver.find_element(
            By.XPATH, '//h4[text()="Buyer name:"]/following-sibling::h5'
        )
        return buyer_name_element

    def get_buyer_name(self) -> str:
        # Get the buyer name element
        return self.find_buyer_name_element().text

    def find_purchase_total_element(self) -> WebElement:
        # Find the element that contains the purchase total
        purchase_total_elem = self.driver.find_element(
            By.CLASS_NAME, "cart-report__total"
        )
        # Find the element that contains the purchase total amount
        return purchase_total_elem.find_element(By.CSS_SELECTOR, "h3.text-right")

    def parse_purchase_total(self) -> Decimal:
        purchase_total_element = self.find_purchase_total_element()
        purchase_total_amount_str = purchase_total_element.text

        purchase_total_price = Price.fromstring(purchase_total_amount_str)
        if not isinstance(purchase_total_price.amount, Decimal):
            raise TypeError("Invalid amount type")

        return purchase_total_price.amount

    def wait_page_is_present(self) -> None:
        # Check that the Cart Report page is loaded
        wait = WebDriverWait(self.driver, 10)
        for class_name in self.CART_REPORT_CLASS_NAMES:
            wait.until(
                EC.visibility_of_all_elements_located((By.CLASS_NAME, class_name))
            )
