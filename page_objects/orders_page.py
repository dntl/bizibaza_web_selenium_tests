from selenium.webdriver.remote.webelement import WebElement

from page_objects.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.history_page import HistoryPage


class OrdersPage(BasePage):
    page_url = "/orders"

    def get_url(self) -> str:
        return self.page_url

    def get_history_element(self) -> WebElement:
        history = self.driver.find_element(By.CSS_SELECTOR, "[data-test-id=history]")
        return history

    def click_history_button(self) -> None:
        self.get_history_element().click()
        HistoryPage(self.driver).wait_page_is_present()

    def wait_page_is_present(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        orders: WebElement = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='orders-menu']")
            )
        )
        history: WebElement = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='history']")
            )
        )
