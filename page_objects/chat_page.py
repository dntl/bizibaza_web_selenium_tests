from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_objects.base_page import BasePage
from page_objects.page_component.spinner_preloader import SpinnerPreloader


class ChatPage(BasePage):
    page_url = "/chat-room"

    def __init__(self, driver: WebDriver) -> None:
        super().__init__(driver)
        self.spinner_preloader: SpinnerPreloader = SpinnerPreloader(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def wait_page_is_present(self) -> None:
        field: WebElement = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='messageInput']")
            )
        )

    def get_the_send_button_element(self) -> WebElement:
        return self.driver.find_element(By.CSS_SELECTOR, "[data-test-id='sendMessage']")

    def get_the_back_button_element(self) -> WebElement:
        return self.driver.find_element(By.CSS_SELECTOR, "[data-test-id='dialogsLink']")

    def click_the_back_button_from_chat(self) -> None:
        self.get_the_back_button_element().click()
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def click_send_button(self) -> None:
        self.get_the_send_button_element().click()
