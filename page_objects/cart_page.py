from typing import Final

from price_parser import Price
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from page_objects.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.chat_page import ChatPage
from page_objects.page_component.header import Header
from page_objects.page_component.modal_window import ModalWindow
from page_objects.page_component.spinner_preloader import SpinnerPreloader
from decimal import Decimal

REFRESH_BUTTON: Final = (By.CSS_SELECTOR, "[data-test-id=cartRefreshItem_0]")
EMPTY_CART: Final = EC.visibility_of_element_located(
    (By.XPATH, "//*[@class = 'empty-list'] [text() = 'Your cart is currently empty']")
)
FULL_CART: Final = EC.visibility_of_all_elements_located(
    (By.XPATH, "//*[contains(@data-test-id, 'cartItemImage_')]")
)

CART_COUNTER = (
    By.XPATH,
    "//*[contains(@data-test-id, 'cartItemCounterHeader')][contains(text(), '1')]",
)


class CartPage(BasePage):
    page_url = "/cart"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.modal_window = ModalWindow(self.driver)
        self.header = Header(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def click_on_pay_button(self) -> None:
        pay_button = self.driver.find_element(By.CSS_SELECTOR, "[data-test-id=payBtn]")
        pay_button.click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        # Wait for either the PaymentPage to load or the modal window to appear
        WebDriverWait(self.driver, 10).until(
            EC.any_of(
                EC.visibility_of_element_located((By.CSS_SELECTOR, "[data-test-id='PayByCartNumberButton']")),
                self.modal_window.is_present_condition()
            )
        )


    def click_ok_button_to_contact_seller(self) -> None:
        self.modal_window.click_ok_button()
        ChatPage(self.driver).wait_page_is_present()

    def click_on_refresh_button(self) -> None:
        self.get_refresh_button().click()
        WebDriverWait(self.driver, 1).until(
            EC.invisibility_of_element(
                (By.CSS_SELECTOR, "[data-test-id=cartRefreshItem_0]")
            )
        )

    def count_all_remove_buttons_on_the_cart(self) -> int:
        try:
            visible = WebDriverWait(self.driver, 5).until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, "[data-test-id='cartRemoveItem_0']")
                )
            )
            remove_buttons = self.driver.find_elements(
                By.XPATH, "//*[contains(@data-test-id, 'cartRemoveItem_')]"
            )
            return len(remove_buttons)
        except TimeoutException:
            invisible = WebDriverWait(self.driver, 1).until(
                EC.invisibility_of_element(
                    (By.CSS_SELECTOR, "[data-test-id='removeBtn_0']")
                )
            )
            return 0

    def get_badge_counter_on_the_cart(self) -> str:
        wait = WebDriverWait(self.driver, 10)
        counter = wait.until(
            EC.visibility_of_element_located((By.CLASS_NAME, "badge"))
        ).text
        return counter

    def get_cart_empty_message(self) -> WebElement:
        return WebDriverWait(self.driver, 5).until(
            EC.visibility_of_element_located((By.CLASS_NAME, "empty-list"))
        )

    def get_cart_product_name(self) -> str:
        name = self.driver.find_element(
            By.XPATH, "//*[contains(@data-test-id, 'cartItemTitle')]"
        )
        return name.text

    def get_cart_total_sum(self) -> Decimal:
        cart_total_text = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='cartTotal']"
        ).text

        price = Price.fromstring(cart_total_text)
        if not isinstance(price.amount, Decimal):
            raise TypeError("Invalid amount type")

        return Decimal(price.amount)

    def get_delivery_option(self) -> WebElement:
        return self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='cartItemShipping_0']"
        )

    def get_expire_time_text(self) -> str:
        expire_time = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id=cartItemExpiresIn_0"
        ).text
        return expire_time

    def get_pick_up_option(self) -> WebElement:
        return self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='cartItemPickUp_0']"
        )

    def get_refresh_button(self) -> WebElement:
        return self.driver.find_element(*REFRESH_BUTTON)

    def get_total_of_items(self) -> str:
        wait = WebDriverWait(self.driver, 10)
        total = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='numberOfItems']")
            )
        ).text
        return total

    def is_cart_page_empty(self) -> bool:
        return self.get_cart_empty_message().text == "Your cart is currently empty"

    def remove_first_item_from_cart(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "[data-test-id='cartRemoveItem_0']")
            )
        )
        element.click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def remove_all_products_from_cart(self) -> None:
        count: int = self.count_all_remove_buttons_on_the_cart()
        while count > 0:
            self.remove_first_item_from_cart()
            count -= 1

    def select_delivery_option(self) -> None:
        self.get_delivery_option().click()
        WebDriverWait(self.driver, 10).until(self.modal_window.is_present_condition())

    def select_pick_up_option(self) -> None:
        self.get_pick_up_option().click()

    def visible_refresh_button(self) -> bool:
        return self.is_visible(REFRESH_BUTTON)

    def visible_cart_counter_badge(self) -> bool:
        return self.is_visible(CART_COUNTER)

    def wait_badge_counter_text(self) -> None:
        WebDriverWait(self.driver, 10).until(
            EC.text_to_be_present_in_element_attribute(
                (By.XPATH, "//*[@id='cartItemCounterHeader']/div[2]"),
                "aria-labelledby",
                "There are 1 products in your shopping cart",
            )
        )

    def wait_cart_counter(self) -> None:
        # Check that the cart counter in the header displays the number "1"
        WebDriverWait(self.driver, 10).until(
            EC.text_to_be_present_in_element(
                (By.CSS_SELECTOR, "[data-test-id='cartItemCounterHeader']"), "1"
            )
        )

    # Checking that the cart page has loaded by checking for the main elements
    def wait_full_cart_page_loaded(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        title: WebElement = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='cartItemTitle_0']")
            )
        )
        product: WebElement = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='cartItemProductNo_0']")
            )
        )
        seller: WebElement = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='cartItemSellerName_0']")
            )
        )

    # Check that the Cart page is loaded
    def wait_page_is_present(self) -> None:
        WebDriverWait(self.driver, 10).until(EC.any_of(FULL_CART, EMPTY_CART))

    def wait_empty_page_is_present(self) -> None:
        WebDriverWait(self.driver, 20).until(EMPTY_CART)
