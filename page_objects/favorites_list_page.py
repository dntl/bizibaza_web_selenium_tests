from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_objects.base_page import BasePage
from page_objects.page_component.spinner_preloader import SpinnerPreloader
from page_objects.product_details_page import ProductDetailsPage


class FavoritesListPage(BasePage):
    page_url = "/favorites"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def wait_item_is_present_on_favorites_list(self, text: str) -> None:
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    "//*[contains(@data-test-id, 'favListPTitle')]"
                    f"[contains(text(), '{text}')]",
                )
            )
        )

    def click_on_product_on_the_favorites_list(
        self, text: str, product_id: str
    ) -> None:
        wait = WebDriverWait(self.driver, timeout=3)
        product = wait.until(
            EC.element_to_be_clickable((By.XPATH, f"//*[contains(text(), '{text}')]"))
        )
        product.click()
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        ProductDetailsPage(self.driver, product_id).wait_page_is_present()

    def wait_that_favorites_list_is_empty(self, text: str) -> None:
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.invisibility_of_element_located(
                (
                    By.XPATH,
                    "//*[contains(@data-test-id, 'favListPTitle')]"
                    f"[contains(text(), '{text}')]",
                )
            )
        )

    def wait_page_is_present(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.visibility_of_all_elements_located(
                (By.XPATH, "//*[text() = 'Favorites']")
            )
        )
