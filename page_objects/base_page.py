from abc import ABC, abstractmethod
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from factory import get_environment


class BasePage(ABC):
    """Base class to initialize the base page that will be called from all
    pages"""

    def __init__(self, driver):
        self.driver = driver

    @staticmethod
    def get_base_url() -> str:
        return get_environment().get_frontend_url()

    def get_url(self) -> str:
        # Must be implemented in child classes.
        raise NotImplementedError

    def get_current_url(self) -> str:
        return self.driver.current_url

    def expected_url(self) -> str:
        return get_environment().get_frontend_url() + self.get_url()

    def open(self) -> None:
        """Open page"""
        self.driver.get(self.get_base_url() + self.get_url())
        self.wait_page_is_present()

    @abstractmethod
    def wait_page_is_present(self) -> None:
        """Implement in all children classes with an algorithm how to wait for complete page is loaded."""
        ...

    def java_script_click(self, locator: tuple[str, str]) -> None:
        # Reliable button click using JavaScript
        wait = WebDriverWait(self.driver, 15)
        element = wait.until(EC.visibility_of_element_located(locator))
        self.driver.execute_script("arguments[0].click()", element)

    def is_current_url(self) -> bool:
        return self.get_url() in self.driver.current_url

    def is_visible(self, locator: tuple[str, str]) -> bool:
        elements = self.driver.find_elements(*locator)
        return len(elements) > 0 and elements[0].is_displayed()
