from typing import Tuple, Callable

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.expected_conditions import all_of

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_objects.page_component.page_component import PageComponent
from page_objects.sub_categories_page import SubCategoriesPage


class CategoriesMenu(PageComponent):
    def __init__(self, driver: WebDriver):
        super().__init__(driver, (By.CLASS_NAME, "footer__popup__category"))
        self.sub_categories_page = SubCategoriesPage(self.driver)
        self.driver = driver

    def is_present_condition(self) -> Callable:
        return EC.visibility_of_element_located(self._locator)

    def is_present(self) -> bool:
        return self.get_component().is_displayed()

    def select_category(self, category: str) -> None:
        select = self.get_component().find_element(
            By.CSS_SELECTOR, f"[data-test-id='{category}']"
        )
        select.click()
        self.sub_categories_page.wait_page_is_present()

    def wait_till_all_categories_is_displayed(self) -> None:
        wait = WebDriverWait(self.driver, 10)

        data_test_ids: list[str] = [
            "Fruits",
            "Vegetables",
            "Poultry",
            "Meat",
            "Dairy",
            "Seafood",
            "Bakery",
            "Other",
        ]
        conditions: list = list(
            map(
                lambda data_test_id: EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, f"[data-test-id='{data_test_id}'")
                ),
                data_test_ids,
            )
        )
        wait.until(all_of(*conditions))
