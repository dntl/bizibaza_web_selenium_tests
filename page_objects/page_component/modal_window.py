from typing import Callable

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.page_component.page_component import PageComponent
from page_objects.page_component.spinner_preloader import SpinnerPreloader


class ModalWindow(PageComponent):
    def __init__(self, driver: WebDriver):
        super().__init__(driver, (By.CLASS_NAME, "modal__dialog"))
        self.spinner_preloader = SpinnerPreloader(self.driver)

    def get_title_element(self) -> WebElement:
        return self.get_component().find_element(
            By.CSS_SELECTOR, "[data-test-id=modalHeader]"
        )

    def get_body_element(self) -> WebElement:
        return self.get_component().find_element(
            By.CSS_SELECTOR, "[data-test-id=modalMessage]"
        )

    def get_modal_title_text(self) -> str:
        return self.get_title_element().text

    def get_modal_message_text(self) -> str:
        return self.get_body_element().text

    def text_strip(self) -> str:
        return self.get_body_element().text.strip()

    def is_present_condition(self) -> Callable:
        return EC.visibility_of_element_located(
            (By.CSS_SELECTOR, "[data-test-id='modalHeader']")
        )

    def is_present(self) -> bool:
        return self.get_component().is_displayed()

    def is_not_present(self) -> bool:
        return len(self.driver.find_elements(*self._locator)) == 0

    def click_ok_button(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-test-id='yesButton']"))
        )
        element.click()
        # We are waiting for the disappearance of the pre-loader (spinner) again after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def click_no_button(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-test-id='noButton']"))
        )
        element.click()
        # We are waiting for the disappearance of the pre-loader (spinner) again after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
