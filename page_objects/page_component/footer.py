from typing import Callable

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.page_component.categories_menu import CategoriesMenu
from page_objects.page_component.navigation_menu import (
    NavigationMenu,
    NavigationMenuLocators,
)
from page_objects.page_component.page_component import PageComponent
from page_objects.page_component.spinner_preloader import SpinnerPreloader


class FooterLocators:
    BUYER_BUTTON: tuple[str, str] = (By.CSS_SELECTOR, "[data-test-id='buyerBtn']")
    SELLER_BUTTON: tuple[str, str] = (By.CSS_SELECTOR, "[data-test-id='sellerBtn']")
    CATEGORIES_MENU: tuple[str, str] = (
        By.CSS_SELECTOR,
        "[data-test-id='showCategoriesBtn']",
    )
    NAVIGATION_MENU: tuple[str, str] = (
        By.CSS_SELECTOR,
        "[data-test-id='footerMainBtn']",
    )


class Footer(PageComponent):
    def __init__(self, driver: WebDriver):
        super().__init__(driver, (By.TAG_NAME, "APP-FOOTER"))
        self.spinner_preloader: SpinnerPreloader = SpinnerPreloader(self.driver)
        self.navigation_menu: NavigationMenu = NavigationMenu(self.driver)
        self.categories_menu: CategoriesMenu = CategoriesMenu(self.driver)

    def click_buyer_mode(self) -> None:
        buyer_mode = self.get_component().find_element(*FooterLocators.BUYER_BUTTON)
        buyer_mode.click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def click_on_categories_btn(self) -> None:
        categories_button = self.get_component().find_element(
            *FooterLocators.CATEGORIES_MENU
        )
        categories_button.click()
        self.categories_menu.is_present()

    def click_seller_mode(self) -> None:
        seller_mode = self.get_component().find_element(*FooterLocators.SELLER_BUTTON)
        seller_mode.click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def close_the_navigation_menu(self) -> None:
        nav_menu = self.get_element_navigation_menu()
        nav_menu.click()
        wait = WebDriverWait(self.driver, 3)
        locator = NavigationMenuLocators.CART
        wait.until(EC.invisibility_of_element_located(locator))

    def get_element_navigation_menu(self) -> WebElement:
        menu_element = self.driver.find_element(*FooterLocators.NAVIGATION_MENU)
        return menu_element

    def is_present(self) -> bool:
        return self.get_component().is_displayed()

    def is_present_condition(self) -> Callable[[], bool]:
        return EC.visibility_of_element_located(self._locator)

    def open_the_navigation_menu(self) -> None:
        nav_menu = self.get_element_navigation_menu()
        nav_menu.click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        self.navigation_menu.wait_for_all_navigation_menu()

    def wait_footer_elements_loaded(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.visibility_of_element_located(FooterLocators.NAVIGATION_MENU))
        wait.until(EC.visibility_of_element_located(FooterLocators.BUYER_BUTTON))
