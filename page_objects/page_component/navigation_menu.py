from typing import Callable

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from page_objects.orders_page import OrdersPage
from page_objects.page_component.page_component import PageComponent


class NavigationMenuLocators:
    CART: tuple[str, str] = (By.CSS_SELECTOR, "[data-test-id='cart']")
    FAVORITES: tuple[str, str] = (By.CSS_SELECTOR, "[data-test-id='favorites']")
    MESSAGES: tuple[str, str] = (By.CSS_SELECTOR, "[data-test-id='dialogs']")
    ORDERS: tuple[str, str] = (By.CSS_SELECTOR, "[data-test-id='orders']")
    PROFILE: tuple[str, str] = (By.CSS_SELECTOR, "[data-test-id='profile']")
    REVIEWS: tuple[str, str] = (By.CSS_SELECTOR, "[data-test-id='reviews']")
    WATCHLIST: tuple[str, str] = (By.CSS_SELECTOR, "[data-test-id='watchlist']")


class NavigationMenu(PageComponent):
    def __init__(self, driver: WebDriver):
        super().__init__(driver, (By.ID, "app-footer-menu"))

    def click_favorites_list(self) -> None:
        self.java_script_click_element(NavigationMenuLocators.FAVORITES)

    def click_messages(self) -> None:
        self.java_script_click_element(NavigationMenuLocators.MESSAGES)

    def click_orders(self) -> None:
        self.get_orders_element().click()
        OrdersPage(self.driver).wait_page_is_present()

    def click_profile(self) -> None:
        self.java_script_click_element(NavigationMenuLocators.PROFILE)

    def click_watch_list(self) -> None:
        self.java_script_click_element(NavigationMenuLocators.WATCHLIST)

    def get_orders_element(self) -> WebElement:
        orders_element: WebElement = self.driver.find_element(
            *NavigationMenuLocators.ORDERS
        )
        return orders_element

    def is_present(self) -> bool:
        return self.get_component().is_displayed()

    def is_present_condition(self) -> Callable[[], bool]:
        return EC.visibility_of_element_located(self._locator)

    def wait_for_all_navigation_menu(self) -> None:
        wait: WebDriverWait = WebDriverWait(self.driver, 10)

        locators: list[tuple[str, str]] = [
            NavigationMenuLocators.CART,
            NavigationMenuLocators.FAVORITES,
            NavigationMenuLocators.MESSAGES,
            NavigationMenuLocators.ORDERS,
            NavigationMenuLocators.PROFILE,
            NavigationMenuLocators.REVIEWS,
            NavigationMenuLocators.WATCHLIST,
        ]

        conditions: list[Callable[[], bool]] = [
            EC.visibility_of_element_located(locator) for locator in locators
        ]
        wait.until(EC.all_of(*conditions))
