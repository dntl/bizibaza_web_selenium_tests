from abc import ABC, abstractmethod
from typing import Final, Tuple, Callable
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait


class PageComponent(ABC):
    TIME_OUT: Final = 10

    def __init__(self, driver: WebDriver, locator: Tuple[str, str]):
        self.driver = driver
        self._locator = locator

    @abstractmethod
    def is_present_condition(self) -> Callable:
        """Must return expected condition."""
        ...

    @abstractmethod
    def is_present(self) -> bool:
        """True if the element is presented (visible)."""
        ...

    def wait_present(self) -> None:
        WebDriverWait(self.driver, self.TIME_OUT).until(self.is_present_condition())

    def locator(self) -> Tuple[str, str]:
        return self._locator

    def get_component(self) -> WebElement:
        return self.driver.find_element(*self._locator)

    def is_visible(self, locator: Tuple[str, str]) -> bool:
        elements = self.driver.find_elements(*locator)
        return len(elements) > 0 and elements[0].is_displayed()

    def get_element(self, locator: tuple[str, str]) -> WebElement:
        return self.get_component().find_element(*locator)

    def get_element_text(self, locator: tuple[str, str]) -> str:
        return self.get_component().find_element(*locator).text.strip()

    def get_element_value(self, locator: tuple[str, str]) -> str:
        result = self.get_element(locator).get_property("value")
        assert isinstance(result, str)
        return result

    def java_script_click_element(self, locator: tuple[str, str]) -> None:
        element = self.get_element(locator)
        self.driver.execute_script("arguments[0].click()", element)
