from typing import Callable

from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from page_objects.page_component.page_component import PageComponent


class InputField(PageComponent):
    def __init__(self, driver: WebDriver):
        super().__init__(driver, (By.TAG_NAME, "app-footer-menu"))

    def is_present_condition(self) -> Callable:
        return EC.visibility_of_element_located(self._locator)

    def is_present(self) -> bool:
        return self.get_component().is_displayed()

    def set_text_enter(self, locator: str, stringtext: str) -> None:
        # This method set text into input field and press Enter:
        self.driver.find_element(By.CSS_SELECTOR, locator).click()
        self.driver.find_element(By.CSS_SELECTOR, locator).send_keys(stringtext)
        self.driver.find_element(By.CSS_SELECTOR, locator).send_keys(Keys.ENTER)

    def type_text(self, locator: str, stringtext: str) -> None:
        # This method type text into input field
        self.driver.find_element(By.CSS_SELECTOR, locator).clear()
        self.driver.find_element(By.CSS_SELECTOR, locator).send_keys(stringtext)
