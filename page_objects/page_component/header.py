from typing import Callable

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.expected_conditions import all_of
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.page_component.page_component import PageComponent
from page_objects.page_component.spinner_preloader import SpinnerPreloader


class Header(PageComponent):
    def __init__(self, driver: WebDriver):
        super().__init__(driver, (By.TAG_NAME, "app-header"))
        self.spinner_preloader = SpinnerPreloader(self.driver)

    def is_present_condition(self) -> Callable:
        return EC.visibility_of_element_located(self._locator)

    def is_present(self) -> bool:
        return self.get_component().is_displayed()

    def wait_header_elements_loaded(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='cartItemCounterHeader']")
            )
        )
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='orderItemCounterHeaderButton']")
            )
        )
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='goToRootPage']")
            )
        )

    def get_cart_button(self) -> WebElement:
        cart_button = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='cartItemCounterHeader']"
        )
        return cart_button

    def click_on_the_cart(self) -> None:
        self.get_cart_button().click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def get_orders_button(self) -> WebElement:
        orders_button = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='orderItemCounterHeaderButton']"
        )
        return orders_button

    def click_on_orders_button(self) -> None:
        self.get_orders_button().click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def get_bizibaza_logo(self) -> WebElement:
        bizibaza_element = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='goToRootPage']"
        )
        return bizibaza_element

    def click_bizibaza_logo(self) -> None:
        self.get_bizibaza_logo().click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        # We expect the Shopping list page to load.
        # DO NOT refer to the page object of the Shopping list page to avoid Circular dependency import errors.
        data_test_ids: list[str] = ["customItemInput", "goToRootPage"]
        conditions: list = list(
            map(
                lambda data_test_id: EC.element_to_be_clickable(
                    (By.CSS_SELECTOR, f"[data-test-id='{data_test_id}'")
                ),
                data_test_ids,
            )
        )
        WebDriverWait(self.driver, 10).until(all_of(*conditions))
