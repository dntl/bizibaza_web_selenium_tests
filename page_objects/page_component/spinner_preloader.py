from typing import Callable

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from page_objects.page_component.page_component import PageComponent


class SpinnerPreloader(PageComponent):
    def __init__(self, driver: WebDriver):
        super().__init__(driver, (By.CSS_SELECTOR, "[data-test-id='spinner']"))

    def is_present_condition(self) -> Callable:
        return EC.visibility_of_element_located(self._locator)

    def is_present(self) -> bool:
        return self.get_component().is_displayed()

    def wait_till_modal_spinner_is_disappeared(self) -> None:
        spinner = WebDriverWait(self.driver, timeout=30).until(
            EC.invisibility_of_element((By.CSS_SELECTOR, "[data-test-id='spinner']"))
        )
        return spinner
