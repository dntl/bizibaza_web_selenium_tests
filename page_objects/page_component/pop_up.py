from typing import Callable

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC

from page_objects.page_component.page_component import PageComponent


class PopUp(PageComponent):
    def __init__(self, driver: WebDriver):
        super().__init__(
            driver,
            (By.CSS_SELECTOR, "[data-test-id='notificationPrivacyPolicyCloseBtn']"),
        )
        self.driver = driver

    def is_present_condition(self) -> Callable:
        return EC.visibility_of_element_located(self._locator)

    def is_present(self) -> bool:
        return self.get_component().is_displayed()

    def close_privacy_policy(self) -> None:
        self.is_present_condition()
        self.get_component().click()
