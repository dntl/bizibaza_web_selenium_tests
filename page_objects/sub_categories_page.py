from selenium.webdriver.remote.webelement import WebElement

from page_objects.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class SubCategoriesPage(BasePage):
    page_url = "/goods-nav"

    def get_url(self) -> str:
        return self.page_url

    def select_subcategory(self) -> None:
        select_sub = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='subcategory_1']"
        )
        select_sub.click()

    def select_subcategory_entry(self) -> None:
        # This function clicks on the Subcategory entry by selecting the first value
        # in the list of subcategories that each subcategory has.
        select_sub_entry = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='entry_0']"
        )
        select_sub_entry.click()

    def check_entry_title_ok(self, text: str) -> WebElement:
        # This function checks that this position is checked and waits for XPath
        # matched element found that its class name == 'entry__title ok' and
        # the text of its nested element "text" (set in test)
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    f"//li[contains(@class, 'entry__title ok')]/*[contains(text(), '{text}')]",
                )
            )
        )
        return element

    def wait_page_is_present(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        all_subcategories: WebElement = wait.until(
            EC.visibility_of_all_elements_located((By.CLASS_NAME, "subcat__title"))
        )
