from decimal import Decimal

from selenium.webdriver.common.keys import Keys

from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from page_objects.base_page import BasePage
from model.my_search_results import SearchResult
from selenium.webdriver.support.ui import Select
from price_parser import parse_price, Price

from page_objects.page_component.header import Header
from page_objects.page_component.spinner_preloader import SpinnerPreloader


class SearchPage(BasePage):
    page_url = "/search"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.header = Header(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def click_search_button(self) -> None:
        search_button = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='searchLink']"
        )
        search_button.click()
        # We are waiting for the disappearance of the pre-loader (spinner) again after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        search_page = SearchPage(self.driver)
        search_page.wait_page_is_present()

    def click_on_searched_product_by_name(self, text: str) -> None:
        wait = WebDriverWait(self.driver, 10)
        product = wait.until(
            EC.element_to_be_clickable((By.XPATH, f"//*[contains(text(), '{text}')]"))
        )
        product.click()

    def click_on_searched_sweet_apple(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        product = wait.until(
            EC.element_to_be_clickable(
                (By.XPATH, "//*[contains(text(),'Sweet apple')]")
            )
        )
        product.click()

    def get_first_search_result_title(self) -> str:
        """
        Returns the name of the first search result.
        :return:
        """
        first_search_result_title = self.driver.find_element(
            By.CSS_SELECTOR, "abstractListProductTitle_0"
        )
        return first_search_result_title.text

    def get_current_page_search_results_price(self) -> list[SearchResult]:
        item_elements = self.driver.find_elements(
            By.CLASS_NAME, "abstract-list__description"
        )
        search_results: list[SearchResult] = []

        for item_element in item_elements:
            # Get a string containing the price tag and units of measurement
            raw_price: str = item_element.find_element(By.CLASS_NAME, "currency").text
            # Divide the string into parts and select the zero value from the array, i.e. price tag
            price_str: str = raw_price.split("/")[0]
            # Parse this value from str to Decimal, ignoring the currency sign, because we only need a numeric value
            price: Price = parse_price(price_str)

            if price.amount is None:
                raise ValueError("Failed to parse price")

            if not isinstance(price.amount, Decimal):
                raise TypeError("Invalid amount type")
            # Pass the value of the price tag to the search results
            # See file model -> search_results (price)
            search_result = SearchResult(price=price.amount)
            # Add each price tag to the array individually to get the full list
            search_results.append(search_result)

        return search_results

    def clear_search_field(self) -> None:
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='searchInp']")
            )
        ).clear()

    def input_keyword_to_search_field(self, keyword: str) -> None:
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='searchInp']")
            )
        ).send_keys(keyword)

    def press_enter_on_search_field(self) -> None:
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='searchInp']")
            )
        ).send_keys(Keys.ENTER)

    def click_on_search_result_by_name(self, item: str) -> None:
        wait = WebDriverWait(self.driver, 10)
        product = wait.until(
            EC.element_to_be_clickable((By.XPATH, f"//*[contains(text(), '{item}')]"))
        )
        product.click()

    def search_by_keyword(self, keyword: str) -> None:
        self.clear_search_field()
        self.input_keyword_to_search_field(keyword)
        self.press_enter_on_search_field()

    def search_and_open_item(self, keyword: str, item: str) -> None:
        self.clear_search_field()
        self.search_by_keyword(keyword)
        self.click_on_search_result_by_name(item)

    def sort_items(self, sortOption: str) -> None:
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='sortSelect']")
            )
        ).click()
        select1 = Select(
            wait.until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, "[data-test-id='sortSelect']")
                )
            )
        )
        select1.select_by_value(sortOption)

    def wait_page_is_present(self) -> None:
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='searchInp']")
            )
        )
