from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.remote.webelement import WebElement
from page_objects.base_page import BasePage
from page_objects.cart_report_page import CartReportPage
from page_objects.page_component.modal_window import ModalWindow
from decimal import Decimal
from price_parser import Price


class PaymentPage(BasePage):
    page_url = "/payment"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.modal_window = ModalWindow(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def click_on_back_from_payment(self) -> None:
        back = self.driver.find_element(By.CSS_SELECTOR, "[data-test-id='backLink']")
        back.click()

    def click_on_pay_button(self) -> None:
        self.get_pay_button().click()
        WebDriverWait(self.driver, 10).until(self.modal_window.is_present_condition())

    def get_payment_total_sum(self) -> Decimal:
        payment_total_text = self.get_pay_button().text
        # Remove the word "Pay" from the payment total text
        payment_total_text = payment_total_text.replace("Pay", "").strip()

        # Parse the price using price_parser library
        payment_total_price = Price.fromstring(payment_total_text)

        # Check if the payment_total_price amount is of type Decimal
        if not isinstance(payment_total_price.amount, Decimal):
            raise TypeError("Invalid amount type")

        # Return the Decimal amount
        return Decimal(payment_total_price.amount)

    def get_pay_button(self) -> WebElement:
        return self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='PayByCartNumberButton']"
        )

    def get_pay_button_status(self) -> str:
        # Get status of the Pay button - disabled or enabled
        status = self.get_pay_button().get_attribute("disabled")
        return status

    def get_input_card_number(self) -> WebElement:
        # Before looking for the field for entering the card number, we need to switch
        # to the Stripe frame, which is embedded in the page from the payment system domain.
        iframe = self.driver.find_element(By.XPATH, "//*[@id='card-element']/div/iframe")
        self.driver.switch_to.frame(iframe)

        # Wait for the card number input to be present
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located((By.XPATH, "//input[@name='cardnumber']"))
        )

        return self.driver.find_element(By.XPATH, "//input[@name='cardnumber']")

    def get_input_exp_date(self) -> WebElement:
        return self.driver.find_element(By.XPATH, "*//input[@placeholder='MM / YY']")

    def get_input_cvc(self) -> WebElement:
        return self.driver.find_element(By.XPATH, "*//input[@placeholder='CVC']")

    def get_name_on_card(self) -> WebElement:
        return self.driver.find_element(By.XPATH, "//input[@placeholder='John Doe']")

    def input_card_number(self, number: str) -> None:
        self.get_input_card_number().send_keys(number)

    def input_exp_date(self, date: float) -> None:
        self.get_input_exp_date().send_keys(date)

    def input_cvc(self, cvc: int) -> None:
        self.get_input_cvc().send_keys(cvc)
        # We switch from the payment system frame after entering the last value.
        self.driver.switch_to.default_content()

    def input_name_on_card(self, name_on_card: str) -> None:
        self.get_name_on_card().send_keys(name_on_card)
        WebDriverWait(self.driver, 10).until(
            EC.text_to_be_present_in_element_value(
                (By.XPATH, "//input[@placeholder='John Doe']"), f"{name_on_card}"
            )
        )

    def wait_page_is_present(self) -> None:
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='PayByCartNumberButton']")
            )
        )

    def click_close_button_on_modal(self) -> None:
        self.modal_window.click_ok_button()
        CartReportPage(self.driver).wait_page_is_present()
