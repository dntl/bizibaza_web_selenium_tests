from selenium.webdriver.remote.webdriver import WebDriver

from page_objects.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.page_component.modal_window import ModalWindow

from page_objects.page_component.spinner_preloader import SpinnerPreloader


class ProfilePage(BasePage):
    page_url = "/profile"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.modal_window = ModalWindow(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def wait_page_is_present(self) -> None:
        WebDriverWait(self.driver, 10).until(
            EC.text_to_be_present_in_element(
                (By.CSS_SELECTOR, "[data-test-id=newAddress]"), "+ Add new address"
            )
        )
