from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_objects.base_page import BasePage


# Class for all operations with Map


class MapPage(BasePage):
    page_url = "/map-search"

    def get_url(self) -> str:
        return self.page_url

    def wait_page_is_present(self) -> None:
        wait = WebDriverWait(self.driver, 15)
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='keywordMapSearchHeaderTitle']")
            )
        )
