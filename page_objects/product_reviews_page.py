from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_objects.base_page import BasePage
from page_objects.page_component.header import Header
from page_objects.page_component.modal_window import ModalWindow
from page_objects.page_component.spinner_preloader import SpinnerPreloader


class ProductReviewsPage(BasePage):
    page_url = "/reviews-item"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.modal_window = ModalWindow(self.driver)
        self.header = Header(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def wait_product_reviews_page_loaded(self, product: str) -> None:
        # We are waiting for the disappearance of the pre-loader (spinner)
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        wait = WebDriverWait(self.driver, 10)
        product_title = wait.until(
            EC.text_to_be_present_in_element(
                (By.CSS_SELECTOR, "[data-test-id='headerTitle']"), f"{product}"
            )
        )
        return product_title

    def click_back_button(self) -> None:
        self.java_script_click((By.CSS_SELECTOR, "[data-test-id='backLink']"))
        # We are waiting for the disappearance of the pre-loader (spinner)
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def wait_page_is_present(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.visibility_of_all_elements_located(
                (By.CSS_SELECTOR, "[data-test-id='showFilter']")
            )
        )
