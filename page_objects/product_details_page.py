from typing import Final, Union

from price_parser import Price
from selenium.webdriver import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.expected_conditions import all_of
from decimal import Decimal

from page_objects.bizipix_viewer_page import BiziPixViewerPage
from page_objects.page_component.footer import Footer
from page_objects.page_component.header import Header
from page_objects.page_component.modal_window import ModalWindow
from page_objects.page_component.navigation_menu import NavigationMenu
from page_objects.page_component.spinner_preloader import SpinnerPreloader
from page_objects.search_page import SearchPage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_objects.base_page import BasePage


ADD_WATCHLIST: Final = EC.visibility_of_element_located(
    (By.CLASS_NAME, "icon-eye_outlined")
)
REMOVE_WATCHLIST: Final = EC.visibility_of_all_elements_located(
    (By.CLASS_NAME, "icon-eye_filled")
)
ADD_FAVORITES: Final = EC.visibility_of_all_elements_located(
    (By.CLASS_NAME, "icon-favorites_outlined")
)
REMOVE_FAVORITES: Final = EC.visibility_of_all_elements_located(
    (By.CLASS_NAME, "icon-favorites_filled")
)


class ProductDetailsPage(BasePage):
    page_url = "/product-details/"

    def __init__(self, driver: WebDriver, product_id: str):
        super().__init__(driver)
        self.product_id = product_id
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.modal_window = ModalWindow(self.driver)
        self.navigation_menu = NavigationMenu(self.driver)
        self.header = Header(self.driver)
        self.footer = Footer(self.driver)

    def get_url(self) -> str:
        return self.page_url + self.product_id

    def click_favorites_list_button(self) -> None:
        self.driver.execute_script(
            "arguments[0].click()", self.get_favorites_list_button()
        )
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def wait_enabled_fav_list_button(self) -> None:
        WebDriverWait(self.driver, 10).until(REMOVE_FAVORITES)

    def wait_disabled_fav_list_button(self) -> None:
        WebDriverWait(self.driver, 10).until(ADD_FAVORITES)

    def wait_enabled_watch_list_button(self) -> None:
        WebDriverWait(self.driver, 10).until(REMOVE_WATCHLIST)

    def wait_disabled_watch_list_button(self) -> None:
        WebDriverWait(self.driver, 10).until(ADD_WATCHLIST)

    def click_watch_list_button(self) -> None:
        self.driver.execute_script("arguments[0].click()", self.get_watch_list_button())
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def get_favorites_list_button(self) -> WebElement:
        return self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='toggle-favlist-status']"
        )

    def get_watch_list_button(self) -> WebElement:
        return self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='toggle-watchlist']"
        )

    def get_favlist_button_status(self) -> str:
        # We make a request in what position the button is - on or off.
        status = (
            WebDriverWait(self.driver, 10)
            .until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, "[data-test-id='toggle-favlist-status']")
                )
            )
            .get_attribute("data-state-active")
        )
        return status

    def get_watchlist_button_status(self) -> str:
        # We make a request in what position the button is - on or off.
        status = (
            WebDriverWait(self.driver, 10)
            .until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, "[data-test-id='toggle-watchlist']")
                )
            )
            .get_attribute("data-state-active")
        )
        return status

    def add_quantity(self, number: int | str) -> None:
        # input the number into 'quantity' field on Product page:
        wait = WebDriverWait(self.driver, 10)
        elem = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='detailPriceQty']")
            )
        )
        elem.clear()
        elem.send_keys(number)

    def add_price_alert(self, number: float | str) -> None:
        wait = WebDriverWait(self.driver, 10)
        price_alert = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='detailAlertQty']")
            )
        )
        price_alert.clear()
        price_alert.send_keys(number)
        price_alert.send_keys(Keys.ENTER)

    def back_to_shopping_list_from_product_details(self) -> None:
        # Double-click on the back button to return to the Shopping list
        self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='searchResultsLink']"
        ).click()
        self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='shoppingLink']"
        ).click()

    def click_add_to_cart_button(self) -> None:
        self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='detailAddToCart']"
        ).click()
        # We are waiting for the disappearance of the pre-loader (spinner) again after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        WebDriverWait(self.driver, 10).until(self.modal_window.is_present_condition())

    def click_buy_now_button(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='detailBuyNow']"
        ).click()
        # We are waiting for the disappearance of the pre-loader (spinner) again after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        WebDriverWait(self.driver, 10).until(self.modal_window.is_present_condition())

    def back_from_the_product_page(self) -> None:
        self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='searchResultsLink']"
        ).click()
        # We are waiting for the disappearance of the pre-loader (spinner) again after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()

    def click_remove_from_cart_button(self) -> None:
        wait = WebDriverWait(self.driver, 20)
        remove = wait.until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    "//*[contains(@data-test-id, 'detailAddToCart')]/span[contains(text(), 'Remove from Cart')]",
                )
            )
        )
        remove.click()
        WebDriverWait(self.driver, 10).until(self.modal_window.is_present_condition())

    def click_ok_alert_message(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        alert = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='noButton']")
            )
        )
        alert.click()

    def click_on_bizipix_viewer(self) -> None:
        bizipix = self.driver.find_element(By.CSS_SELECTOR, "[data-test-id='BiziPix']")
        bizipix.click()
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        BiziPixViewerPage(self.driver).wait_page_is_present()

    def click_on_product_reviews(self) -> None:
        wait = WebDriverWait(self.driver, timeout=10)
        reviews = wait.until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "[data-test-id='showReviews']")
            )
        )
        reviews.click()

    def click_on_seller_details(self) -> None:
        wait = WebDriverWait(self.driver, timeout=10)
        reviews = wait.until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, "[data-test-id='showSeller']"))
        )
        reviews.click()

    def click_non_gmo_certificate(self) -> None:
        wait = WebDriverWait(self.driver, timeout=2)
        reviews = wait.until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "[data-test-id='showGMOCertificate']")
            )
        )
        reviews.click()

    def click_organic_certificate(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        organic = wait.until(
            EC.presence_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='showOrganicCertificate']")
            )
        )
        organic.click()

    def find_market_name_on_product_page(self, text: str):
        market_name = EC.element_to_be_clickable(
            (By.XPATH, f"//*[contains(text(), '{text}')]")
        )
        WebDriverWait(self.driver, 5).until(market_name)

    def wait_certificate_is_opened(self) -> None:
        wait = WebDriverWait(self.driver, timeout=10)
        wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='modal_image_content']")
            )
        )

    def wait_page_is_present(self) -> None:
        # The method wait main elements on the page

        data_test_ids: list[str] = [
            "detailTitle",
            "detailPriceQty",
            "detailAlertQty",
            "priceAlertSwitch",
            "detailSwitchAlert",
            "showReviews",
            "detailRateStars",
            "detailCurrentQty",
            "detailCost",
            "showGMOCertificate",
            "showOrganicCertificate",
            "detailAddToCart",
            "detailBuyNow",
            "toggle-watchlist",
            "toggle-favlist-status",
        ]
        conditions_first: list = list(
            map(
                lambda data_test_id: EC.presence_of_all_elements_located(
                    (By.CSS_SELECTOR, f"[data-test-id='{data_test_id}'")
                ),
                data_test_ids,
            )
        )
        WebDriverWait(self.driver, 20).until(all_of(*conditions_first))

    def get_remove_from_cart_button(self) -> WebElement:
        element = WebDriverWait(self.driver, 10).until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    "//*[contains(@data-test-id, 'detailAddToCart')]/span[contains(text(), 'Remove from Cart')]",
                )
            )
        )
        return element

    def get_remove_from_cart_button_text(self) -> str:
        button = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='detailAddToCart']"
        )
        return button.text

    def get_cost_of_product(self) -> Decimal:
        wait = WebDriverWait(self.driver, 10)
        cost_text = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='detailCost']")
            )
        ).text
        price = Price.fromstring(cost_text)
        if not isinstance(price.amount, Decimal):
            raise TypeError("Invalid amount type")

        return Decimal(price.amount)

    def get_exceeds_error_text(self) -> str:
        wait = WebDriverWait(self.driver, 10)
        # Get error text
        error_text = wait.until(
            EC.visibility_of_element_located((By.CLASS_NAME, "error-message"))
        ).text
        return error_text

    def get_invisibility_cart_counter(self) -> None:
        wait = WebDriverWait(self.driver, timeout=10)
        wait.until(
            EC.invisibility_of_element_located(
                (
                    By.XPATH,
                    "//*[contains(@aria-labelledby, "
                    "'There are 1 products in your shopping cart')]",
                )
            )
        )

    def get_on_off_switch_status(self) -> str:
        result = (
            WebDriverWait(self.driver, 10)
            .until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, "[data-test-id='priceAlertSwitch']")
                )
            )
            .get_attribute("ng-reflect-state")
        )
        return result

    def get_value_from_quantity_field(self) -> str:
        wait = WebDriverWait(self.driver, 10)
        result = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='detailPriceQty']")
            )
        ).get_attribute("value")
        return result

    def get_value_from_price_alert_field(self) -> str:
        wait = WebDriverWait(self.driver, 10)
        result = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='detailAlertQty']")
            )
        ).get_attribute("value")
        return result

    def get_disabled_attribute_from_price_alert_field(self) -> str:
        wait = WebDriverWait(self.driver, 10)
        result = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='detailAlertQty']")
            )
        ).get_attribute("disabled")
        return result

    def go_to_the_product_page_by_search(self, keyword: str, product: str) -> None:
        search_page = SearchPage(self.driver)

        # Click on the Search icon
        search_page.click_search_button()

        # Click on the Search field, type product name and submit
        search_page.search_by_keyword(keyword)

        # Click on a product in the search results
        search_page.click_on_searched_product_by_name(product)
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        self.wait_page_is_present()

    def switch_on_off_price_alert(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        switcher = wait.until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "[data-test-id='priceAlertSwitch']")
            )
        )
        switcher.click()

    def wait_till_alert_qty_on_or_off(self, status: str) -> None:
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.text_to_be_present_in_element_attribute(
                (By.CSS_SELECTOR, "[data-test-id='detailAlertQty']"),
                "willValidate",
                f"{status}",
            )
        )

    def get_zero_cost(self) -> bool:
        wait = WebDriverWait(self.driver, 10)
        zero: WebElement = wait.until(
            EC.text_to_be_present_in_element(
                (By.CSS_SELECTOR, "[data-test-id='detailCost']"), "$0.00"
            )
        )
        return True
