from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_objects.base_page import BasePage


class BiziPixViewerPage(BasePage):
    page_url = "/bizipix-viewer"

    def get_url(self) -> str:
        return self.page_url

    def close_bizipix_viewer(self) -> None:
        wait = WebDriverWait(self.driver, timeout=2)
        bizipix = wait.until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "[data-test-id='closeButton']")
            )
        )
        bizipix.click()

    def wait_page_is_present(self) -> None:
        WebDriverWait(self.driver, 10).until(
            EC.visibility_of_all_elements_located((By.CLASS_NAME, "bizipix-menu"))
        )
