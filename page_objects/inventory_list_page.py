from page_objects.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class InventoryListPage(BasePage):
    page_url = "/inventory-list"

    def get_url(self) -> str:
        return self.page_url

    def wait_page_is_present(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        wait.until(
            EC.visibility_of_all_elements_located(
                (By.CLASS_NAME, "inventory-list__item")
            )
        )
