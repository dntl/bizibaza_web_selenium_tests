from selenium.webdriver.remote.webdriver import WebDriver

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from page_objects.base_page import BasePage

from page_objects.page_component.spinner_preloader import SpinnerPreloader


class SellerDetailsPage(BasePage):
    page_url = "/shopper-details"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def wait_page_is_present(self) -> None:
        # We are waiting for the disappearance of the pre-loader (spinner)
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(
            EC.visibility_of_element_located(
                (By.XPATH, "//*[text() = 'Seller Details']")
            )
        )

    def click_back_button(self) -> None:
        seller_back = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='searchResultsLink']"
        )
        seller_back.click()
        # We are waiting for the disappearance of the pre-loader (spinner)
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
