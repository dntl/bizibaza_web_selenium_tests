from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.expected_conditions import all_of

from page_objects.base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from page_objects.page_component.categories_menu import CategoriesMenu
from page_objects.page_component.footer import Footer
from page_objects.page_component.header import Header
from page_objects.page_component.modal_window import ModalWindow
from page_objects.page_component.navigation_menu import NavigationMenu
from page_objects.page_component.spinner_preloader import SpinnerPreloader
from page_objects.product_details_page import ProductDetailsPage


class SearchbySubcategoryPage(BasePage):
    page_url = "/search"

    def __init__(self, driver: WebDriver):
        super().__init__(driver)
        self.spinner_preloader = SpinnerPreloader(self.driver)
        self.navigation_menu = NavigationMenu(self.driver)
        self.categories_menu = CategoriesMenu(self.driver)
        self.modal_window = ModalWindow(self.driver)
        self.header = Header(self.driver)
        self.footer = Footer(self.driver)

    def get_url(self) -> str:
        return self.page_url

    def expand_the_market_list(self) -> None:
        wait = WebDriverWait(self.driver, 5)
        element = wait.until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    "//*[contains(@class,'accordion__header__arrow')] [contains(@data-test-id, 'switchItem_0')]",
                )
            )
        )
        element.click()

    def search_title_of_subcategory_page(self) -> WebElement:
        wait = WebDriverWait(self.driver, 5)
        search_title_element = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='searchKey']")
            )
        )
        return search_title_element

    def get_text_search_title_of_subcategory_page(self) -> str:
        return self.search_title_of_subcategory_page().get_attribute("textContent")

    def wait_page_is_present(self) -> None:
        # Search the page for all the main elements
        wait = WebDriverWait(self.driver, 5)

        class_names: list[str] = [
            "icon-right-open",
            "abstract-list__product__title",
            "rating",
        ]
        conditions: list = list(
            map(
                lambda class_name: EC.visibility_of_all_elements_located(
                    (By.CLASS_NAME, f"{class_name}")
                ),
                class_names,
            )
        )
        wait.until(all_of(*conditions))

    def wait_all_markets_elements(self) -> None:
        # Waiting for all elements of the Market List
        class_names: list[str] = [
            "accordion__header__arrow",
            "accordion__header__title",
            "accordion__header__label",
            "market-table__item",
        ]
        conditions: list = list(
            map(
                lambda class_name: EC.visibility_of_all_elements_located(
                    (By.CLASS_NAME, f"{class_name}")
                ),
                class_names,
            )
        )
        WebDriverWait(self.driver, 5).until(all_of(*conditions))

        # Checking the captions in the table header
        WebDriverWait(self.driver, 5).until(
            EC.text_to_be_present_in_element(
                (By.CLASS_NAME, "market-table__header__title"), "Market"
            )
        )
        WebDriverWait(self.driver, 5).until(
            EC.text_to_be_present_in_element(
                (By.CLASS_NAME, "market-table__header__qti"), "QTY"
            )
        )

    def wait_elements_on_the_header_of_search(self) -> None:
        # Search on the page for all items in the search header by Subcategories
        data_test_ids: list[str] = [
            "shoppingLink",
            "resultsCount",
            "sortSelect",
            "mapBtnTopContainer",
        ]
        conditions: list = list(
            map(
                lambda data_test_id: EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, f"[data-test-id='{data_test_id}']")
                ),
                data_test_ids,
            )
        )
        WebDriverWait(self.driver, 5).until(all_of(*conditions))

    def enter_qty_to_first_item(self, keyword: int) -> None:
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(
            EC.visibility_of_element_located(
                (
                    By.XPATH,
                    "//*[contains(@data-test-id, 'abstractListProduct_0')]"
                    "//*[contains(@data-test-id, 'abstractListProductQty')]",
                )
            )
        )
        element.send_keys(keyword)

    def click_on_the_first_item_in_list(self) -> None:
        wait = WebDriverWait(self.driver, 10)
        element = wait.until(
            EC.element_to_be_clickable(
                (By.CSS_SELECTOR, "[data-test-id" "='abstractListProductTitle_0']")
            )
        )
        element.click()

    def click_on_product_by_name(self, text: str) -> None:
        wait = WebDriverWait(self.driver, 10)
        product = wait.until(
            EC.element_to_be_clickable((By.XPATH, f"//*[contains(text(), '{text}')]"))
        )
        product.click()

    def back_from_the_subcategory_map(self) -> None:
        map_back = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='backLink']"
        )
        map_back.click()

    def open_market_list_of_the_first_item(self) -> None:
        wait = WebDriverWait(self.driver, 5)
        element = wait.until(
            EC.visibility_of_element_located(
                (By.CSS_SELECTOR, "[data-test-id='switchItem_0']")
            )
        )
        element.click()

    def click_on_the_market_on_search_by_subcategory(
        self, text: str, product_id: str
    ) -> None:
        market_on_search_by = EC.visibility_of_element_located(
            (By.XPATH, f"//*[contains(text(), '{text}')]")
        )
        WebDriverWait(self.driver, 5).until(market_on_search_by).click()
        # We are waiting for the disappearance of the pre-loader (spinner) after click
        self.spinner_preloader.wait_till_modal_spinner_is_disappeared()
        ProductDetailsPage(self.driver, product_id).wait_page_is_present()

    def open_map_by_subcategory(self) -> None:
        map_button = self.driver.find_element(
            By.CSS_SELECTOR, "[data-test-id='mapBtnTopContainer']"
        )
        map_button.click()

    def check_maps_elements_search_by_subcategory(self, keyword: str) -> None:
        wait = WebDriverWait(self.driver, 10)
        # Checking the page title
        wait.until(
            EC.text_to_be_present_in_element(
                (
                    By.XPATH,
                    "//*[contains(@data-test-id,'keywordMapSearchHeaderTitle')]",
                ),
                "Map for " f"{keyword}",
            )
        )

    def get_product_is_not_sold_out(self) -> bool:
        wait = WebDriverWait(self.driver, timeout=2)
        wait.until(
            EC.invisibility_of_element(
                (By.CSS_SELECTOR, "[data-test-id='soldOutLabel_0']")
            )
        )
        return True
