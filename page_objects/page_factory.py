from selenium.webdriver.remote.webdriver import WebDriver

from page_objects.cart_report_page import CartReportPage
from page_objects.change_password_page import ChangePasswordPage
from page_objects.chat_page import ChatPage
from page_objects.favorites_list_page import FavoritesListPage
from page_objects.history_page import HistoryPage
from page_objects.map_page import MapPage
from page_objects.profile_page import ProfilePage
from page_objects.sub_categories_page import SubCategoriesPage
from page_objects.watch_list_page import WatchListPage
from page_objects.cart_page import CartPage
from page_objects.inventory_list_page import InventoryListPage
from page_objects.login_page import LoginPage
from page_objects.orders_page import OrdersPage
from page_objects.search_page import SearchPage
from page_objects.search_by_subcategory_page import SearchbySubcategoryPage
from page_objects.settings_page import SettingsPage
from page_objects.shopping_list_page import ShoppingListPage
from page_objects.product_details_page import ProductDetailsPage
from page_objects.bizipix_viewer_page import BiziPixViewerPage
from page_objects.payment_page import PaymentPage
from page_objects.product_reviews_page import ProductReviewsPage
from page_objects.seller_details_page import SellerDetailsPage
from page_objects.page_component.navigation_menu import NavigationMenu
from page_objects.page_component.categories_menu import CategoriesMenu


class PageFactory:
    def __init__(self, web_driver: WebDriver):
        self.driver = web_driver

    def bizipix_viewer_page(self) -> BiziPixViewerPage:
        return BiziPixViewerPage(self.driver)

    def cart_page(self) -> CartPage:
        return CartPage(self.driver)

    def cart_report_page(self) -> CartReportPage:
        return CartReportPage(self.driver)

    def categories_menu(self) -> CategoriesMenu:
        return CategoriesMenu(self.driver)

    def change_password_page(self) -> ChangePasswordPage:
        return ChangePasswordPage(self.driver)

    def chat_page(self) -> ChatPage:
        return ChatPage(self.driver)

    def favorites_list_page(self) -> FavoritesListPage:
        return FavoritesListPage(self.driver)

    def history_page(self) -> HistoryPage:
        return HistoryPage(self.driver)

    def inventory_list_page(self) -> InventoryListPage:
        return InventoryListPage(self.driver)

    def login_page(self) -> LoginPage:
        return LoginPage(self.driver)

    def map_page(self) -> MapPage:
        return MapPage(self.driver)

    def navigation_menu(self) -> NavigationMenu:
        return NavigationMenu(self.driver)

    def orders_page(self) -> OrdersPage:
        return OrdersPage(self.driver)

    def payment_page(self) -> PaymentPage:
        return PaymentPage(self.driver)

    def product_details_page(self, product_id: str) -> ProductDetailsPage:
        return ProductDetailsPage(self.driver, product_id)

    def product_reviews_page(self) -> ProductReviewsPage:
        return ProductReviewsPage(self.driver)

    def profile_page(self) -> ProfilePage:
        return ProfilePage(self.driver)

    def search_by_subcategory_page(self) -> SearchbySubcategoryPage:
        return SearchbySubcategoryPage(self.driver)

    def search_page(self) -> SearchPage:
        return SearchPage(self.driver)

    def seller_details_page(self) -> SellerDetailsPage:
        return SellerDetailsPage(self.driver)

    def settings_page(self) -> SettingsPage:
        return SettingsPage(self.driver)

    def shopping_list_page(self) -> ShoppingListPage:
        return ShoppingListPage(self.driver)

    def sub_categories_page(self) -> SubCategoriesPage:
        return SubCategoriesPage(self.driver)

    def watch_list_page(self) -> WatchListPage:
        return WatchListPage(self.driver)
