@Echo off
SET ENVIRONMENT_KIND=develop
SET SELENIUM_DRIVER_KIND=chrome
SET BACKEND_URL=https://dev-backend.bizibaza.com
SET FRONTEND_URL=https://dev-frontend.bizibaza.com
python.exe -m pytest test_objects -v --junitxml "junit-report.xml"
pause

REM .\run_test.bat
