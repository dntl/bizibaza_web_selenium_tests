from datetime import datetime
from typing import Any


class ApproximateTime:
    """
    param: delta - > Permissible difference between the two compared times in seconds
                     default = 70 sec.
    """

    def __init__(self, time: datetime, delta: int = 120):
        self.epoch_time = time.timestamp()
        self.delta = delta

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, ApproximateTime):
            return abs(self.epoch_time - other.epoch_time) < self.delta
        return False

    def __lt__(self, other: Any) -> bool:
        return self.epoch_time < other.epoch_time

    def __repr__(self) -> str:
        formatted_datetime = datetime.fromtimestamp(self.epoch_time)
        return (
            f"datetime.datetime({formatted_datetime.year}, {formatted_datetime.month}, "
            f"{formatted_datetime.day}, {formatted_datetime.hour}, {formatted_datetime.minute})"
        )
