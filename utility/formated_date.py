import pytz
from datetime import datetime, date


def format_date(date_string: str, date_formatter_str: str) -> date:
    return datetime.strptime(date_string, date_formatter_str).date()


def format_str_to_date_time(date_string: str, date_formatter_str: str) -> datetime:
    return datetime.strptime(date_string, date_formatter_str)


def convert_local_date_time_to_utc(local_datetime: datetime) -> datetime:
    return local_datetime.astimezone(pytz.UTC)


def str_to_datetime_utc(date_string: str, date_formatter_str: str) -> datetime:
    return convert_local_date_time_to_utc(
        format_str_to_date_time(date_string, date_formatter_str)
    ).replace(tzinfo=None)


def get_date_time_now() -> datetime:
    return datetime.now().replace(second=0, microsecond=0)


def get_time_now_utc() -> datetime:
    return convert_local_date_time_to_utc(get_date_time_now()).replace(tzinfo=None)


def get_current_datetime() -> datetime:
    now = datetime.now()
    formatted_datetime_str = now.strftime("%B %d, %Y %I:%M %p")
    formatted_datetime = datetime.strptime(formatted_datetime_str, "%B %d, %Y %I:%M %p")
    formatted_datetime_without_seconds = formatted_datetime.replace(second=0)
    return formatted_datetime_without_seconds
