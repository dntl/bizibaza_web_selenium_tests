from dataclasses import dataclass


@dataclass
class LoginResponse:
    login: str
    token: str
