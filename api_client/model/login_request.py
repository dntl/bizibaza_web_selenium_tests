from dataclasses import dataclass


@dataclass
class Account:
    login: str
    password: str


@dataclass
class LoginRequest:
    account: Account
