import requests
from factory import get_environment


def auth_logout(token: str) -> None:
    """

    :rtype: object
    """
    base_url = get_environment().get_backend_url()
    headers = {"token": token}
    r = requests.post(url=f"{base_url}/api/v1/logout", headers=headers, timeout=5)
    if r.status_code != 200:
        raise RuntimeError(
            f"Can't perform POST /api/v1/logout. Status: {r.status_code}, body: {r.json()}"
        )
