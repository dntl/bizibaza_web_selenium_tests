import json

import requests

from api_client.model.login_response import LoginResponse
from factory import get_environment


def auth_login(request: dict, headers: dict) -> LoginResponse:
    base_url = get_environment().get_backend_url()
    headers["Content-Type"] = "application/json"
    r = requests.post(
        url=f"{base_url}/api/v1/login",
        data=json.dumps(request),
        headers=headers,
        timeout=5,
    )
    if r.status_code != 200:
        raise RuntimeError(
            f"Can't perform POST /api/v1/login. Status: {r.status_code}, body: {r.json()}"
        )

    response = r.json()
    return LoginResponse(
        login=response["user"]["login"],
        token=response["token"],
    )


def login_guest() -> str:
    base_url = get_environment().get_backend_url()
    request_body: dict = {}
    headers = {
        "Content-Type": "application/json"
    }
    r = requests.post(
        url=f"{base_url}/api/v1/login_guest",
        json=request_body,
        headers=headers
    )
    if r.status_code != 200:
        raise RuntimeError(
            f"Can't perform POST /api/v1/login_guest. Status: {r.status_code}, body: {r.json()}"
        )

    response = r.json()
    return response["token"]
