from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.chrome.service import Service
import os

from typing import TypedDict

from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.firefox import GeckoDriverManager


class ScreenResolution(TypedDict):
    width: int
    height: int
    deviceScaleFactor: int
    mobile: bool


WINDOW_SIZES: dict[str, ScreenResolution] = {
    "DESKTOP_850X1980": {
        "width": 850,
        "height": 1980,
        "deviceScaleFactor": 1,
        "mobile": False,
    },
    "DESKTOP_4K": {
        "width": 3840,
        "height": 2160,
        "deviceScaleFactor": 1,
        "mobile": False,
    },
    "DESKTOP_1280X720": {
        "width": 1280,
        "height": 720,
        "deviceScaleFactor": 1,
        "mobile": False,
    },
    "IPHONE_12_MINI": {
        "width": 360,
        "height": 780,
        "deviceScaleFactor": 3,
        "mobile": True,
    },
    "IPHONE_12_PRO": {
        "width": 390,
        "height": 844,
        "deviceScaleFactor": 3,
        "mobile": True,
    },
    "412X914": {"width": 300, "height": 300, "deviceScaleFactor": 3, "mobile": True},
}


class WebDriverFactory:
    @staticmethod
    def get_driver() -> WebDriver:
        """
        :return:  WebDriver is an abstract class from which all other web drivers
        are derived (Chrome, Remote, Firefox, etc.)
        """

        # There should be an algorithm by which the factory selects a SPECIFIC IMPLEMENTATION of the driver.
        # There are three variants of the algorithm:
        # 1. By day of the week (jokingly, of course)
        # 2. By environment variable DRIVER_KIND (implemented by BitBucket Pipelines)
        # 3. By command line arguments at the time of running the tests.

        driver_kind: str = WebDriverFactory.get_driver_kind()
        if driver_kind == "remote":
            driver = WebDriverFactory.get_remote_driver()
        elif driver_kind == "chrome":
            driver = WebDriverFactory.get_chrome_driver()
        elif driver_kind == "firefox":
            driver = WebDriverFactory.get_firefox_driver()
        elif driver_kind == "safari":
            driver = WebDriverFactory.get_safari_driver()
        else:
            raise NotImplementedError(
                "Getting driver for " + driver_kind + " is not implemented yet."
            )

        return driver

    @staticmethod
    def get_driver_kind() -> str:
        driver_kind: str = os.environ["SELENIUM_DRIVER_KIND"].lower()
        return driver_kind

    @staticmethod
    def set_resolution(resolution: ScreenResolution, driver: WebDriver) -> None:
        driver.set_window_size(width=resolution["width"], height=resolution["height"])

    @staticmethod
    def get_window_resolution() -> ScreenResolution:
        window_config: str = os.environ["WINDOW_RESOLUTION"].upper()
        return WINDOW_SIZES[window_config]

    @staticmethod
    def set_geolocation(
        chrome_driver, latitude: float, longitude: float, accuracy: int = 1000
    ) -> None:
        chrome_driver.execute_cdp_cmd(
            cmd="Browser.grantPermissions", cmd_args={"permissions": ["geolocation"]}
        )
        chrome_driver.execute_cdp_cmd(
            cmd="Emulation.setGeolocationOverride",
            cmd_args={
                "latitude": latitude,
                "longitude": longitude,
                "accuracy": accuracy,
            },
        )

    @staticmethod
    def get_chrome_driver() -> WebDriver:
        options = Options()
        prefs = {
            "profile.default_content_setting_values.notifications": 2,
            "profile.default_content_setting_values" ".geolocation": 2,
        }
        options.add_experimental_option("prefs", prefs)
        options.page_load_strategy = "none"
        chrome_driver = webdriver.Chrome(
            service=Service(ChromeDriverManager().install()), options=options
        )
        chrome_driver.maximize_window()
        return chrome_driver

    @staticmethod
    def get_remote_driver() -> WebDriver:
        options = Options()
        options.add_argument("--window-size=850, 1980")
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")

        # Этот аргумент я добавил, чтобы избежать ошибки "selenium.common.exceptions.
        # WebDriverException: Message: unknown error: session deleted because of page crash"
        # "This will force Chrome to use the /tmp directory instead.
        # This may slow down the execution though since disk will be used instead of memory."
        # https://stackoverflow.com/questions/53902507/unknown-error-session-deleted-because-of-page-crash-from-unknown-error-cannot
        options.add_argument("--disable-dev-shm-usage")

        driver = webdriver.Remote(
            command_executor="http://localhost:3000/webdriver",
            options=options,
        )

        driver.set_window_size(850, 1980)
        return driver

    @staticmethod
    def get_firefox_driver() -> WebDriver:
        driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))
        return driver

    @staticmethod
    def get_safari_driver() -> WebDriver:
        raise NotImplementedError("Getting Safari driver not implemented yet.")
