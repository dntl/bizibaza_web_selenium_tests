import os

from api_client.model.login_request import Account
from factory.application_environment_factory import ApplicationEnvironmentFactory
from factory.login_via_api import User
from factory.sign_in_with_api_test_data import SignInWithApiTestData


DEFAULT_ACCOUNT = Account(login="buyer_001", password="Test123456")
SELLER_ACCOUNT = Account(
    login="sellertest_0001", password="Ss123456"
)  # sellerautotest Test123456 TODO: Create product
BUYER_ACCOUNT = Account(login="buyer_001", password="Test123456")

BUYER_ACCOUNT_WITH_HISTORY = Account(login="buyer_002", password="Test123456")

BUYER_ACCOUNT_WITH_SH = Account(
    login="buyer_sh_list", password="Test123456"
)  # buyer_sh_list@yopmail.com

ADMIN_ACCOUNT = Account(login="SA1", password="Bizibaza111")


class DevelopEnv(ApplicationEnvironmentFactory):
    def get_frontend_url(self) -> str:
        return os.environ["FRONTEND_URL"]

    def get_backend_url(self) -> str:
        return os.environ["BACKEND_URL"]

    def get_test_user_logout_data(self) -> Account:
        return DEFAULT_ACCOUNT

    def get_admin_user(self) -> User:
        return User(account=ADMIN_ACCOUNT)

    def get_seller_user(self) -> User:
        return User(account=SELLER_ACCOUNT)

    def get_buyer_user(self) -> User:
        return User(account=BUYER_ACCOUNT)

    def get_buyer_user_with_sh(self) -> User:
        return User(account=BUYER_ACCOUNT_WITH_SH)

    def get_buyer_user_with_history(self) -> User:
        return User(account=BUYER_ACCOUNT_WITH_HISTORY)

    def get_sign_in_with_api_test_data(self) -> SignInWithApiTestData:
        return SignInWithApiTestData(account=DEFAULT_ACCOUNT)
