import os

from factory.application_environment_factory import ApplicationEnvironmentFactory
from factory.develop_env import DevelopEnv
from factory.stage_env import StageEnv


def get_environment() -> ApplicationEnvironmentFactory:
    env_kind = os.environ["ENVIRONMENT_KIND"].lower()
    match env_kind:
        case "develop":
            return DevelopEnv()
        case "stage":
            return StageEnv()

    raise RuntimeError(f"Unknown environment kind: {env_kind}")
