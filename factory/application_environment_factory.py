from abc import ABC, abstractmethod

from api_client.model.login_request import Account
from factory.login_via_api import User
from factory.sign_in_with_api_test_data import SignInWithApiTestData


class ApplicationEnvironmentFactory(ABC):
    @abstractmethod
    def get_frontend_url(self) -> str:
        ...

    @abstractmethod
    def get_backend_url(self) -> str:
        ...

    @abstractmethod
    def get_sign_in_with_api_test_data(self) -> SignInWithApiTestData:
        ...

    @abstractmethod
    def get_seller_user(self) -> User:
        ...

    @abstractmethod
    def get_buyer_user(self) -> User:
        ...

    @abstractmethod
    # Buyer with shopping list item
    def get_buyer_user_with_sh(self) -> User:
        ...

    @abstractmethod
    # Buyer with History
    def get_buyer_user_with_history(self) -> User:
        ...

    @abstractmethod
    def get_admin_user(self) -> User:
        ...

    @abstractmethod
    def get_test_user_logout_data(self) -> Account:
        ...
