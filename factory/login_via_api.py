from dataclasses import dataclass

from api_client.model.login_request import Account


@dataclass
class User:
    account: Account
