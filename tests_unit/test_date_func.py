import datetime
import unittest
from datetime import date
from utility.formated_date import format_date


class TestFunc(unittest.TestCase):
    def test_format_date_1(self) -> None:
        date_str: str = "11/02/22"
        expected_date: date = datetime.date(2022, 11, 2)
        date_formatter_str = "%m/%d/%y"
        formatted_date = format_date(date_str, date_formatter_str)
        self.assertEqual(formatted_date, expected_date)

    def test_format_date_2(self) -> None:
        date_str: str = "September 29, 2022"
        expected_date: date = datetime.date(2022, 9, 29)
        date_formatter_str = "%B %d, %Y"
        formatted_date = format_date(date_str, date_formatter_str)
        self.assertEqual(formatted_date, expected_date)
