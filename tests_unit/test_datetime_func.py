import unittest
import datetime

from utility.approximate_time import ApproximateTime
from utility.formated_date import format_str_to_date_time


class TestFunc(unittest.TestCase):
    def test_format_date_and_time(self) -> None:
        date_str: str = "11/19/22, 7:34 AM"
        expected_date = datetime.datetime(2022, 11, 19, 7, 34, 0)
        date_formatter_str = "%m/%d/%y, %I:%M %p"
        formatted_date = format_str_to_date_time(date_str, date_formatter_str)
        self.assertEqual(formatted_date, expected_date)


class TestApproximateTime(unittest.TestCase):
    def test_approximate_time_equal(self) -> None:
        expected_date = ApproximateTime(
            datetime.datetime(2022, 11, 19, 7, 34, 58), delta=5
        )
        test_date = ApproximateTime(datetime.datetime(2022, 11, 19, 7, 35, 0))

        self.assertEqual(expected_date, test_date)

    def test_approximate_time_not_equal(self) -> None:
        expected_date = ApproximateTime(
            datetime.datetime(2022, 11, 19, 7, 34, 50), delta=5
        )
        test_date = ApproximateTime(datetime.datetime(2022, 11, 19, 7, 35, 0))

        self.assertNotEqual(expected_date, test_date)
